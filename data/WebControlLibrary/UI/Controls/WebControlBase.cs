﻿namespace WebControlLibrary.UI.Controls
{
    using System;
    using System.Web.UI.WebControls;

    public class WebControlBase : WebControl, IControlBase
    {
        public string GetMajickText()
        {
            var page = this.Page as PageBase;
            if (page == null)
            {
                throw new InvalidOperationException("Control must be on PageBase");
            }

            return page.GetMajickText();
        }
    }
}