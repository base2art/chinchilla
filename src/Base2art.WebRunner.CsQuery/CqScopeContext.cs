﻿namespace Base2art.WebRunner.CsQuery
{
    using global::CsQuery;

    public class CqScopeContext : ScopeContextBase
    {
        private readonly IDomElement scopedNode;

        public CqScopeContext(WebPageBase pageApi, IDomElement scopedNode)
            : base(pageApi)
        {
            this.scopedNode = scopedNode;
        }

        public IDomElement ScopedNode
        {
            get
            {
                return this.scopedNode;
            }
        }
    }
}