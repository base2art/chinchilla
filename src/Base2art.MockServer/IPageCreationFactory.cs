﻿namespace Base2art.MockServer
{
    using System.Web.UI;

    public interface IPageCreationFactory
    {
        TPage CreatePage<TPage>(string path)
            where TPage : Page;
    }
}