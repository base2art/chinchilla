﻿namespace Base2art.WebRunner
{
    using System;
    using System.ComponentModel;

    using Base2art.MockUI;

    public class Chinchilla : Component, IChinchilla
    {
        private static IChinchilla theChinchilla;

        private readonly IMockBrowser mockBrowser;

        private readonly IWebPageFactory webPageFactory;

        private readonly Lazy<IWebPage> lazyPage;

        private Chinchilla(LocatorType defaultLocatorType, IMockBrowser mockBrowser, IWebPageFactory webPageFactory)
        {
            this.mockBrowser = mockBrowser;
            this.webPageFactory = webPageFactory;
            this.lazyPage = new Lazy<IWebPage>(() => this.webPageFactory.Create(defaultLocatorType, this.mockBrowser));
        }

        public static IChinchilla Instance
        {
            get
            {
                return theChinchilla;
            }
        }

        public static IWebPage WebPage
        {
            get
            {
                return theChinchilla.WebPage;
            }
        }

        IWebPage IChinchilla.WebPage
        {
            get
            {
                return this.lazyPage.Value;
            }
        }

        public static IChinchilla SetInstance(IMockBrowser mockBrowser, IWebPageFactory webPageFactory)
        {
            return SetInstance(LocatorType.Label, mockBrowser, webPageFactory);
        }

        public static IChinchilla SetInstance(LocatorType defaultLocatorType, IMockBrowser mockBrowser, IWebPageFactory webPageFactory)
        {
            theChinchilla = new Chinchilla(defaultLocatorType, mockBrowser, webPageFactory);
            return theChinchilla;
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            if (disposing)
            {
                this.mockBrowser.Dispose();
            }
        }
    }
}
