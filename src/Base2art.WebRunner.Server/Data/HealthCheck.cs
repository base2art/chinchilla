﻿namespace Base2art.WebRunner.Server
{
    using System;

    public class HealthCheck
    {
        public string Url { get; set; }
        
        public TimeSpan Interval { get; set; }
    }
}


