﻿namespace Base2art.MockServer.Util.Converters
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Linq;

    public static class PostConverter
    {
        public static string ToQueryString(this IDictionary<string, string> collection)
        {
            Func<string, string> esc = s => Uri.EscapeDataString(s ?? string.Empty);
            var pairs = collection.Where(pair => pair.Key != null)
                                  .Select(pair => string.Concat(esc(pair.Key), "=", esc(pair.Value)));
            return string.Join("&", pairs);
        }

        public static NameValueCollection Convert(this IDictionary<string, string> collection)
        {
            NameValueCollection nameValueCollection = new NameValueCollection();

            foreach (var pair in collection)
            {
                nameValueCollection[pair.Key] = pair.Value;
            }

            return nameValueCollection;
        }

        public static IDictionary<string, string> Convert(this NameValueCollection collection)
        {
            Dictionary<string, string> dictionary = new Dictionary<string, string>();

            foreach (var key in collection.AllKeys)
            {
                dictionary[key] = collection[key];
            }

            return dictionary;
        }
    }
}
