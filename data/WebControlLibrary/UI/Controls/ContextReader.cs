﻿
namespace WebControlLibrary.UI.Controls
{
    using System.Web.UI;

    public class ContextReader : Control
    {
        protected override void Render(HtmlTextWriter writer)
        {
//            base.Render(writer);

            writer.Write(this.Context.Items[this.Key]);
        }

        public string Key { get; set; }
    }
}