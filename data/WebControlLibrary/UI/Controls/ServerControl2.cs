﻿
namespace WebControlLibrary.UI.Controls
{
    using System;
    using System.ComponentModel;
    using System.Web.UI;

    [DefaultProperty("Text")]
    [ToolboxData("<{0}:ServerControl2 runat=server></{0}:ServerControl2>")]
    public class ServerControl2 : WebControlBase
    {
        [Bindable(true)]
        [Category("Appearance")]
        [DefaultValue("")]
        [Localizable(true)]
        public string Text
        {
            get
            {
                String s = (String)this.ViewState["Text"];
                return ((s == null) ? "[" + this.ID + "]" : s);
            }

            set
            {
                this.ViewState["Text"] = value;
            }
        }

        protected override void RenderContents(HtmlTextWriter output)
        {
            output.Write("--st--");
            output.Write(this.GetMajickText());
            output.Write("--ln--");

            output.Write(this.Text);
        }
    }
}
