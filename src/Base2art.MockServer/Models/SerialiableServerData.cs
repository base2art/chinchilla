﻿namespace Base2art.MockServer
{
    using System;
    using System.Web;

    using Base2art.MockServer.Util;

    [Serializable]
    public class SerialiableServerData : IServerData
    {
        public string VirtualDirectory { get; set; }

        public bool UseDirectLoading { get; set; }

        public string PhysicalApplicationDirectory { get; set; }

        public string DefaultPage { get; set; }

        public IDelegate<AppDomain, HttpContext> ContextSetup { get; set; }
    }
}