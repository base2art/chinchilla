﻿
namespace Base2art.WebRunner.Server
{
    using System;
    using CommandLine;
    using CommandLine.Text;
    
    public class ModeOptions
    {
        [Option(
            'm', 
            "mode", 
            HelpText = "Single instance or multple instance",
            Required = true,
            DefaultValue = ServerMode.Single)]
        public ServerMode Mode { get; set; }
        
        [ParserState]
        public IParserState LastParserState { get; set; }

        [HelpOption]
        public string GetUsage()
        {
            return HelpText.AutoBuild(this,
                current => HelpText.DefaultParsingErrorsHandler(this, current));
        }
    }
}


