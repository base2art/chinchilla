﻿namespace Base2art.WebRunner.Features
{
    using System;
    using System.Linq.Expressions;

    using Base2art.MockUI;

    using Moq;

    using NUnit.Framework;

    [TestFixture]
    public class StaticApiFeature
    {
        [Test]
        public void ShouldHaveMethods()
        {
            this.Verify(() => WebPage.Source, x => x.Source, "sdf");

            this.Verify(() => WebPage.DefaultLocatorType, x => x.DefaultLocatorType, LocatorType.Label);

            this.Verify(() => WebPage.Check("ABC"), x => x.Check("ABC"));
            this.Verify(() => WebPage.Check(LocatorType.Css, "ABC"), x => x.Check(LocatorType.Css, "ABC"));

            this.Verify(() => WebPage.Choose("ABC"), x => x.Choose("ABC"));
            this.Verify(() => WebPage.Choose(LocatorType.Css, "ABC"), x => x.Choose(LocatorType.Css, "ABC"));

            this.Verify(() => WebPage.ClickButton("ABC"), x => x.ClickButton("ABC"));
            this.Verify(() => WebPage.ClickButton(LocatorType.Css, "ABC"), x => x.ClickButton(LocatorType.Css, "ABC"));

            this.Verify(() => WebPage.ClickLink("ABC"), x => x.ClickLink("ABC"));
            this.Verify(() => WebPage.ClickLink(LocatorType.Css, "ABC"), x => x.ClickLink(LocatorType.Css, "ABC"));

            this.Verify(() => WebPage.FillIn("ABC", "asd"), x => x.FillIn("ABC", "asd"));
            this.Verify(() => WebPage.FillIn(LocatorType.Css, "ABC", "asd"), x => x.FillIn(LocatorType.Css, "ABC", "asd"));

            this.Verify(() => WebPage.Select("ABC", "Data"), x => x.Select("ABC", "Data"));
            this.Verify(() => WebPage.Select(LocatorType.Css, "ABC", "Data"), x => x.Select(LocatorType.Css, "ABC", "Data"));

            this.Verify(() => WebPage.TriggerPostBack("ABC"), x => x.TriggerPostBack("ABC"));
            this.Verify(() => WebPage.TriggerPostBack(LocatorType.Css, "ABC"), x => x.TriggerPostBack(LocatorType.Css, "ABC"));

            this.Verify(() => WebPage.Uncheck("ABC"), x => x.Uncheck("ABC"));
            this.Verify(() => WebPage.Uncheck(LocatorType.Css, "ABC"), x => x.Uncheck(LocatorType.Css, "ABC"));

            this.Verify(() => WebPage.Visit("ABC"), x => x.Visit("ABC"));

            this.Verify(() => WebPage.Within(LocatorType.Css, "ABC"), x => x.Within(LocatorType.Css, "ABC"), null);
            this.Verify(() => WebPage.Within("ABC"), x => x.Within("ABC"), null);

            this.Verify(() => WebPage.FindElement(LocatorType.Css, "ABC"), x => x.FindElement(LocatorType.Css, "ABC"), null);
            this.Verify(() => WebPage.FindElement("ABC"), x => x.FindElement("ABC"), null);

            this.Verify(() => WebPage.FindFirstElement(LocatorType.Css, "ABC"), x => x.FindFirstElement(LocatorType.Css, "ABC"), null);
            this.Verify(() => WebPage.FindFirstElement("ABC"), x => x.FindFirstElement("ABC"), null);

            this.Verify(() => WebPage.FindElements(LocatorType.Css, "ABC"), x => x.FindElements(LocatorType.Css, "ABC"), null);
            this.Verify(() => WebPage.FindElements("ABC"), x => x.FindElements("ABC"), null);
        }

        private void Verify(Expression<Action> invocation, Expression<Action<IWebPage>> func)
        {
            var browser = new Mock<IMockBrowser>(MockBehavior.Strict);
            var pageApi = new Mock<IWebPage>(MockBehavior.Strict);
            var wpf = new MockedWebPageFactory(pageApi.Object);
            Chinchilla.SetInstance(browser.Object, wpf);

            pageApi.Setup(func).Verifiable();
            invocation.Compile()();
            pageApi.Verify(func);
        }

        private void Verify<T>(Expression<Func<T>> invocation, Expression<Func<IWebPage, T>> func, T returnValue)
        {
            var browser = new Mock<IMockBrowser>(MockBehavior.Strict);
            var pageApi = new Mock<IWebPage>(MockBehavior.Strict);
            var wpf = new MockedWebPageFactory(pageApi.Object);
            Chinchilla.SetInstance(browser.Object, wpf);

            pageApi.Setup(func).Returns(returnValue).Verifiable();
            invocation.Compile()();
            pageApi.Verify(func);
        }
    }
}
