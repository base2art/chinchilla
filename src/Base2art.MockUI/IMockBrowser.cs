﻿namespace Base2art.MockUI
{
    using System;

    using Base2art.MockServer;

    public interface IMockBrowser : IDisposable
    {
        IQueryablePageResult RequestPage(PageData pageData);
    }
}