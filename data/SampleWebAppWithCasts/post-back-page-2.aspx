﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="post-back-page-2.aspx.cs" Inherits="SampleWebAppWithCasts.post_back_page_2" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        
        PostBack first_name: <%= this.Request["first_name"]%>!
        PostBack button: <%= this.Request["my_button"]%>!
        PostBack testing: <%= this.Request["testing"]%>!

    </div>
    </form>
</body>
</html>
