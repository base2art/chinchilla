﻿namespace Base2art.MockServer.Util
{
    public interface IDelegate<in T>
    {
        void Invoke(T item);
    }

    public interface IDelegate<in T1, in T2>
    {
        void Invoke(T1 item1, T2 item2);
    }
}