﻿namespace SampleWebAppWithCasts
{
    using System;
    using System.Globalization;

    using WebControlLibrary;
    using WebControlLibrary.UI;

    public partial class _Default : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            this.someValue.Text = DateTime.UtcNow.Date.ToString(CultureInfo.InvariantCulture);
        }
    }
}
