﻿namespace Base2art.WebRunner
{
    using System;

    public interface IChinchilla : IDisposable
    {
        IWebPage WebPage { get; }

        //string Host { get; set; }
    }
}