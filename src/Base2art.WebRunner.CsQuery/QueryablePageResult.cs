﻿namespace Base2art.WebRunner.CsQuery
{
    using Base2art.MockUI;

    using global::CsQuery;

    public static class QueryablePageResult
    {
        public static CQ Document(this IQueryablePageResult html)
        {
            return new CQ(html.RawText);
        }
    }
}
