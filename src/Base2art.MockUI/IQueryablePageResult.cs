﻿namespace Base2art.MockUI
{
    using Base2art.MockServer;

    public interface IQueryablePageResult : IPageResult
    {
        IFormData[] FormData { get; }
    }
}