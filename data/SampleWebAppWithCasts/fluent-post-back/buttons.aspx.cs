﻿namespace SampleWebAppWithCasts.fluent_post_back
{
    using System;

    public partial class buttons : System.Web.UI.Page
    {
        protected void HandleButton1Click(object sender, EventArgs e)
        {
            this.ButtonLabelResponse1.Text = "Button 1 Clicked";
        }

        protected void HandleButton2Click(object sender, EventArgs e)
        {
            this.ButtonLabelResponse2.Text = "Button 2 Clicked";
        }
    }
}