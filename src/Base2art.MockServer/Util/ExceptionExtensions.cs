﻿namespace Base2art.MockServer.Util
{
    using System;

    public static class ExceptionExtensions
    {
        public static T SupressExceptions<T>(this Func<T> valueGetter)
        {
            try
            {
                return valueGetter();
            }
            catch (Exception)
            {
                return default(T);
            }
        }

        public static void SupressExceptions(this Action action)
        {
            try
            {
                action();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }
    }
}
