﻿namespace Base2art.MockServer
{
    using System.Web;

    public class HttpExtensibleApplicationDelegator : IHttpExtensibleApplication
    {
        public event RequestProcessing PreValidateRequestAndPath;

        public event RequestProcessing PostValidateRequestAndPath;

        public event RequestProcessing PreBeginRequest;

        public event RequestProcessing PreMapHandler;

        public event RequestProcessing PostMapHandler;

        public event RequestProcessing PreCallHandler;

        public event RequestProcessing PostCallHandler;

        public event RequestProcessing EndRequestHandler;

        internal virtual void OnPreValidateRequestAndPath(HttpContext context)
        {
            this.Fire(this.PreValidateRequestAndPath, context);
        }

        internal virtual void OnPostValidateRequestAndPath(HttpContext context)
        {
            this.Fire(this.PostValidateRequestAndPath, context);
        }

        internal virtual void OnPreBeginRequest(HttpContext context)
        {
            this.Fire(this.PreBeginRequest, context);
        }

        internal virtual void OnPreMapHandler(HttpContext context)
        {
            this.Fire(this.PreMapHandler, context);
        }

        internal virtual void OnPostMapHandler(HttpContext context)
        {
            this.Fire(this.PostMapHandler, context);
        }

        internal virtual void OnPreCallHandler(HttpContext context)
        {
            this.Fire(this.PreCallHandler, context);
        }

        internal virtual void OnPostCallHandler(HttpContext context)
        {
            this.Fire(this.PostCallHandler, context);
        }

        internal virtual void OnEndRequestHandler(HttpContext context)
        {
            this.Fire(this.EndRequestHandler, context);
        }

        private void Fire(RequestProcessing handlers, HttpContext context)
        {
            if (handlers != null)
            {
                handlers(context);
            }
        }
    }
}