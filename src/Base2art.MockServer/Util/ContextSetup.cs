﻿namespace Base2art.MockServer.Util
{
    using System;
    using System.Web;

    [Serializable]
    public class ContextSetup : IDelegate<AppDomain, HttpContext>
    {
        private readonly Action<AppDomain, HttpContext> func;

        public ContextSetup(Action<AppDomain, HttpContext> func)
        {
            this.func = func;
        }

        public void Invoke(AppDomain domain, HttpContext item)
        {
            if (this.func == null)
            {
                return;
            }

            this.func(domain, item);
        }
    }
}