﻿namespace Base2art.MockServer
{
    using System;
    using System.Web.UI;

    [Serializable]
    public class ContextExtensionData
    {
        public ContextExtensionData()
        {
            
        }
        public Page Page { get; set; }

        public RequestDataBase RequestData { get; set; }

        public bool HasException { get; set; }
        
        public IServerData ServerData { get; set; }
    }
}