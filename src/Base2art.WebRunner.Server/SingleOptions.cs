﻿
namespace Base2art.WebRunner.Server
{
    
    using System;
    using CommandLine;
    using CommandLine.Text;

    public class SingleOptions : ModeOptions
    {
        [Option('h', "hosts",
            HelpText = "The binding's host name (default: localhost).")]
        public string Hosts { get; set; }
        
        [Option('p', "port", Required = false,
            HelpText = "The port to run the site on (default: 7998)", DefaultValue = 7998)]
        public int Port { get; set; }
        
        [Option('s', "secure", Required = false,
            HelpText = "Indicates that ssl will be used (default: false)", DefaultValue = false)]
        public bool IsSecure { get; set; }

        [Option('r', "appRoot", Required = true,
            HelpText = "The directory of the application to run")]
        public string ApplicationDirectory { get; set; }

        [Option('v', "virDir", Required = false,
            HelpText = "The virtual directory of the application to run")]
        public string VirtualDirectory { get; set; }

        [Option("defaultPage", Required = false,
            HelpText = "The default Document if needed")]
        public string DefaultPage { get; set; }
        
        [Option("directLoading", Required = false,
            HelpText = "Enable to prevent Shadow Copies", DefaultValue = false)]
        public bool UseDirectLoading { get; set; }
    }
}
