﻿namespace Base2art.WebRunner.CsQuery.Features
{
    using Base2art.MockServer.Features;
    using Base2art.MockUI;
    using Base2art.MockUI.CsQuery;

    using FluentAssertions;

    using NUnit.Framework;

    [TestFixture]
    public class LinkApiFeature
    {
        [TearDown]
        public void DisposeChinchilla()
        {
            var chinchilla = Chinchilla.Instance;
            if (chinchilla != null)
            {
                chinchilla.Dispose();
            }
        }

        [Test]
        public void ShouldClickLinkByCss()
        {
            var mockBrowser = new MockBrowser(new CsQueryFormParser(), new MockServer.MockServer(ServerDataFactory.Create()));
            IChinchilla chinchilla = Chinchilla.SetInstance(mockBrowser, new WebPageFactory());

            chinchilla.WebPage.Visit("fluent-post-back/links.aspx");

            chinchilla.WebPage.Source.Should().NotContain("THIS IS THE PAGE FOR SELECTS!");
            chinchilla.WebPage.ClickLink(LocatorType.Css, ".control-2 a");
            chinchilla.WebPage.Source.Should().Contain("THIS IS THE PAGE FOR SELECTS!");
//            chinchilla.WebPage.CurrentUrl.Should().Contain("Button 2");
        }

        [Test]
        public void ShouldClickLinkByLabel()
        {
            var mockBrowser = new MockBrowser(new CsQueryFormParser(), new MockServer.MockServer(ServerDataFactory.Create()));
            IChinchilla chinchilla = Chinchilla.SetInstance(mockBrowser, new WebPageFactory());

            chinchilla.WebPage.Visit("fluent-post-back/links.aspx");

            chinchilla.WebPage.Source.Should().NotContain("THIS IS THE PAGE FOR SELECTS!");
            chinchilla.WebPage.ClickLink("Link 2");
            chinchilla.WebPage.Source.Should().Contain("THIS IS THE PAGE FOR SELECTS!");
        }

        [Test]
        public void ShouldClickLinkByPath()
        {
            var mockBrowser = new MockBrowser(new CsQueryFormParser(), new MockServer.MockServer(ServerDataFactory.Create()));
            IChinchilla chinchilla = Chinchilla.SetInstance(mockBrowser, new WebPageFactory());

            chinchilla.WebPage.Visit("fluent-post-back/links.aspx");

            chinchilla.WebPage.Source.Should().NotContain("THIS IS THE PAGE FOR SELECTS!");
            chinchilla.WebPage.ClickLink(LocatorType.Path, "//form/div/div[2]/a");
            chinchilla.WebPage.Source.Should().Contain("THIS IS THE PAGE FOR SELECTS!");
        }

        //[Test]
        //public void ShouldRespectViewState()
        //{
        //    var mockBrowser = new MockBrowser(new CsQueryFormParser(), new MockServer.MockServer(ServerDataFactory.Create()));
        //    IChinchilla chinchilla = Chinchilla.SetInstance(mockBrowser, new WebPageFactory());

        //    chinchilla.WebPage.Visit("fluent-post-back/links.aspx");

        //    chinchilla.WebPage.Source.Should().NotContain("Button 1 Clicked");
        //    chinchilla.WebPage.Source.Should().NotContain("Button 2 Clicked");

        //    chinchilla.WebPage.ClickLink(LocatorType.Css, ".control-2 input");
        //    chinchilla.WebPage.Source.Should().NotContain("Button 1 Clicked");
        //    chinchilla.WebPage.Source.Should().Contain("Button 2 Clicked");

        //    chinchilla.WebPage.ClickLink(LocatorType.Css, ".control-1 input");
        //    chinchilla.WebPage.Source.Should().Contain("Button 1 Clicked");
        //    chinchilla.WebPage.Source.Should().Contain("Button 2 Clicked");
        //}
    }
}
