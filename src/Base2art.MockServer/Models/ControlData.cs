﻿namespace Base2art.MockServer
{
    using System;
    using System.Web.UI;

    using Base2art.MockServer.Util;

    [Serializable]
    public class ControlData<T> : RequestDataBase
        where T : Control
    {
        private PageType pageType;

        public IControlCreator<T> Control { get; set; }

        public bool PlaceInsideForm { get; set; }

        public PageType PageType
        {
            get
            {
                return this.Page == null ? this.pageType : PageType.Delegate;
            }

            set
            {
                this.pageType = value;
            }
        }

        public IPageCreator<Page> Page { get; set; }
    }
}