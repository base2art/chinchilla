﻿namespace Base2art.MockServer
{
    using Base2art.Collections;

    public static class KnownHeaderTypesLookup
    {
        private static readonly Map<KnownHeaderTypes, string> requestHeaderMapping = new Map<KnownHeaderTypes, string>();
        private static readonly Map<KnownHeaderTypes, string> responseHeaderMapping = new Map<KnownHeaderTypes, string>();

        static KnownHeaderTypesLookup()
        {
            DefineHeader(true, true, KnownHeaderTypes.HeaderCacheControl, "Cache-Control", "HTTP_CACHE_CONTROL");
            DefineHeader(true, true, KnownHeaderTypes.HeaderConnection, "Connection", "HTTP_CONNECTION");
            DefineHeader(true, true, KnownHeaderTypes.HeaderDate, "Date", "HTTP_DATE");
            DefineHeader(true, true, KnownHeaderTypes.HeaderKeepAlive, "Keep-Alive", "HTTP_KEEP_ALIVE");
            DefineHeader(true, true, KnownHeaderTypes.HeaderPragma, "Pragma", "HTTP_PRAGMA");
            DefineHeader(true, true, KnownHeaderTypes.HeaderTrailer, "Trailer", "HTTP_TRAILER");
            DefineHeader(true, true, KnownHeaderTypes.HeaderTransferEncoding, "Transfer-Encoding", "HTTP_TRANSFER_ENCODING");
            DefineHeader(true, true, KnownHeaderTypes.HeaderUpgrade, "Upgrade", "HTTP_UPGRADE");
            DefineHeader(true, true, KnownHeaderTypes.HeaderVia, "Via", "HTTP_VIA");
            DefineHeader(true, true, KnownHeaderTypes.HeaderWarning, "Warning", "HTTP_WARNING");
            DefineHeader(true, true, KnownHeaderTypes.HeaderAllow, "Allow", "HTTP_ALLOW");
            DefineHeader(true, true, KnownHeaderTypes.HeaderContentLength, "Content-Length", "HTTP_CONTENT_LENGTH");
            DefineHeader(true, true, KnownHeaderTypes.HeaderContentType, "Content-Type", "HTTP_CONTENT_TYPE");
            DefineHeader(true, true, KnownHeaderTypes.HeaderContentEncoding, "Content-Encoding", "HTTP_CONTENT_ENCODING");
            DefineHeader(true, true, KnownHeaderTypes.HeaderContentLanguage, "Content-Language", "HTTP_CONTENT_LANGUAGE");
            DefineHeader(true, true, KnownHeaderTypes.HeaderContentLocation, "Content-Location", "HTTP_CONTENT_LOCATION");
            DefineHeader(true, true, KnownHeaderTypes.HeaderContentMd5, "Content-MD5", "HTTP_CONTENT_MD5");
            DefineHeader(true, true, KnownHeaderTypes.HeaderContentRange, "Content-Range", "HTTP_CONTENT_RANGE");
            DefineHeader(true, true, KnownHeaderTypes.HeaderExpires, "Expires", "HTTP_EXPIRES");
            DefineHeader(true, true, KnownHeaderTypes.HeaderLastModified, "Last-Modified", "HTTP_LAST_MODIFIED");
            DefineHeader(true, false, KnownHeaderTypes.HeaderAccept, "Accept", "HTTP_ACCEPT");
            DefineHeader(true, false, KnownHeaderTypes.HeaderAcceptCharset, "Accept-Charset", "HTTP_ACCEPT_CHARSET");
            DefineHeader(true, false, KnownHeaderTypes.HeaderAcceptEncoding, "Accept-Encoding", "HTTP_ACCEPT_ENCODING");
            DefineHeader(true, false, KnownHeaderTypes.HeaderAcceptLanguage, "Accept-Language", "HTTP_ACCEPT_LANGUAGE");
            DefineHeader(true, false, KnownHeaderTypes.HeaderAuthorization, "Authorization", "HTTP_AUTHORIZATION");
            DefineHeader(true, false, KnownHeaderTypes.HeaderCookie, "Cookie", "HTTP_COOKIE");
            DefineHeader(true, false, KnownHeaderTypes.HeaderExpect, "Expect", "HTTP_EXPECT");
            DefineHeader(true, false, KnownHeaderTypes.HeaderFrom, "From", "HTTP_FROM");
            DefineHeader(true, false, KnownHeaderTypes.HeaderHost, "Host", "HTTP_HOST");
            DefineHeader(true, false, KnownHeaderTypes.HeaderIfMatch, "If-Match", "HTTP_IF_MATCH");
            DefineHeader(true, false, KnownHeaderTypes.HeaderIfModifiedSince, "If-Modified-Since", "HTTP_IF_MODIFIED_SINCE");
            DefineHeader(true, false, KnownHeaderTypes.HeaderIfNoneMatch, "If-None-Match", "HTTP_IF_NONE_MATCH");
            DefineHeader(true, false, KnownHeaderTypes.HeaderIfRange, "If-Range", "HTTP_IF_RANGE");
            DefineHeader(true, false, KnownHeaderTypes.HeaderIfUnmodifiedSince, "If-Unmodified-Since", "HTTP_IF_UNMODIFIED_SINCE");
            DefineHeader(true, false, KnownHeaderTypes.HeaderMaxForwards, "Max-Forwards", "HTTP_MAX_FORWARDS");
            DefineHeader(true, false, KnownHeaderTypes.HeaderProxyAuthorization, "Proxy-Authorization", "HTTP_PROXY_AUTHORIZATION");
            DefineHeader(true, false, KnownHeaderTypes.HeaderReferer, "Referer", "HTTP_REFERER");
            DefineHeader(true, false, KnownHeaderTypes.HeaderRange, "Range", "HTTP_RANGE");
            DefineHeader(true, false, KnownHeaderTypes.HeaderTe, "TE", "HTTP_TE");
            DefineHeader(true, false, KnownHeaderTypes.HeaderUserAgent, "User-Agent", "HTTP_USER_AGENT");
            DefineHeader(false, true, KnownHeaderTypes.HeaderAcceptRanges, "Accept-Ranges", null);
            DefineHeader(false, true, KnownHeaderTypes.HeaderAge, "Age", null);
            DefineHeader(false, true, KnownHeaderTypes.HeaderEtag, "ETag", null);
            DefineHeader(false, true, KnownHeaderTypes.HeaderLocation, "Location", null);
            DefineHeader(false, true, KnownHeaderTypes.HeaderProxyAuthenticate, "Proxy-Authenticate", null);
            DefineHeader(false, true, KnownHeaderTypes.HeaderRetryAfter, "Retry-After", null);
            DefineHeader(false, true, KnownHeaderTypes.HeaderServer, "Server", null);
            DefineHeader(false, true, KnownHeaderTypes.HeaderSetCookie, "Set-Cookie", null);
            DefineHeader(false, true, KnownHeaderTypes.HeaderVary, "Vary", null);
            DefineHeader(false, true, KnownHeaderTypes.HeaderWwwAuthenticate, "WWW-Authenticate", null);
        }

        public static string Find(KnownHeaderTypes type, bool request, bool response)
        {
            if (request && requestHeaderMapping.Contains(type))
            {
                return requestHeaderMapping[type];
            }
            
            if (response && responseHeaderMapping.Contains(type))
            {
                return responseHeaderMapping[type];
            }
            
            return null;
        }
        
        private static void DefineHeader(bool isRequest, bool isResponse, KnownHeaderTypes index, string headerName, string serverVarName)
        {
            if (isRequest)
            {
                requestHeaderMapping[index] = headerName;
            }
            
            if (isResponse)
            {
                responseHeaderMapping[index] = headerName;
            }
        }
    }
}


