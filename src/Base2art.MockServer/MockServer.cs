﻿namespace Base2art.MockServer
{
    using System;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.IO;
    using System.Web;
    using System.Web.Hosting;
    using System.Web.UI;

    using Base2art.MockServer.Util;
    using Base2art.MockServerDirectHost;

    using Validation;

    // http://lostechies.com/chadmyers/2008/06/25/hosting-an-entire-asp-net-mvc-request-for-testing-purposes/
    public class MockServer : Component, IMockServer
    {
        private readonly ICreator<HttpBrowserCapabilities> creator;

        private readonly ServerData parameters;

        private readonly Lazy<IHost> host;

        public MockServer()
            : this(new JavascriptEnabledCapabilitiesFactory(), new ServerData())
        {
        }

        public MockServer(ICreator<HttpBrowserCapabilities> creator)
            : this(creator, new ServerData())
        {
        }

        public MockServer(ServerData parameters)
            : this(new JavascriptEnabledCapabilitiesFactory(), parameters)
        {
        }

        public MockServer(ICreator<HttpBrowserCapabilities> creator, ServerData parameters)
        {
            this.creator = creator;
            this.parameters = parameters;
            this.host = new Lazy<IHost>(() => this.GetHost(parameters));
        }

        public void EagerInit()
        {
            // Eager Load
            // ReSharper disable ReturnValueOfPureMethodIsNotUsed
            this.host.Value.ToString();
            // ReSharper restore ReturnValueOfPureMethodIsNotUsed
        }

        public IPageResult RequestPage(PageData pageData)
        {
            pageData.Validate().IsNotNull();
            return this.host.Value.RunPage(this.creator, this.CopyServerData(this.parameters), pageData);
        }

        public IPageResult RequestControl<T>(ControlData<T> controlData)
            where T : Control
        {
            controlData.Validate().IsNotNull();
            return this.host.Value.RequestControl(this.creator, this.CopyServerData(this.parameters), controlData);
        }

        protected IHost GetHost(ServerData serverData)
        {
            var physicalApplicationDirectory = serverData.PhysicalApplicationDirectory;
            string destinationDirectory = Path.Combine(physicalApplicationDirectory, "bin");

            string primarySourceDll = new Uri(typeof(AspNetHost).Assembly.CodeBase).LocalPath;
            string sourceDirectory = Path.GetDirectoryName(primarySourceDll);
            Directory.CreateDirectory(destinationDirectory);

            Debug.WriteLine(string.Format("Creating Application in: {0}", physicalApplicationDirectory));

            IHost localHost;

            // http://stackoverflow.com/questions/3236909/using-applicationhost-createapplicationhost-to-create-an-asp-net-post-while-l
            if (serverData.UseDirectLoading)
            {
                ShadowCopyItemsByExtension(sourceDirectory, destinationDirectory, "*.exe");
                ShadowCopyItemsByExtension(sourceDirectory, destinationDirectory, "*.dll");
                ShadowCopyItemsByExtension(sourceDirectory, destinationDirectory, "*.pdb");

                var localHostWrapper = ApplicationHost.CreateApplicationHost(
                    typeof(AspNetHostDirectReferenceProxy),
                    serverData.VirtualDirectory,
                    physicalApplicationDirectory);

                localHost = (IHost)((AspNetHostDirectReferenceProxy)localHostWrapper).Unwrap();
            }
            else
            {
                ShadowCopyItem(sourceDirectory, destinationDirectory, "Base2art.MockServerDynamicHost.dll");
                ShadowCopyItem(sourceDirectory, destinationDirectory, "Base2art.MockServerDynamicHost.pdb");

                var localHostWrapper = ApplicationHost.CreateApplicationHost(
                    typeof(AspNetHostDynamicLoadingProxy),
                    serverData.VirtualDirectory,
                    physicalApplicationDirectory);

                localHost = (IHost)((AspNetHostDynamicLoadingProxy)localHostWrapper).Unwrap();
            }

            // In order for the server
            // to be initialized
            // for consumption
            // requests must run.
            localHost.Init(serverData.DefaultPage);

            return localHost;
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            if (disposing)
            {
                if (this.host.IsValueCreated)
                {
                    this.host.Value.Dispose();
                }
            }
        }

        private static void ShadowCopyItemsByExtension(string sourceDirectory, string destinationDirectory, string searchPattern)
        {
            foreach (var item in Directory.GetFiles(sourceDirectory, searchPattern))
            {
                var fileName = Path.GetFileName(item);
                ShadowCopyItem(sourceDirectory, destinationDirectory, fileName);
            }
        }

        private static void ShadowCopyItem(string sourceDirectory, string destinationDirectory, string fileName)
        {
            string srcFileName = Path.Combine(sourceDirectory, fileName);
            if (!File.Exists(srcFileName))
            {
                return;
            }

            string destFileName = Path.Combine(destinationDirectory, fileName);
            const StringComparison Comp = StringComparison.InvariantCultureIgnoreCase;

            //// Console.WriteLine(Environment.CurrentDirectory);
            //// Console.WriteLine(srcFileName);

            if (fileName.StartsWith("Base2art.Mock", Comp) || fileName.StartsWith("Base2art.WebRunner", Comp))
            {
                // This is a bad hack...
                File.Copy(srcFileName, destFileName, true);
            }
            else if (!File.Exists(destFileName))
            {
                File.Copy(srcFileName, destFileName, true);
            }
            else if (File.GetLastWriteTimeUtc(srcFileName) > File.GetLastWriteTimeUtc(destFileName))
            {
                File.Copy(srcFileName, destFileName, true);
            }
            else
            {
                Debug.WriteLine("NewerFileFound: " + fileName);
            }
        }

        private IServerData CopyServerData(IServerData data)
        {
            return new SerialiableServerData
            {
                ContextSetup = data.ContextSetup,
                DefaultPage = data.DefaultPage,
                PhysicalApplicationDirectory = data.PhysicalApplicationDirectory,
                UseDirectLoading = data.UseDirectLoading,
                VirtualDirectory = data.VirtualDirectory
            };
        }
    }
}