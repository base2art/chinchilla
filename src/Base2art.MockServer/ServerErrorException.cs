namespace Base2art.MockServer
{
    using System;
    using System.Runtime.Serialization;

    [Serializable]
    public class ServerErrorException : ResponseException
    {
        public ServerErrorException(int statusCode, string simpleMessage, IPageResult pageResult, Exception exception)
            : base(statusCode, simpleMessage, pageResult, exception)
        {
        }

        public ServerErrorException()
        {
        }

        public ServerErrorException(string message)
            : base(message)
        {
        }

        public ServerErrorException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        protected ServerErrorException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}