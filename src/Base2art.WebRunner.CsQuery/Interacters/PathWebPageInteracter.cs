﻿namespace Base2art.WebRunner.CsQuery.Interacters
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Linq;
    using System.Xml.XPath;

    using Base2art.MockUI.CsQuery;

    using global::CsQuery;

    public class PathWebPageInteracter : WebPageInteracterBase
    {
        public PathWebPageInteracter(CsQueryWebPage pageApi, IScopeContext scope, string[] explicitlyMatchedNodes)
            : base(pageApi, scope, false, explicitlyMatchedNodes)
        {
        }

        protected override IDomObjectWrapper[] FindTargetElements(string locator)
        {
            var scopedNode = this.ScopedNode;

            List<IDomObjectWrapper> domObjects = new List<IDomObjectWrapper>();

            XNode doc;
            IDomDocument node = scopedNode as IDomDocument;
            int counter = 0;
            if (node != null)
            {
                doc = node.ToXDocument();
            }
            else
            {
                doc = ((IDomElement)scopedNode).ToXElement();
                counter += 1;
            }

            var selectors = doc.XPathSelectElements(locator).Select(x => x.GeneratePath()).ToArray();
            foreach (var selector in selectors)
            {
                var parts = selector.ParsePath();
                var result = this.FindElement(scopedNode, parts, counter);
                domObjects.Add(new DomObjectWrapper(result));
            }

            return domObjects.ToArray();
        }

        private IDomObject FindElement(IDomNode node, KeyValuePair<string, int>[] parts, int i)
        {
            if (i >= parts.Length)
            {
                return (IDomObject)node;
            }

            var element = node.ChildElements
                .Where(x => string.Equals(x.NodeName, parts[i].Key, StringComparison.InvariantCultureIgnoreCase))
                .ElementAt(parts[i].Value - 1);
            return this.FindElement(element, parts, i + 1);
        }

        ////private class PathObjectWrapper : IDomObjectWrapper
        ////{
        ////    private readonly XElement element;

        ////    public PathObjectWrapper(XElement element)
        ////    {
        ////        this.element = element;
        ////    }

        ////    public string Value
        ////    {
        ////        get
        ////        {
        ////            return this.element.Attribute("value").Value;
        ////        }
        ////    }

        ////    public string Name
        ////    {
        ////        get
        ////        {
        ////            return this.element.Attribute("name").Value;
        ////        }
        ////    }

        ////    public IFormWrapper Form
        ////    {
        ////        get
        ////        {
        ////            return new PathFormWrapper(this.element.FindForm());
        ////        }
        ////    }

        ////    public string PathToElement
        ////    {
        ////        get
        ////        {
        ////            return this.element.GeneratePath();
        ////        }
        ////    }

        ////    public string NodeName
        ////    {
        ////        get
        ////        {
        ////            return this.element.Name.LocalName.ToLowerInvariant();
        ////        }
        ////    }

        ////    public string TextValue
        ////    {
        ////        get
        ////        {
        ////            return this.element.Value;
        ////        }
        ////    }

        ////    public CQ NativeElement
        ////    {
        ////        get
        ////        {
        ////            throw new NotImplementedException("Scoping isn't supported in XPath at this point");
        ////        }
        ////    }

        ////    public string this[string name]
        ////    {
        ////        get
        ////        {
        ////            var attr = this.element.Attribute(name);
        ////            return attr == null ? null : attr.Value;
        ////        }
        ////    }

        ////    private class PathFormWrapper : IFormWrapper
        ////    {
        ////        private readonly XElement form;

        ////        public PathFormWrapper(XElement form)
        ////        {
        ////            this.form = form;
        ////        }

        ////        public string PathToElement
        ////        {
        ////            get
        ////            {
        ////                return this.form.GeneratePath();
        ////            }
        ////        }

        ////        public string Action
        ////        {
        ////            get
        ////            {
        ////                var action = this.form.Attribute("action");
        ////                return action == null ? string.Empty : action.Value;
        ////            }
        ////        }

        ////        public HttpMethod Method
        ////        {
        ////            get
        ////            {
        ////                var action = this.form.Attribute("method");

        ////                if (action != null)
        ////                {
        ////                    var httpMethod = HttpMethod.Post;
        ////                    if (string.Equals(action.Value, httpMethod.Method, StringComparison.InvariantCultureIgnoreCase))
        ////                    {
        ////                        return httpMethod;
        ////                    }
        ////                }

        ////                return HttpMethod.Get;
        ////            }
        ////        }
        ////    }
        ////}
    }
}
