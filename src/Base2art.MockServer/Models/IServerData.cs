﻿namespace Base2art.MockServer
{
    using System;
    using System.Web;

    using Base2art.MockServer.Util;

    public interface IServerData
    {
        string VirtualDirectory { get; set; }

        bool UseDirectLoading { get; set; }

        string PhysicalApplicationDirectory { get; set; }

        string DefaultPage { get; set; }

        IDelegate<AppDomain, HttpContext> ContextSetup { get; set; }
    }
}