﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EmbeddedControl.ascx.cs"
    Inherits="SampleWebAppWithCasts.fluent_post_back.EmbeddedControl" %>
<asp:DropDownList ID="PostbackControl"  AutoPostBack="True" runat="server" OnSelectedIndexChanged="HandleChange">
    <asp:ListItem runat="server" Text="A">A</asp:ListItem>
    <asp:ListItem runat="server" Text="B">B</asp:ListItem>
</asp:DropDownList>

<asp:FormView ID="FormView1" runat="server"  
    AllowPaging="True" EnableViewState="False">
    <ItemTemplate>
    </ItemTemplate>
</asp:FormView>

        <asp:Label runat="server" ID="PostbackLabel"></asp:Label>
        
        <asp:Repeater ID="MainRepeater" runat="server">
            <ItemTemplate>
                <asp:LinkButton runat="server" Text="<%# Container.DataItem %>"  CommandArgument="Some Text" OnClick="HandleSomething"/>
            </ItemTemplate>

        </asp:Repeater>