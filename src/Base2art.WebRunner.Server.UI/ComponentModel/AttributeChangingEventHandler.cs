﻿namespace Base2art.WebRunner.Server.UI
{
    public delegate void AttributeChangingEventHandler<T>(object sender, AttributeChangingEventArgs<T> e);
}
