﻿
using System;
using System.Web.Mvc;

namespace SampleMvcWebAppWithCasts_v4_5_2.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
        
        public ActionResult Contact()
        {
            return View();
        }
    }
}
