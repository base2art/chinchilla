﻿namespace Base2art.MockServer.Features.Fixtures.Controls
{
    using System.Web.UI;

    public class QueryStringControl : Control
    {
        public string Key { get; set; }

        public string Text { get; set; }

        protected override void OnInit(System.EventArgs e)
        {
            base.OnInit(e);
            this.Text = this.Page.Request.QueryString[this.Key];
        }

        protected override void Render(HtmlTextWriter writer)
        {
            writer.Write(this.Text);
        }
    }
}
