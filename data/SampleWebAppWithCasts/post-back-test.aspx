﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="post-back-test.aspx.cs"
    Inherits="SampleWebAppWithCasts.post_back_test" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <div class="text-box-1">
            <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
        </div>
        <div>
            <asp:TextBox ID="TextBox2" runat="server" TextMode="MultiLine"></asp:TextBox>
        </div>
        <div>
            <asp:TextBox ID="TextBox3" runat="server" TextMode="Password"></asp:TextBox>
            Password: <asp:Literal runat="server" ID="PasswordDisplay"></asp:Literal>
        </div>
        <div>
            <asp:DropDownList ID="DropDownList1" runat="server">
                <asp:ListItem Text="A" Selected="True"></asp:ListItem>
                <asp:ListItem Text="B"></asp:ListItem>
                <asp:ListItem Text="C"></asp:ListItem>
            </asp:DropDownList>
        </div>
        <div>
            <asp:RadioButtonList ID="RadioButtonList1" runat="server">
                <asp:ListItem Text="x" Selected="True"></asp:ListItem>
                <asp:ListItem Text="y"></asp:ListItem>
                <asp:ListItem Text="z"></asp:ListItem>
            </asp:RadioButtonList>
        </div>
        <div>
            <asp:CheckBoxList runat="server">
                <asp:ListItem Text="r" Selected="True"></asp:ListItem>
                <asp:ListItem Text="s"></asp:ListItem>
                <asp:ListItem Text="t"></asp:ListItem>
            </asp:CheckBoxList>
        </div>
        <div>
            <asp:ListBox runat="server">
                <asp:ListItem Text="d" Selected="True"></asp:ListItem>
                <asp:ListItem Text="e"></asp:ListItem>
                <asp:ListItem Text="f"></asp:ListItem>
            </asp:ListBox>
        </div>
        <div class="post-back-button">
            <asp:Button runat="server" ID="SubmitButton" OnClick="HandleClick"/>
        </div>
    </div>
    </form>
</body>
</html>
