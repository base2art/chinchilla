﻿
namespace Base2art.WebRunner.Server.Data
{
    using Base2art.MockServer;
    
    public static class DataMapper
    {
        public static ServerData Map(this ConfigurableServerData serverData)
        {
            return new ServerData
            {
                PhysicalApplicationDirectory = serverData.PhysicalApplicationDirectory,
                DefaultPage = serverData.DefaultPage,
                UseDirectLoading = serverData.UseDirectLoading,
                VirtualDirectory = serverData.VirtualDirectory
            };
        }

        public static WebServerData Map(this ConfigurableWebServerData serverData)
        {
            return new WebServerData
            {
                Hosts = serverData.Hosts,
                Port = serverData.Port,
                UseSsl = serverData.UseSsl
            };
        }
    }
}
