﻿namespace Base2art.WebRunner.Server
{
    public interface ILogger
    {
        void WriteLine(string log);
    }
}