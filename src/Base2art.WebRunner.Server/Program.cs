﻿namespace Base2art.WebRunner.Server
{
    using System;

    using System.Globalization;
    using System.IO;
    using System.Linq;
    using Base2art.MockServer;

    public class Program
    {
        public static void Main(string[] args)
        {
            if (!Server.IsSupported)
            {
                throw new NotSupportedException("Your system is not supported");
            }
            
            var parser = new CommandLine.Parser(with =>
                {
                    with.CaseSensitive = false;
                    with.MutuallyExclusive = false;
                    with.HelpWriter = NullWriter();
                    with.ParsingCulture = CultureInfo.InvariantCulture;
                });
            
            var modeOptions = new ModeOptions();
            parser.ParseArguments(args, modeOptions);
            
            if (modeOptions.Mode == ServerMode.Multiple)
            {
                var multipleOptions = new MultipleOptions();
                if (parser.ParseArguments(args, multipleOptions))
                {
                    var serializer = new Base2art.Serialization.NewtonsoftSerializer();
                    if (!File.Exists(multipleOptions.ConfigFile))
                    {
                        var contentToWrite = serializer.Serialize<ConfigurableSiteData[]>(new[]
                            {
                                new ConfigurableSiteData
                                {
                                    ServerData = new ServerData(),
                                    WebServerData = new WebServerData(),
                                }
                            });
                        
                        File.WriteAllText(multipleOptions.ConfigFile, contentToWrite);
                        return;
                    }
                    
                    var file = File.ReadAllText(multipleOptions.ConfigFile);
                    
                    var content = serializer.Deserialize<ConfigurableSiteData[]>(file);
                    var logger = new ConsoleLogger();
                    var servers = content.Select(x => new Server(
                                          x.WebServerData,
                                          x.ServerData,
                                          logger))
                                         .ToList();
                    

                    servers.ForEach(x => x.Start());
                    Console.WriteLine("Press a enter to exit...");
                    Console.ReadLine();
                    servers.ForEach(x => x.Stop());
                }
                else
                {
                    Console.Error.WriteLine(multipleOptions.GetUsage());
                }
                
                return;
            }
            
            var options = new SingleOptions();
            if (parser.ParseArguments(args, options))
            {
                // Values are available here
                var webServer = new WebServerData();
                webServer.Hosts = (options.Hosts ?? "").Split(new []{ ';' }, StringSplitOptions.RemoveEmptyEntries);
                webServer.Port = (short)options.Port;
                webServer.UseSsl = options.IsSecure;
                
                var server = new ServerData();
                server.PhysicalApplicationDirectory = options.ApplicationDirectory;
                server.VirtualDirectory = options.VirtualDirectory;
                server.UseDirectLoading = options.UseDirectLoading;
                server.DefaultPage = options.DefaultPage;
                
                
                var program = new Server(
                                  webServer,
                                  server,
                                  new ConsoleLogger());

                program.Start();
                Console.WriteLine("Press a enter to exit...");
                Console.ReadLine();
                program.Stop();
            }
            else
            {
                Console.Error.WriteLine(options.GetUsage());
            }
        }

        private static TextWriter NullWriter()
        {
            return new StringWriter();
        }
    }
}
