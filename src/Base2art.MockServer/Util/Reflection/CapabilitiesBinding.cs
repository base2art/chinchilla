﻿namespace Base2art.MockServer.Util.Reflection
{
    using System;
    using System.Web;
    using System.Web.Configuration;

    using Base2art.Validation;

    public static class CapabilitiesBinding
    {
        /// <summary>
        /// The Type of <see cref="HttpCapabilitiesBase" />.
        /// </summary>
        private static readonly Type HttpCapabilitiesBaseType = typeof(HttpCapabilitiesBase);
        
        /// <summary>
        /// Set the Properties on the HttpCapabilities.
        /// </summary>
        /// <param name="browser">The Browser object.</param>
        /// <param name="semifor">The name of the semifor.</param>
        /// <param name="valueProp">The name of the value property.</param>
        /// <param name="value">The value to set.</param>
        public static void SetProperty(this HttpBrowserCapabilities browser, string semifor, string valueProp, object value)
        {
            var field1 = HttpCapabilitiesBaseType.GetField(semifor, StandardBinding.StandardBindingFlags);
            field1.Validate().IsNotNull();
            field1.SetValue(browser, true);

            var field2 = HttpCapabilitiesBaseType.GetField(valueProp, StandardBinding.StandardBindingFlags);
            field2.Validate().IsNotNull(); 
            field2.SetValue(browser, value);
        }
    }
}
