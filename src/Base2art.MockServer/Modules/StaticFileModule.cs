﻿namespace Base2art.MockServer.Modules
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Web;

    public class StaticFileModule : IHttpExtensibilityModule
    {
        private readonly Dictionary<string, string> map = new Dictionary<string, string>
        {
            {".nupkg", "application/zip"},
            {".obj", "application/octet-stream"},
            {".ra", "audio/x-pn-realaudio"},
            {".wsdl", "application/xml"},
            {".dll", "application/octet-stream"},
            {".ras", "image/x-cmu-raster"},
            {".ram", "application/x-pn-realaudio"},
            {".bcpio", "application/x-bcpio"},
            {".sh", "application/x-sh"},
            {".m1v", "video/mpeg"},
            {".xwd", "image/x-xwindowdump"},
            {".doc", "application/msword"},
            {".bmp", "image/x-ms-bmp"},
            {".shar", "application/x-shar"},
            {".js", "application/javascript"},
            {".src", "application/x-wais-source"},
            {".dvi", "application/x-dvi"},
            {".aif", "audio/x-aiff"},
            {".ksh", "text/plain"},
            {".dot", "application/msword"},
            {".mht", "message/rfc822"},
            {".p12", "application/x-pkcs12"},
            {".css", "text/css"},
            {".csh", "application/x-csh"},
            {".pwz", "application/vnd.ms-powerpoint"},
            {".pdf", "application/pdf"},
            {".cdf", "application/x-netcdf"},
            {".pl", "text/plain"},
            {".ai", "application/postscript"},
            {".jpe", "image/jpeg"},
            {".jpg", "image/jpeg"},
            {".py", "text/x-python"},
            {".xml", "text/xml"},
            {".jpeg", "image/jpeg"},
            {".ps", "application/postscript"},
            {".gtar", "application/x-gtar"},
            {".xpm", "image/x-xpixmap"},
            {".hdf", "application/x-hdf"},
            {".nws", "message/rfc822"},
            {".tsv", "text/tab-separated-values"},
            {".xpdl", "application/xml"},
            {".p7c", "application/pkcs7-mime"},
            {".ico", "image/x-icon"},
            {".eps", "application/postscript"},
            {".ief", "image/ief"},
            {".so", "application/octet-stream"},
            {".xlb", "application/vnd.ms-excel"},
            {".pbm", "image/x-portable-bitmap"},
            {".texinfo", "application/x-texinfo"},
            {".xls", "application/vnd.ms-excel"},
            {".tex", "application/x-tex"},
            {".rtx", "text/richtext"},
            {".html", "text/html"},
            {".aiff", "audio/x-aiff"},
            {".aifc", "audio/x-aiff"},
            {".exe", "application/octet-stream"},
            {".sgm", "text/x-sgml"},
            {".tif", "image/tiff"},
            {".mpeg", "video/mpeg"},
            {".ustar", "application/x-ustar"},
            {".gif", "image/gif"},
            {".ppt", "application/vnd.ms-powerpoint"},
            {".pps", "application/vnd.ms-powerpoint"},
            {".sgml", "text/x-sgml"},
            {".ppm", "image/x-portable-pixmap"},
            {".latex", "application/x-latex"},
            {".bat", "text/plain"},
            {".mov", "video/quicktime"},
            {".ppa", "application/vnd.ms-powerpoint"},
            {".tr", "application/x-troff"},
            {".rdf", "application/xml"},
            {".xsl", "application/xml"},
            {".eml", "message/rfc822"},
            {".nc", "application/x-netcdf"},
            {".sv4cpio", "application/x-sv4cpio"},
            {".bin", "application/octet-stream"},
            {".h", "text/plain"},
            {".tcl", "application/x-tcl"},
            {".wiz", "application/msword"},
            {".o", "application/octet-stream"},
            {".a", "application/octet-stream"},
            {".c", "text/plain"},
            {".wav", "audio/x-wav"},
            {".vcf", "text/x-vcard"},
            {".xbm", "image/x-xbitmap"},
            {".txt", "text/plain"},
            {".au", "audio/basic"},
            {".t", "application/x-troff"},
            {".tiff", "image/tiff"},
            {".texi", "application/x-texinfo"},
            {".oda", "application/oda"},
            {".ms", "application/x-troff-ms"},
            {".rgb", "image/x-rgb"},
            {".me", "application/x-troff-me"},
            {".sv4crc", "application/x-sv4crc"},
            {".qt", "video/quicktime"},
            {".mpa", "video/mpeg"},
            {".mpg", "video/mpeg"},
            {".mpe", "video/mpeg"},
            {".avi", "video/x-msvideo"},
            {".pgm", "image/x-portable-graymap"},
            {".pot", "application/vnd.ms-powerpoint"},
            {".mif", "application/x-mif"},
            {".roff", "application/x-troff"},
            {".htm", "text/html"},
            {".man", "application/x-troff-man"},
            {".etx", "text/x-setext"},
            {".zip", "application/zip"},
            {".movie", "video/x-sgi-movie"},
            {".pyc", "application/x-python-code"},
            {".png", "image/png"},
            {".pfx", "application/x-pkcs12"},
            {".mhtml", "message/rfc822"},
            {".tar", "application/x-tar"},
            {".pnm", "image/x-portable-anymap"},
            {".pyo", "application/x-python-code"},
            {".snd", "audio/basic"},
            {".cpio", "application/x-cpio"},
            {".swf", "application/x-shockwave-flash"},
            {".mp3", "audio/mpeg"},
            {".mp2", "audio/mpeg"},
            {".mp4", "video/mp4"},
            {".woff", "application/font-woff"},
            {".woff2", "application/font-woff2"},
            {".ttf", "application/octet-stream"},
        };
        
        public void Init(IHttpExtensibleApplication application)
        {
            application.PreCallHandler += this.HandleStaticFile;
        }

        private void HandleStaticFile(System.Web.HttpContext httpContext)
        {
            var handler = httpContext.CurrentHandler;
            if (handler == null || handler is DefaultHttpHandler)
            {
                ContextExtensionData extensionData = httpContext.GetExtensionData();

                var mapPath = httpContext.Server.MapPath("~/" + extensionData.RequestData.Path);
                if (File.Exists(mapPath))
                {
                    // Set the date from where the content was last modified
                    // It you are serving a file, use the last write time
                    DateTimeOffset contentModified;
                    var localTime = File.GetLastWriteTime(mapPath);
                    try
                    {
                        contentModified = new DateTimeOffset(localTime, DateTimeOffset.Now.Offset);
                    }
                    catch (Exception)
                    {
                        contentModified = new DateTimeOffset(localTime);
                    }

                    if (this.IsClientCached(httpContext.Request, contentModified.UtcDateTime))
                    {
                        httpContext.Response.Cache.SetLastModified(contentModified.UtcDateTime);
                        httpContext.Response.Cache.SetCacheability(HttpCacheability.ServerAndPrivate);
                        httpContext.Response.StatusCode = 304;
                        httpContext.Response.SuppressContent = true;
                    }
                    else
                    {
                        httpContext.Response.Cache.SetLastModified(contentModified.UtcDateTime);
                        httpContext.Response.Cache.SetCacheability(HttpCacheability.ServerAndPrivate);
                        
                        // Then serve your RSS feed or dynamic image
                        // or anything else for that matter.
                        
                        
                        var text = File.ReadAllBytes(mapPath);
                        var extension = Path.GetExtension(mapPath);
                        
                        if (map.ContainsKey(extension))
                        {
                            httpContext.Response.ContentType = map[extension];
                        }
                        
                        httpContext.Response.OutputStream.Write(text, 0, text.Length);
                    }
                    
                    httpContext.Response.End();
                }
            }
        }

        private bool IsClientCached(HttpRequest request, DateTime contentModified)
        {
            string header = request.Headers["If-Modified-Since"];

            if (header != null)
            {
                DateTime isModifiedSince;
                if (DateTime.TryParse(header, out isModifiedSince))
                {
                    return isModifiedSince >= contentModified;
                }
            }

            return false;
        }
    }
}
