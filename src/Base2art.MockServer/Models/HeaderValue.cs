﻿namespace Base2art.MockServer
{
    using System;
    
    [Serializable]
    public struct HeaderValue //: IHeaderValue
    {
        public bool IsKnownValue { get; set; }
        public string Value { get; set; }
    }
}

