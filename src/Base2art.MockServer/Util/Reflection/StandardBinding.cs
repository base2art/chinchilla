﻿namespace Base2art.MockServer.Util.Reflection
{
    using System;
    using System.Reflection;

    using Base2art.Validation;

    public static class StandardBinding
    {
        /// <summary>
        /// The Standard Binding Flags.
        /// </summary>
        public const BindingFlags StandardBindingFlags = BindingFlags.NonPublic | BindingFlags.Instance;

        /// <summary>
        /// The static Binding Flags.
        /// </summary>
        public const BindingFlags StaticBindingFlags = BindingFlags.NonPublic | BindingFlags.Static;

        /// <summary>
        /// Set the Properties on an object.
        /// </summary>
        /// <typeparam name="T">
        /// The Type of the object used to Invoke.
        /// </typeparam>
        /// <param name="item">The Request object.</param>
        /// <param name="valueProp">The name of the value property.</param>
        /// <param name="value">The value to set.</param>
        public static void SetField<T>(this T item, string valueProp, object value)
            where T : class
        {
            item.Validate().IsNotNull();
            SetField(item, item.GetType(), valueProp, value);
        }

        /// <summary>
        /// Set the Properties on an object.
        /// </summary>
        /// <typeparam name="T">
        /// The Type of the object used to Invoke.
        /// </typeparam>
        /// <param name="item">The Request object.</param>
        /// <param name="type">The Type</param>
        /// <param name="valueProp">The name of the value property.</param>
        /// <param name="value">The value to set.</param>
        public static void SetField<T>(this T item, Type type, string valueProp, object value)
        {
            var containerField = type.GetField(valueProp, StandardBindingFlags);
            containerField.Validate().IsNotNull();
            containerField.SetValue(item, value);
        }

        /// <summary>
        /// Set the Properties on an object.
        /// </summary>
        /// <param name="type">The Type</param>
        /// <param name="valueProp">The name of the value property.</param>
        /// <param name="value">The value to set.</param>
        public static void SetStaticField(this Type type, string valueProp, object value)
        {
            SetField<object>(null, type, valueProp, value);
        }

        /// <summary>
        /// Set the Properties on an object.
        /// </summary>
        /// <typeparam name="T">
        /// The Type of the object used to Invoke.
        /// </typeparam>
        /// <param name="item">The Request object.</param>
        /// <param name="valueProp">The name of the value property.</param>
        /// <param name="value">The value to set.</param>
        public static void SetProperty<T>(this T item, string valueProp, object value)
            where T : class
        {
            item.Validate().IsNotNull();
            SetProperty(item, item.GetType(), valueProp, value);
        }

        /// <summary>
        /// Set the Properties on an object.
        /// </summary>
        /// <typeparam name="T">
        /// The Type of the object used to Invoke.
        /// </typeparam>
        /// <param name="item">The Request object.</param>
        /// <param name="type">The Type</param>
        /// <param name="valueProp">The name of the value property.</param>
        /// <param name="value">The value to set.</param>
        public static void SetProperty<T>(this T item, Type type, string valueProp, object value)
        {
            var containerField = type.GetProperty(valueProp, StandardBindingFlags);
            containerField.Validate().IsNotNull();
            containerField.SetValue(item, value, new object[0]);
        }

        /// <summary>
        /// Set the Properties on an object.
        /// </summary>
        /// <param name="type">The Type</param>
        /// <param name="valueProp">The name of the value property.</param>
        /// <param name="value">The value to set.</param>
        public static void SetStaticProperty(this Type type, string valueProp, object value)
        {
            SetProperty<object>(null, type, valueProp, value);
        }

        /// <summary>
        /// Set the Properties on an object.
        /// </summary>
        /// <typeparam name="T">
        /// The Type of the object used to Invoke.
        /// </typeparam>
        /// <param name="item">
        /// The Request object.
        /// </param>
        /// <param name="valueProp">
        /// The name of the value property.
        /// </param>
        /// <param name="values">
        /// The value to set.
        /// </param>
        public static void CallProcedure<T>(this T item, string valueProp, params object[] values)
            where T : class
        {
            item.Validate().IsNotNull();
            CallProcedure(item, item.GetType(), valueProp, values);
        }

        /// <summary>
        /// Set the Properties on an object.
        /// </summary>
        /// <typeparam name="T">
        /// The Type of the object used to Invoke.
        /// </typeparam>
        /// <param name="item">The Request object.</param>
        /// <param name="type">The Type</param>
        /// <param name="valueProp">The name of the value property.</param>
        /// <param name="values">The value to set.</param>
        public static void CallProcedure<T>(this T item, Type type, string valueProp, params object[] values)
        {
            var containerField = type.GetMethod(valueProp, StandardBindingFlags);
            containerField.Validate().IsNotNull();
            containerField.Invoke(item, values);
        }


        /// <summary>
        /// Set the Properties on an object.
        /// </summary>
        /// <typeparam name="T">
        /// The Type of the object used to Invoke.
        /// </typeparam>
        /// <typeparam name="TReturn">
        /// The Type of the object to return upon invoke.
        /// </typeparam>
        /// <param name="item">
        /// The Request object.
        /// </param>
        /// <param name="valueProp">
        /// The name of the value property.
        /// </param>
        /// <param name="values">
        /// The value to set.
        /// </param>
        /// <returns>
        /// The <see cref="TReturn"/> object after invoke.
        /// </returns>
        public static TReturn CallFunction<T, TReturn>(this T item, string valueProp, params object[] values)
            where T : class
        {
            item.Validate().IsNotNull();
            return CallFunction<T, TReturn>(item, item.GetType(), valueProp, values);
        }

        /// <summary>
        /// Set the Properties on an object.
        /// </summary>
        /// <typeparam name="T">
        /// The Type of the object used to Invoke.
        /// </typeparam>
        /// <typeparam name="TReturn">
        /// The Type of the object to return upon invoke.
        /// </typeparam>
        /// <param name="item">
        /// The Request object.
        /// </param>
        /// <param name="type">
        /// The Type
        /// </param>
        /// <param name="valueProp">
        /// The name of the value property.
        /// </param>
        /// <param name="values">
        /// The value to set.
        /// </param>
        /// <returns>
        /// The <see cref="TReturn"/> object after invoke.
        /// </returns>
        public static TReturn CallFunction<T, TReturn>(this T item, Type type, string valueProp, params object[] values)
        {
            var containerField = type.GetMethod(valueProp, item != null ? StandardBindingFlags : StaticBindingFlags);
            containerField.Validate().IsNotNull();
            return (TReturn)containerField.Invoke(item, values);
        }
    }
}
