﻿namespace Base2art.WebRunner
{
    public interface IWebPageAction
    {
        LocatorType DefaultLocatorType { get; }

        void TriggerPostBack(string locator);

        void TriggerPostBack(LocatorType locatorType, string locator);

        //void AttachFile(string selector, string path);

        void Check(string locator);

        void Check(LocatorType locatorType, string locator);

        void Choose(string locator);

        void Choose(LocatorType locatorType, string locator);

        void ClickButton(string locator);

        void ClickButton(LocatorType locatorType, string locator);

        void ClickLink(string locator);

        void ClickLink(LocatorType locatorType, string locator);

        ////void click_link_or_button(string selector);

        void FillIn(string locator, string with);

        void FillIn(LocatorType locatorType, string locator, string with);

        void Select(string locator, string with);

        void Select(LocatorType locatorType, string locator, string with);

        void Uncheck(string locator);

        void Uncheck(LocatorType locatorType, string locator);
    }
}