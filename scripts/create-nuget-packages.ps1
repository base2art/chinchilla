
$version = "0.1.0.8"
$dest = "W:\src\playground\LocalNugetServer\packages"

function delete_packing() {
  if (Test-Path -path ..\packing) {
    Remove-Item -Recurse -Force ..\packing
  }
  
  mkdir ..\packing
}

function copy_to($paths) {
  foreach ($item in $paths) {
    cp $item ..\packing
  }	
}

function process_package($nuspecName, $paths)
{
  delete_packing
  $paths += $nuspecName
  copy_to $paths
			
  (Get-Content ..\packing\*.nuspec) | 
    Foreach-Object {$_ -replace "AAAA.BBBB.CCCC.DDDD", $version} | 
    Set-Content ..\packing\*.nuspec
  
  cd ..\packing
  ..\packages\NuGet.CommandLine.2.8.0\tools\NuGet.exe pack $nuspecName
  cd ..\scripts
  
  cp ..\packing\*.nupkg $dest
  
  delete_packing
}

process_package ".\Base2art.MockServer.nuspec" @(
			"..\src\Base2art.MockServer\bin\Release\Base2art.MockServer.dll",
			"..\src\Base2art.MockServer\bin\Release\Base2art.MockServer.pdb",
			"..\src\Base2art.MockServer\bin\Release\Base2art.MockServerDynamicHost.dll",
			"..\src\Base2art.MockServer\bin\Release\Base2art.MockServerDynamicHost.pdb"
			)

process_package ".\Base2art.MockUI.nuspec" @(
			"..\src\Base2art.MockUI\bin\Release\Base2art.MockUI.dll",
			"..\src\Base2art.MockUI\bin\Release\Base2art.MockUI.pdb",
			"..\src\Base2art.MockUI.CsQuery\bin\Release\Base2art.MockUI.CsQuery.dll",
			"..\src\Base2art.MockUI.CsQuery\bin\Release\Base2art.MockUI.CsQuery.pdb"
			)

process_package ".\Base2art.WebRunner.nuspec" @(
			"..\src\Base2art.WebRunner\bin\Release\Base2art.WebRunner.dll",
			"..\src\Base2art.WebRunner\bin\Release\Base2art.WebRunner.pdb",
			"..\src\Base2art.WebRunner.CsQuery\bin\Release\Base2art.WebRunner.CsQuery.dll",
			"..\src\Base2art.WebRunner.CsQuery\bin\Release\Base2art.WebRunner.CsQuery.pdb"
			)

echo "Done..."