﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.master"
     Inherits="WebControlLibrary.UI.AbstractPageBase" %>

<script runat="server" type="text/C#">
    protected override void OnPreInit(EventArgs e)
    {
        this.Server.Execute("~/error-404.aspx",this.Response.Output, true);
        this.Response.End();
    }
    
</script>