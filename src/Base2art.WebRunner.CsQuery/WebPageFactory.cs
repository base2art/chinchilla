﻿namespace Base2art.WebRunner.CsQuery
{
    using Base2art.MockUI;

    public class WebPageFactory : IWebPageFactory
    {
        public IWebPage Create(LocatorType defaultLocatorType, IMockBrowser mockBrowser)
        {
            return new CsQueryWebPage(defaultLocatorType, mockBrowser);
        }
    }
}