﻿namespace Base2art.MockUI.CsQuery
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Base2art.Collections;

    using global::CsQuery;

    public class CsQueryFormParser : IFormParser
    {
        public IFormData[] GetFormData(string rawText)
        {
            List<FormData> formDatum = new List<FormData>();
            var doc = new CQ(rawText);

            foreach (var formDomObj in doc.Find("form"))
            {
                var formData = new FormData(formDomObj.GeneratePath());
                formDatum.Add(formData);

                var form = formDomObj.Cq();

                var inputs = form.Find("input");
                var textareas = form.Find("textarea");
                var selects = form.Find("select");

                inputs.ForAll(delegate(IDomObject x)
                {
                    if (x["type"] == "file")
                    {
                        throw new NotImplementedException("File Uploads Not currebntly supported");
                    }

                    var inputType = InputTypeHelper.ParseType(x["type"]);
                    var elementPath = x.GeneratePath();
                    var name = x["name"];
                    if (inputType == InputType.Checkbox)
                    {
                        formData.Add(elementPath, inputType, name, x.HasAttribute("checked") ? "on" : string.Empty, true);
                    }
                    else if (inputType == InputType.RadioButton)
                    {
                        formData.Add(elementPath, inputType, name, x["value"], x.HasAttribute("checked"));
                    }
                    else
                    {
                        formData.Add(elementPath, inputType, name, x["value"], true);
                    }
                });

                textareas.ForAll(x => formData.Add(x.GeneratePath(), InputType.TextArea, x["name"], x["value"], true));

                selects.ForAll(x =>
                {
                    var options = x.Cq().Find("option");
                    var selectedItem = options.FirstOrDefault(y => y.HasAttribute("selected"));
                    string selectedValue = string.Empty;
                    if (selectedItem != null)
                    {
                        selectedValue = selectedItem["value"];
                    }

                    var xpath = x.GeneratePath();
                    formData.Add(
                        x["name"],
                        x.GeneratePath(),
                        InputType.DropDownList,
                        selectedValue,
                        true,
                        options.Select(
                            y =>
                            new FormFieldOption
                            {
                                PathToElement = xpath,
                                Value = y["value"],
                                ValueText = y.InnerText
                            }).ToArray());
                });
            }

            return formDatum.ToArray();
        }
    }
}
