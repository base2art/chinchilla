﻿namespace Base2art.WebRunner.CsQuery
{
    using global::CsQuery;

    public interface IDomObjectWrapper
    {
        string Value { get; }

        string Name { get; }

        IFormWrapper Form { get; }

        string PathToElement { get; }

        string NodeName { get; }

        string TextValue { get; }

        CQ NativeElement { get; }

        string this[string name] { get; }
    }
}