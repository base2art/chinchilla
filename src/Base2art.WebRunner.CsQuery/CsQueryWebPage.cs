﻿namespace Base2art.WebRunner.CsQuery
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Base2art.MockServer;
    using Base2art.MockServer.Util.Converters;
    using Base2art.MockUI;
    using Base2art.WebRunner.CsQuery.Interacters;

    public class CsQueryWebPage : WebPageBase
    {
        private readonly IMockBrowser mockBrowser;

        private Dictionary<string, Dictionary<string, string>> backingValues;

        private IQueryablePageResult currentPage;

        public CsQueryWebPage(LocatorType defaultLocatorType, IMockBrowser mockBrowser)
            : base(defaultLocatorType)
        {
            this.mockBrowser = mockBrowser;
        }

        public IQueryablePageResult CurrentPage
        {
            get
            {
                return this.currentPage;
            }
        }

        public Dictionary<string, Dictionary<string, string>> BackingValues
        {
            get
            {
                if (this.backingValues == null)
                {
                    this.backingValues = new Dictionary<string, Dictionary<string, string>>();
                    foreach (var form in this.currentPage.FormData)
                    {
                        Dictionary<string, string> formData = new Dictionary<string, string>();
                        this.backingValues[form.PathToElement] = formData;
                        foreach (var field in form)
                        {
                            if (field.InputType.AutomaticallyPostOnSubmit())
                            {
                                if (field.InputType == InputType.DropDownList)
                                {
                                    var formFieldOption = field.Options.FirstOrDefault(x => x.ValueText == field.CurrentValue);
                                    formData[field.Name] = formFieldOption == null
                                                               ? ((field.Options.FirstOrDefault() ?? new FormFieldOption()).Value ?? string.Empty)
                                                               : field.CurrentValue;
                                }
                                else
                                {
                                    formData[field.Name] = field.CurrentValue;
                                }
                            }
                        }
                    }
                }

                return this.backingValues;
            }
        }

        protected override string SourceInternal
        {
            get
            {
                return this.currentPage.RawText;
            }
        }

        public void Navigate(PageData pageData)
        {
            this.backingValues = null;
            this.currentPage = this.mockBrowser.RequestPage(pageData);
        }

        public void Navigate(string path, string queryString, Dictionary<string, string> postData)
        {
            this.Navigate(
                new PageData
                {
                    Path = path,
                    QueryString = queryString,
                    Post = postData.Convert()
                });
        }

        public string QueryString(string pathAndQuery)
        {
            var uriBuilder = new UriBuilder("http://127.0.0.1/" + pathAndQuery);
            return new string(uriBuilder.Query.SkipWhile(x => x == '?').ToArray());
        }

        public string Path(string pathAndQuery)
        {
            var uriBuilder = new UriBuilder("http://127.0.0.1/" + pathAndQuery);
            return uriBuilder.Path;
        }

        protected override void DoVisit(string pathAndQuery)
        {
            this.Navigate(new PageData
                              {
                                  Path = this.Path(pathAndQuery),
                                  QueryString = this.QueryString(pathAndQuery)
                              });
        }

        protected override IWebPageInteracter CreateLabelInteracter(IScopeContext scope, params string[] explicitlyMatchedNodes)
        {
            return new LabelWebPageInteracter(this, scope, explicitlyMatchedNodes);
        }

        protected override IWebPageInteracter CreateCssInteracter(IScopeContext scope, params string[] explicitlyMatchedNodes)
        {
            return new CssWebPageInteracter(this, scope, explicitlyMatchedNodes);
        }

        protected override IWebPageInteracter CreatePathInteracter(IScopeContext scope, params string[] explicitlyMatchedNodes)
        {
            return new PathWebPageInteracter(this, scope, explicitlyMatchedNodes);
        }

        protected override IWebPageInteracter CreateTextInteracter(IScopeContext scope, params string[] explicitlyMatchedNodes)
        {
            return new TextWebPageInteracter(this, scope, explicitlyMatchedNodes);
        }

        protected override IWebPageInteracter CreateValueInteracter(IScopeContext scope, params string[] explicitlyMatchedNodes)
        {
            return new ValueWebPageInteracter(this, scope, explicitlyMatchedNodes);
        }
    }
}





// string Source { get; }
////public static void Visit(string path)
////{
////    Chinchilla.Instance.WebPage.Visit(path);
////}

////public static void FillIn(string locator, string with)
////{
////    Chinchilla.Instance.WebPage.FillIn(locator, with);
////}

////public static void FillIn(LocatorType locatorType, string locator, string with)
////{
////    Chinchilla.Instance.WebPage.FillIn(locatorType, locator, with);
////}

////public static void ClickButton(string locator)
////{
////    Chinchilla.Instance.WebPage.ClickButton(locator);
////}

////public static void ClickButton(LocatorType locatorType, string locator)
////{
////    Chinchilla.Instance.WebPage.ClickButton(locatorType, locator);
////}