﻿namespace Base2art.MockServerDirectHost
{
    using System;

    using Base2art.MockServer;

    public class AspNetHostDirectReferenceProxy : MarshalByRefObject
    {
        public override object InitializeLifetimeService()
        {
            return null;
        }

        public object Unwrap()
        {
            return new AspNetHost();
        }
    }
}