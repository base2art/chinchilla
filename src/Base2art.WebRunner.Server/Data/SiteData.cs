﻿namespace Base2art.WebRunner.Server
{
    using System;
    using System.ComponentModel;
    using Base2art.MockServer;
    
    //    [TypeConverter(typeof(ExpandableObjectConverter))]
    public class ConfigurableSiteData
    {
        public string Name { get; set; }
        
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public ServerData ServerData { get; set; }
        
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public WebServerData WebServerData { get; set; }
        
        private HealthCheck[] healthChecks;
        public HealthCheck[] HealthChecks
        {
            get
            {
                return healthChecks ?? new HealthCheck[0];
            }
            set
            {
                healthChecks = value;
            }
        }
    }
}
