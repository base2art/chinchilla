﻿namespace Base2art.MockServer.Util
{
    public interface ICreator<out TResult>
    {
        TResult Create();
    }

    public interface ICreator<in TParams, out TResult>
    {
        TResult Create(TParams pageData);
    }
}