﻿namespace Base2art.MockUI
{
    using System;
    using System.Net;

    using Base2art.Collections;
    using Base2art.MockServer;

    public class QueryablePageResult : IQueryablePageResult
    {
        private readonly IFormParser parser;

        private readonly IPageResult requestPage;

        private readonly Lazy<IFormData[]> lazyFormData;

        public QueryablePageResult(IFormParser parser, IPageResult requestPage)
        {
            this.parser = parser;
            this.requestPage = requestPage;
            this.lazyFormData = new Lazy<IFormData[]>(this.GetFormData);
        }

        public string Path
        {
            get
            {
                return this.requestPage.Path;
            }
        }

        public string NormalizedText
        {
            get
            {
                return this.requestPage.NormalizedText;
            }
        }

        public string RawText
        {
            get
            {
                return this.requestPage.RawText;
            }
        }

        public IReadOnlyMultiMap<string, Cookie> Cookies
        {
            get
            {
                return this.requestPage.Cookies;
            }
        }

        public IReadOnlyMap<string, HeaderValue> Headers
        {
            get
            {
                return this.requestPage.Headers;
            }
        }

        public int StatusCode
        {
            get
            {
                return this.requestPage.StatusCode;
            }
        }

        public string ContentType
        {
            get
            {
                return this.requestPage.ContentType;
            }
        }

        public IFormData[] FormData
        {
            get
            {
                return this.lazyFormData.Value;
            }
        }

        private IFormData[] GetFormData()
        {
            return this.parser.GetFormData(this.RawText);
        }
    }
}