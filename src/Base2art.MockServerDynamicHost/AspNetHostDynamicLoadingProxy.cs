﻿namespace Base2art.MockServer
{
    using System;
    using System.IO;
    using System.Linq;
    using System.Reflection;

    public class AspNetHostDynamicLoadingProxy : MarshalByRefObject
    {
        public override object InitializeLifetimeService()
        {
            AppDomain.CurrentDomain.AssemblyResolve += (x, y) =>
                {
                    var name = y.Name.Split(new[] { ',' }).First();
                    var path = Path.Combine(Environment.CurrentDirectory, name + ".dll");
                    if (File.Exists(path))
                    {
                        try 
                        {
                            return Assembly.LoadFile(path);
                        } 
                        catch (Exception ex)
                        {
                            throw new TypeLoadException(name + " : " + path, ex);
                        }
                    }

                    return null;
                };

            return null;
        }

        public object Unwrap()
        {
            var objectHandle = Activator.CreateInstance("Base2art.MockServer", "Base2art.MockServer.AspNetHost");
            var obj = objectHandle.Unwrap();
            return obj;
        }
    }
}
