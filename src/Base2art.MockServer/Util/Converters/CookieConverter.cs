﻿namespace Base2art.MockServer.Util.Converters
{
    using System.Net;
    using System.Web;

    internal static class CookieConverter
    {
        public static Cookie Convert(this HttpCookie cookie)
        {
            return new Cookie(cookie.Name, cookie.Value, cookie.Path, cookie.Domain)
            {
                Expires = cookie.Expires,
                Secure = cookie.Secure,
                HttpOnly = cookie.HttpOnly
            };
        }

        public static HttpCookie Convert(this Cookie cookie)
        {
            return new HttpCookie(cookie.Name, cookie.Value)
                       {
                           Path = cookie.Path,
                           Domain = cookie.Domain,
                           Expires = cookie.Expires,
                           Secure = cookie.Secure,
                           HttpOnly = cookie.HttpOnly
                       };
        }
    }
}
