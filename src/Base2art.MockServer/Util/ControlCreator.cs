﻿namespace Base2art.MockServer.Util
{
    using System;
    using System.Web.UI;

    [Serializable]
    public class ControlCreator<T> : IControlCreator<T>
        where T : Control
    {
        private readonly Func<T> func;

        public ControlCreator(Func<T> func)
        {
            this.func = func;
        }

        public T Create()
        {
            return this.func.Invoke();
        }
    }
}