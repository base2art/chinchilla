﻿
namespace Base2art.WebRunner.Server.UI
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Windows.Forms;
    using Base2art.WebRunner.Server.Data;

    public partial class MainForm : Form
    {
        private readonly List<Server> servers = new List<Server>();
        private readonly Base2art.Serialization.ISerializer serializer;
        //        private Uri uri;
        
        private readonly Observable<bool?> isRunning = new Observable<bool?>();
        private readonly List<HealthCheckContainer> healthChecks = new List<HealthCheckContainer>();
        
        public MainForm()
        {
            //
            // The InitializeComponent() call is required for Windows Forms designer support.
            //
            InitializeComponent();
            
            this.serializer = new Base2art.Serialization.NewtonsoftSerializer();
            
            isRunning.Changing += (x, y) =>
            {
                this.singleModelEditor1.Enabled = false;
            };
            
            isRunning.Changed += (x, y) =>
            {
                if (!y.Value.HasValue)
                {
                    return;
                }
                
                this.singleModelEditor1.Enabled = true;
                
                bool isServerRunning = y.Value.GetValueOrDefault();
                
                
                this.openBrowserButton.Enabled = isServerRunning;
                this.outputDisplay.Visible = isServerRunning;
                this.singleModelEditor1.Visible = !isServerRunning;
                
                this.runButton.Text = isServerRunning ? "Stop" : "Run";
            };
        }
        
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            
            this.Width = (int)((float)Screen.PrimaryScreen.WorkingArea.Width / 1.3);
            this.Height = (int)((float)Screen.PrimaryScreen.WorkingArea.Height / 1.3);
            this.Top = (Screen.PrimaryScreen.WorkingArea.Top + Screen.PrimaryScreen.WorkingArea.Height) / 6;
            this.Left = (Screen.PrimaryScreen.WorkingArea.Left + Screen.PrimaryScreen.WorkingArea.Width) / 6;

            this.outputDisplay.Dock = DockStyle.Fill;
            this.singleModelEditor1.Dock = DockStyle.Fill;
            
            this.isRunning.Value = null;
            string multiSiteConfig = null;
            if (File.Exists("server.config"))
            {
                multiSiteConfig = File.ReadAllText("server.config");
            }
            
            try
            {
                if (!string.IsNullOrWhiteSpace(multiSiteConfig))
                {
                    var sites = this.serializer.Deserialize<List<ConfigurableSiteData>>(multiSiteConfig);
                    this.singleModelEditor1.Sites = sites;
                }
            }
            catch (Exception)
            {
                
            }
            
            this.isRunning.Value = false;
        }
        
        private async Task Run()
        {
            isRunning.Value = null;
            
            var sites = this.singleModelEditor1.Sites.ToList();
            
            File.WriteAllText("server.config", this.serializer.Serialize(sites));
            
            var logger = new WindowLogger(this.outputDisplay);
            healthChecks.Clear();
            await Task.Factory.StartNew(() =>
                {
                    var newServers = sites.Select(x => new Server(
                                             x.WebServerData,
                                             x.ServerData,
                                             logger))
                                                .ToList();
                    this.servers.AddRange(newServers);
                                            
                    this.servers.ForEach(x => x.Start());
                });
            
            
            healthChecks.AddRange(sites.SelectMany(x => x.HealthChecks)
                .Select(x => new HealthCheckContainer(Guid.NewGuid(), x){ LastCheck = DateTimeOffset.UtcNow }));
            
            isRunning.Value = true;
        }

        private async Task Stop()
        {
            isRunning.Value = null;
            healthChecks.Clear();
            await Task.Factory.StartNew(() =>
                {
                    this.servers.ForEach(x => x.Stop());
                    this.servers.Clear();
                });
            isRunning.Value = false;
        }
        
        
        private void OpenBrowserButtonClick(object sender, EventArgs e)
        {
            //            if (this.uri != null)
            //            {
            //                Process.Start(this.uri.ToString());
            //            }
        }
        
        private async void runButtonClick(object sender, EventArgs e)
        {
            if (!this.isRunning.Value.HasValue)
            {
                return;
            }
            
            
            if (this.isRunning.Value.Value)
            {
                await this.Stop();
            }
            else
            {
                await this.Run();
            }
        }
        
        private void Timer1Tick(object sender, EventArgs ea)
        {
            
            Action<String> log = (s) => this.outputDisplay.Invoke(new Action(() =>
                    {
                        this.outputDisplay.Text += s + Environment.NewLine + Environment.NewLine;
                        this.outputDisplay.SelectionStart = this.outputDisplay.TextLength;
                        this.outputDisplay.ScrollToCaret();
                    }));
            
            var items = this.healthChecks
                .Where(x => (x.LastCheck + x.Check.Interval) <= DateTimeOffset.UtcNow)
                .ToArray();
            
            foreach (var item in items)
            {
                item.LastCheck = DateTimeOffset.UtcNow;
            }
            
            foreach (var item in items)
            {
                ThreadPool.QueueUserWorkItem(o =>
                    {
                        try
                        {
                            log("Health Check [Request]: " + item.Check.Url);
                            HttpWebRequest request = System.Net.WebRequest.CreateHttp(item.Check.Url);
                            using (WebResponse response = request.GetResponse())
                            {   
                                var httpWebResponse = ((HttpWebResponse)response);
                                log("Health Check [Response]: " + item.Check.Url + " " + httpWebResponse.StatusCode + " " + httpWebResponse.StatusDescription);
                                using (Stream dataStream = response.GetResponseStream())
                                {
                                    using (StreamReader reader = new StreamReader(dataStream))
                                    {
                                        string responseFromServer = reader.ReadToEnd();
                                    }
                                }
                            }
                        }
                        catch (WebException)
                        {
                        }
                        catch (Exception e)
                        {
                            log(e.Message);
                            log(e.StackTrace);
                        }
                    });
            }
            
            
//            await Task.WhenAll(items);
//            await Task.Factory.StartNew(() =>
//                {
//                    Parallel.ForEach(this.healthChecks, (check) =>
//                        {
//                            if ()
//                            {
//                                
//                               
//
//                                try 
//                                {
//                                    client.D
//                                check.Check.Url
//                                }
//                                catch (Exception) 
//                                {
//                                    
//                                    throw;
//                                }
//                            }
//                        });
//                });
        }
        
        private class WindowLogger : ILogger
        {

            private readonly TextBox outputDisplay;

            public WindowLogger(TextBox outputDisplay)
            {
                this.outputDisplay = outputDisplay;
            }
            
            public void WriteLine(string log)
            {
                this.outputDisplay.Invoke(new Action(() =>
                        {
                            //                Console.WriteLine(log);
                            this.outputDisplay.Text += log + Environment.NewLine + Environment.NewLine;
                            this.outputDisplay.SelectionStart = this.outputDisplay.TextLength;
                            this.outputDisplay.ScrollToCaret();
                        }));
            }
            
            public void Write(char value)
            {
                this.outputDisplay.Invoke(new Action(() =>
                        {
                            this.outputDisplay.Text += value;
                            this.outputDisplay.SelectionStart = this.outputDisplay.TextLength;
                            this.outputDisplay.ScrollToCaret();
                        }));
            }
        }
        
        private class WindowLoggerWrapper : TextWriter
        {
            private readonly WindowLogger logger;

            public WindowLoggerWrapper(WindowLogger logger)
            {
                this.logger = logger;
            }

            public override System.Text.Encoding Encoding
            {
                get { return System.Text.Encoding.Default; }
            }
            
            public override void Write(char value)
            {
                this.logger.Write(value);
            }
        }
    }
}



//            Settings1.Default.AppDir = this.AppDirInput.Text;
//            Settings1.Default.Save();
//
//            var webServer = new WebServerData();
//            var server = new ServerData();
//
//
//            webServer.Hosts = (this.hostChooser.Text ?? "").Split(new []{';'}, StringSplitOptions.RemoveEmptyEntries);
//            webServer.Port = (short)this.portChooser.Value;
//            webServer.UseSsl = this.sslChooser.Checked;
//
//            server.PhysicalApplicationDirectory = this.AppDirInput.Text;
//            server.VirtualDirectory = this.VirDirInput.Text;
//            server.UseDirectLoading = this.directLoadingChooser.Checked;
//            server.DefaultPage = "Default.aspx";
//
//            var logger = new WindowLogger(this.outputDisplay);
//
//            Console.SetOut(new WindowLoggerWrapper(logger));
//
//            try
//            {
//                this.serverRunner = new Server(webServer, server, logger);
//
//                this.uri = this.serverRunner.PrimaryUrl;
//
//                serverRunner.Start();
//
//                this.runButton.Text = "Stop";
//                this.openBrowserButton.Enabled = true;
//            }
//            catch (Exception ex)
//            {
//                try
//                {
//                    logger.WriteLine(ex.GetType().ToString());
//                    logger.WriteLine(ex.Message);
//                    logger.WriteLine(ex.StackTrace);
//                }
//                catch (Exception)
//                {
//                }
//            }


        
//        private void SslChooserCheckedChanged(object sender, EventArgs e)
//        {
////            if (this.sslChooser.Checked)
////            {
////                if (this.portChooser.Value == 80)
////                {
////                    this.portChooser.Value = 443;
////                }
////            }
////            else
////            {
////                if (this.portChooser.Value == 443)
////                {
////                    this.portChooser.Value = 80;
////                }
////            }
//        }
        
//        private void Button1Click(object sender, EventArgs e)
//        {
////            var result = this.folderBrowserDialog1.ShowDialog();
////            if (result == DialogResult.OK)
////            {
////                this.AppDirInput.Text = this.folderBrowserDialog1.SelectedPath;
////            }
//        }
        
