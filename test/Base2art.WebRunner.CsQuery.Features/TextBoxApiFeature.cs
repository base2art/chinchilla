﻿namespace Base2art.WebRunner.CsQuery.Features
{
    using System;

    using Base2art.MockServer;
    using Base2art.MockServer.Features;
    using Base2art.MockUI;
    using Base2art.MockUI.CsQuery;

    using FluentAssertions;

    using NUnit.Framework;

    [TestFixture]
    public class TextBoxApiFeature
    {
        [TearDown]
        public void DisposeChinchilla()
        {
            var chinchilla = Chinchilla.Instance;
            if (chinchilla != null)
            {
                chinchilla.Dispose();
            }
        }

        [Test]
        public void ShouldHaveFluentApiByCss()
        {
            // Setup normally done in an [SetupFixture] class
            // but I do it here for testability.
            // http://www.nunit.org/index.php?p=setupFixture&r=2.4
            var mockBrowser = new MockBrowser(new CsQueryFormParser(), new MockServer(ServerDataFactory.Create()));
            IChinchilla chinchilla = Chinchilla.SetInstance(mockBrowser, new WebPageFactory());

            chinchilla.WebPage.Visit("fluent-post-back/text-boxes.aspx");
            chinchilla.WebPage.FillIn(LocatorType.Css, ".control-1 input", "hello world");
            chinchilla.WebPage.ClickButton(LocatorType.Css, ".post-back-button input");
            chinchilla.WebPage.Source.Should().Contain("name=\"Control1\" type=\"text\" value=\"hello world\" id=\"Control1\" />");
            chinchilla.WebPage.Source.Should().Contain("name=\"Control2\" type=\"text\" id=\"Control2\" />");
            chinchilla.WebPage.Source.Should().Contain("name=\"Control3\" type=\"text\" value=\"hello world\" id=\"Control3\" />");
        }

        [Test]
        public void ShouldHaveFluentApiByLabel()
        {
            // Setup normally done in an [SetupFixture] class
            // but I do it here for testability.
            // http://www.nunit.org/index.php?p=setupFixture&r=2.4
            var mockBrowser = new MockBrowser(new CsQueryFormParser(), new MockServer(ServerDataFactory.Create()));
            IChinchilla chinchilla = Chinchilla.SetInstance(mockBrowser, new WebPageFactory());

            chinchilla.WebPage.Visit("fluent-post-back/text-boxes.aspx");
            chinchilla.WebPage.FillIn("My First Control:", "hello world2");
            new Action(() => chinchilla.WebPage.FillIn("My Second Control:", "hello world3")).ShouldThrow<AmbiguousMatchFoundException>();
            new Action(() => chinchilla.WebPage.FillIn("My Non-Existant Control:", "hello world3")).ShouldThrow<NoMatchFoundException>();
            chinchilla.WebPage.ClickButton(LocatorType.Css, ".post-back-button input");
            chinchilla.WebPage.Source.Should().Contain("name=\"Control1\" type=\"text\" value=\"hello world2\" id=\"Control1\" />");
            chinchilla.WebPage.Source.Should().Contain("name=\"Control2\" type=\"text\" id=\"Control2\" />");
            chinchilla.WebPage.Source.Should().Contain("name=\"Control3\" type=\"text\" id=\"Control3\" />");
        }

        [Test]
        public void ShouldHaveFluentApiByPath()
        {
            // Setup normally done in an [SetupFixture] class
            // but I do it here for testability.
            // http://www.nunit.org/index.php?p=setupFixture&r=2.4
            var mockBrowser = new MockBrowser(new CsQueryFormParser(), new MockServer(ServerDataFactory.Create()));
            IChinchilla chinchilla = Chinchilla.SetInstance(mockBrowser, new WebPageFactory());

            chinchilla.WebPage.Visit("fluent-post-back/text-boxes.aspx");
            chinchilla.WebPage.FillIn(LocatorType.Path, "//form/div/div[1]/input[1] | //form/div/div[3]/input[1]", "hello world2");
            chinchilla.WebPage.ClickButton(LocatorType.Css, ".post-back-button input");
            chinchilla.WebPage.Source.Should().Contain("name=\"Control1\" type=\"text\" value=\"hello world2\" id=\"Control1\" />");
            chinchilla.WebPage.Source.Should().Contain("name=\"Control2\" type=\"text\" id=\"Control2\" />");
            chinchilla.WebPage.Source.Should().Contain("name=\"Control3\" type=\"text\" value=\"hello world2\" id=\"Control3\" />");
        }
    }
}
