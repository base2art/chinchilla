﻿namespace Base2art.MockServer
{
    using System.Web;

    public static class HttpContextExtensions
    {
        private const string Key = "SjY_HANDLER_HACK_UNSAFE";

        public static ContextExtensionData GetExtensionData(this HttpContext httpContext)
        {
            return (ContextExtensionData)httpContext.Items[Key];
        }

        public static void SetExtensionData(this HttpContext httpContext, ContextExtensionData extensionData)
        {
            httpContext.Items[Key] = extensionData;
        }

        public static void ClearExtensionData(this HttpContext httpContext)
        {
            if (httpContext.Items.Contains("SjY_HANDLER_HACK_UNSAFE"))
            {
                httpContext.Items.Remove("SjY_HANDLER_HACK_UNSAFE");
            }
        }
    }
}