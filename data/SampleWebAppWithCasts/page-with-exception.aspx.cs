﻿namespace SampleWebAppWithCasts
{
    using System;
    using System.Web;

    public partial class PageWithException : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            throw new HttpUnhandledException("TESTING");
        }
    }
}