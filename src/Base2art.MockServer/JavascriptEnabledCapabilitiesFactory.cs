﻿namespace Base2art.MockServer
{
    using System;
    using System.Collections;
    using System.Web;
    using System.Web.Configuration;
    using System.Web.UI;

    using Base2art.MockServer.Util;
    using Base2art.MockServer.Util.Reflection;

    [Serializable]
    public class JavascriptEnabledCapabilitiesFactory 
        : ICreator<HttpBrowserCapabilities>
    {
        public HttpBrowserCapabilities Create()
        {
            var capabilities = new HttpBrowserCapabilities();

            capabilities.SetProperty("_havePreferredRenderingMime", "_preferredRenderingMime", "text/html");
            capabilities.SetProperty("_havePreferredResponseEncoding", "_preferredResponseEncoding", "utf-8");
            capabilities.SetProperty("_havePreferredRequestEncoding", "_preferredRequestEncoding", "utf-8");
            capabilities.SetProperty("_haveSupportsMaintainScrollPositionOnPostback", "_supportsMaintainScrollPositionOnPostback", false);

            capabilities.SetProperty("_havew3cdomversion", "_w3cdomversion", new Version(4, 0));
            capabilities.SetProperty("_haveecmascriptversion", "_ecmascriptversion", new Version(5, 0));
            capabilities.SetProperty("_havejscriptversion", "_jscriptversion", new Version(5, 0));

            capabilities.SetProperty("_havecookies", "_cookies", true);
            capabilities.SetProperty("_havetables", "_tables", true);
            capabilities.SetProperty("_havetagwriter", "_tagwriter", typeof(HtmlTextWriter));
            capabilities.SetField(typeof(HttpCapabilitiesBase), "_items", new Hashtable());
            return capabilities;
        }
    }
}

////            if (parameters.JavascriptDisabled)
////            {
////                capabilities.SetProperty("_havew3cdomversion", "_w3cdomversion", new Version(0, 0));
////                capabilities.SetProperty("_haveecmascriptversion", "_ecmascriptversion", new Version(0, 0));
////                capabilities.SetProperty("_havejscriptversion", "_jscriptversion", new Version(0, 0));
////            }
////            else
////            {
////            }