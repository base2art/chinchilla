﻿namespace Base2art.WebRunner.Server
{
	using System;
    using System.IO;
	using System.Linq;

	public static class StreamReader
	{
		public static byte[] ReadFully(this Stream stream)
		{
            var memoryStream = stream as MemoryStream;
            return memoryStream != null 
                ? memoryStream.ToArray() 
                : ReadFullyInternal(stream);
		}
		
		private static byte[] ReadFullyInternal(Stream input)
		{
			byte[] buffer = new byte[16 * 1024];
			using (var ms = new MemoryStream())
			{
				int read;
				while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
				{
					ms.Write(buffer, 0, read);
				}
				
				return ms.ToArray();
			}
		}
	}
}


