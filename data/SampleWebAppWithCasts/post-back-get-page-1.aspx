﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="post-back-get-page-1.aspx.cs" Inherits="SampleWebAppWithCasts.post_back_get_page_1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" method="GET" action="post-back-page-2.aspx?testing=1" >
    <div>
        <div class="control-1">
            <input type="text" name="first_name"/>
        </div>
        <div class="post-back-button">
            <input type="submit" name="my_button" value="My Text"/>
        </div>
    </div>
    </form>
</body>
</html>
