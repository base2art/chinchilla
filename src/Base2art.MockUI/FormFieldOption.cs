﻿namespace Base2art.MockUI
{
    public class FormFieldOption : IFormFieldOption
    {
        //public string Name { get; set; }

        public string Value { get; set; }

        public string ValueText { get; set; }

        public string PathToElement { get; set; }
    }
}