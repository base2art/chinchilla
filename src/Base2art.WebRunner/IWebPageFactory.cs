﻿namespace Base2art.WebRunner
{
    using Base2art.MockUI;

    public interface IWebPageFactory
    {
        IWebPage Create(LocatorType defaultLocatorType, IMockBrowser mockBrowser);
    }
}