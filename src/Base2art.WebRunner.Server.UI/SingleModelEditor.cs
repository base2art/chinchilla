﻿

namespace Base2art.WebRunner.Server.UI
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Windows.Forms;
    using Base2art.MockServer;
    using Base2art.Serialization;
    
    public partial class SingleModelEditor : UserControl
    {
        private readonly List<ConfigurableSiteData> sites = new List<ConfigurableSiteData>();
        
        public SingleModelEditor()
        {
            //
            // The InitializeComponent() call is required for Windows Forms designer support.
            //
            this.InitializeComponent();
            
        }
        
        public IEnumerable<ConfigurableSiteData> Sites
        {
            get
            { 
                var serializer = new NewtonsoftSerializer();
                Func<ConfigurableSiteData, ConfigurableSiteData> clone = (x) =>
                {
                    try
                    {
                        string text = serializer.Serialize<ConfigurableSiteData>(x);
                        return serializer.Deserialize<ConfigurableSiteData>(text);
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e);
                        return x;
                    }
                };
                return this.sites.Select(x => clone(x));
            }
            
            set
            {
                this.sites.Clear();
                this.sites.AddRange(value);
                this.bindingSource1.DataSource = this.sites;
                this.bindingSource1.ResetBindings(false);
            }
        }
        
        private void AddSiteButtonClick(object sender, EventArgs e)
        {
            var items = this.sites;
            items.Add(new ConfigurableSiteData
                {
                    Name = "New Site",
                    ServerData = new ServerData(),
                    WebServerData = new WebServerData(),
                });
            this.bindingSource1.DataSource = items;
            this.bindingSource1.ResetBindings(false);
            
            if (this.sites.Count == 1)
            {
                this.siteChooser.SelectedIndex = 0;
            }
        }
        
        private void RemoveSiteButtonClick(object sender, EventArgs e)
        {
            if (this.siteChooser.SelectedIndex >= 0)
            {
                this.sites.RemoveAt(this.siteChooser.SelectedIndex);
            }
            
            this.bindingSource1.DataSource = this.sites;
            this.bindingSource1.ResetBindings(false);
        }
        
        private void SiteChooserSelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.siteChooser.SelectedIndex >= 0)
            {
                this.propertyGrid.SelectedObject = this.sites[this.siteChooser.SelectedIndex];
                this.propertyGrid.ExpandAllGridItems();
            }
        }
    }
}
