﻿namespace Base2art.MockServer.Features.ServerControl1Fetaure
{
    using System;
    using System.Globalization;
    using System.Linq;
    using System.Web;
    using System.Web.UI;

    using Base2art.MockServer;
    using Base2art.MockServer.Features;
    using Base2art.MockServer.Util;

    using FluentAssertions;

    using NUnit.Framework;

    using WebControlLibrary.UI;
    using WebControlLibrary.UI.Controls;

    [TestFixture]
    public class PageRenderingFeature
    {
        [Test]
        public void ShouldGetPageWithCodeBehind()
        {
            using (var server = new MockServer(ServerDataFactory.Create()))
            {
                var rezult = server.RequestPage(new PageData
                {
                    Path = "Default.aspx",
                    QueryString = "qsValue=42"
                });

                var str = DateTime.UtcNow.Date.ToString(CultureInfo.InvariantCulture);
                rezult.RawText.Should().Contain("--STARTER--" + str + "--ENDER--");
            }
        }

        [Test]
        public void ShouldGetAspxPageInFolder()
        {
            using (var server = new MockServer(ServerDataFactory.Create()))
            {
                var rezult = server.RequestPage(new PageData
                {
                    Path = "level-two/simple-page.aspx",
                });

                rezult.RawText.Should().Contain("NULL CONTENT");
            }
        }

        [Test]
        public void ShouldGetPageThatThrowsException()
        {
            using (var server = new MockServer(ServerDataFactory.Create()))
            {
                var exception = new Action(
                    () =>
                    {
                        server.RequestPage(new PageData { Path = "page-with-exception.aspx" });
                    })
                    .ShouldThrow<ServerErrorException>();
                exception.And.InnerException.GetType().Should().Be(typeof(HttpUnhandledException));
                exception.And.InnerException.Message.Should().Be("TESTING");
            }
        }

        [Test]
        public void ShouldGetPageThatUsesSessionSamePage()
        {
            using (var server = new MockServer(ServerDataFactory.Create()))
            {
                var rezult = server.RequestPage(new PageData { Path = "session-sample.aspx" });
                rezult.RawText.Should().Contain("----");
                rezult = server.RequestPage(new PageData { Path = "session-sample.aspx", Cookies = rezult.Cookies.Values.ToArray() });
                rezult.RawText.Should().Contain("--World--");
            }
        }

        [Test]
        public void ShouldGetPageThatUsesSessionDifferentPage()
        {
            using (var server = new MockServer(ServerDataFactory.Create()))
            {
                var rezult = server.RequestPage(new PageData { Path = "session-sample.aspx" });
                rezult.RawText.Should().Contain("----");
                rezult = server.RequestPage(new PageData { Path = "session-sample-page2.aspx", Cookies = rezult.Cookies.Values.ToArray() });
                rezult.RawText.Should().Contain("--World--");
            }
        }

        [Test]
        public void ShouldGetPageThatUsesUrlRewrite()
        {
            using (var server = new MockServer(ServerDataFactory.Create()))
            {
                var rezult = server.RequestPage(new PageData
                {
                    Path = "rewrite-test",
                });
                rezult.RawText.Should().Contain("REWRITE-DEST");
                rezult.RawText.Should().Contain("url: http://127.0.0.1/rewrite-test");
                rezult.RawText.Should().Contain("raw-url: /rewrite-test");
            }
        }

        [Test]
        public void ShouldGetControWithBases()
        {
            using (var server = new MockServer(ServerDataFactory.Create()))
            {
                var rezult = server.RequestControl(new ControlData<ServerControl1>
                {
                    Path = "Default.aspx",
                    QueryString = "qsValue=42",
                    Control = new ControlCreator<ServerControl1>(() => new ServerControl1()),
                    Page = new PageCreator<Page>(() => new PageBase()),
                    PlaceInsideForm = false
                });

                rezult.RawText.Should().Be("<span>--s--42--e--[]</span>");
            }
        }

        [Test]
        public void ShouldGetControWithRamPage()
        {
            using (var server = new MockServer(ServerDataFactory.Create()))
            {
                var rezult = server.RequestControl(new ControlData<ServerControl2>
                {
                    Path = "Default.aspx",
                    QueryString = "qsValue=42",
                    Control = new ControlCreator<ServerControl2>(() => new ServerControl2()),
                    Page = new PageCreator<Page>(() => new TempPageBase()),
                    PlaceInsideForm = false
                });

                rezult.RawText.Should().Be("<span>--st--42--ln--[]</span>");
            }
        }

        [Test]
        public void ShouldGetControWithAspxPage2()
        {
            using (var server = new MockServer(ServerDataFactory.Create()))
            {
                var rezult = server.RequestPage(new PageData
                {
                    Path = "ab-impl.aspx",
                    QueryString = "qsValue=42",
                });

                rezult.RawText.Should().Contain("NULL CONTENT");
            }
        }

        [Test]
        public void ShouldGetControWithAspxPage()
        {
            using (var server = new MockServer(ServerDataFactory.Create()))
            {
                var rezult = server.RequestControl(new ControlData<ServerControl2>
                {
                    Path = "ab-impl.aspx",
                    QueryString = "qsValue=42",
                    Control = new ControlCreator<ServerControl2>(() => new ServerControl2()),
                    PageType = PageType.DiskPath,
                    PlaceInsideForm = true
                });

                rezult.RawText.Should().Contain("<span>--st--42--ln--[]</span>");
            }
        }

        [Test]
        public void ShouldGetMvcPage()
        {
            using (var server = new MockServer(ServerDataFactory.CreateMvc()))
            {
                var rezult = server.RequestPage(new PageData { Path = "/Home/About" });

                rezult.NormalizedText.Should().Contain("<h1>About.</h1> <h2>Your app description page.</h2>");
            }
        }

        private class TempPageBase : AbstractPageBase
        {
            protected override string DoGetSomeAbstractValue()
            {
                return "667";
            }
        }
    }
}
