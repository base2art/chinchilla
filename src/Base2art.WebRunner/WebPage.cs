﻿namespace Base2art.WebRunner
{
    public class WebPage
    {
       public static string Source 
       {
           get
           {
               return Chinchilla.WebPage.Source;
           }
       }

       public static LocatorType DefaultLocatorType
       {
           get
           {
               return Chinchilla.WebPage.DefaultLocatorType;
           }
       }

       public static void Visit(string path)
       {
           Chinchilla.WebPage.Visit(path);
       }

       public static IScopeContext Within(string locator)
       {
           return Chinchilla.WebPage.Within(locator);
       }

       public static IScopeContext Within(LocatorType locatorType, string locator)
       {
           return Chinchilla.WebPage.Within(locatorType, locator);
       }

       public static void TriggerPostBack(string locator)
       {
           Chinchilla.WebPage.TriggerPostBack(locator);
       }

       public static void TriggerPostBack(LocatorType locatorType, string locator)
       {
           Chinchilla.WebPage.TriggerPostBack(locatorType, locator);
       }

       ////public static  void AttachFile(string selector, string path);

       public static void Check(string locator)
       {
           Chinchilla.WebPage.Check(locator);
       }

       public static void Check(LocatorType locatorType, string locator)
       {
           Chinchilla.WebPage.Check(locatorType, locator);
       }

       public static void Choose(string locator)
       {
           Chinchilla.WebPage.Choose(locator);
       }

       public static void Choose(LocatorType locatorType, string locator)
       {
           Chinchilla.WebPage.Choose(locatorType, locator);
       }

       public static void ClickButton(string locator)
       {
           Chinchilla.WebPage.ClickButton(locator);
       }

       public static void ClickButton(LocatorType locatorType, string locator)
       {
           Chinchilla.WebPage.ClickButton(locatorType, locator);
       }

       public static void ClickLink(string locator)
       {
           Chinchilla.WebPage.ClickLink(locator);
       }

       public static void ClickLink(LocatorType locatorType, string locator)
       {
           Chinchilla.WebPage.ClickLink(locatorType, locator);
       }

        ////void click_link_or_button(string selector);

       public static void FillIn(string locator, string with)
       {
           Chinchilla.WebPage.FillIn(locator, with);
       }

       public static void FillIn(LocatorType locatorType, string locator, string with)
       {
           Chinchilla.WebPage.FillIn(locatorType, locator, with);
       }

       public static void Select(string locator, string with)
       {
           Chinchilla.WebPage.Select(locator, with);
       }

       public static void Select(LocatorType locatorType, string locator, string with)
       {
           Chinchilla.WebPage.Select(locatorType, locator, with);
       }

       public static void Uncheck(string locator)
       {
           Chinchilla.WebPage.Uncheck(locator);
       }

       public static void Uncheck(LocatorType locatorType, string locator)
       {
           Chinchilla.WebPage.Uncheck(locatorType, locator);
       }

       public static IElement FindElement(string locator)
       {
           return Chinchilla.WebPage.FindElement(locator);
       }

       public static IElement FindElement(LocatorType locatorType, string locator)
       {
           return Chinchilla.WebPage.FindElement(locatorType, locator);
       }

       public static IElement FindFirstElement(string locator)
       {
           return Chinchilla.WebPage.FindFirstElement(locator);
       }

       public static IElement FindFirstElement(LocatorType locatorType, string locator)
       {
           return Chinchilla.WebPage.FindFirstElement(locatorType, locator);
       }

       public static IElement[] FindElements(string locator)
       {
           return Chinchilla.WebPage.FindElements(locator);
       }

       public static IElement[] FindElements(LocatorType locatorType, string locator)
       {
           return Chinchilla.WebPage.FindElements(locatorType, locator);
       }
    }
}