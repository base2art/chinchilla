﻿namespace Base2art.WebRunner.CsQuery.Features
{
    using Base2art.MockServer.Features;
    using Base2art.MockUI;
    using Base2art.MockUI.CsQuery;

    using FluentAssertions;

    using NUnit.Framework;

    [TestFixture]
    public class ButtonApiFeature
    {
        [TearDown]
        public void DisposeChinchilla()
        {
            var chinchilla = Chinchilla.Instance;
            if (chinchilla != null)
            {
                chinchilla.Dispose();
            }
        }

        [Test]
        public void ShouldClickButtonByCss()
        {
            var mockBrowser = new MockBrowser(new CsQueryFormParser(), new MockServer.MockServer(ServerDataFactory.Create()));
            IChinchilla chinchilla = Chinchilla.SetInstance(mockBrowser, new WebPageFactory());

            chinchilla.WebPage.Visit("fluent-post-back/buttons.aspx");

            chinchilla.WebPage.Source.Should().NotContain("Button 1 Clicked");
            chinchilla.WebPage.Source.Should().NotContain("Button 2 Clicked");

            chinchilla.WebPage.ClickButton(LocatorType.Css, ".control-2 input");
            chinchilla.WebPage.Source.Should().NotContain("Button 1 Clicked");
            chinchilla.WebPage.Source.Should().Contain("Button 2 Clicked");
        }

        [Test]
        public void ShouldClickButtonByLabel()
        {
            var mockBrowser = new MockBrowser(new CsQueryFormParser(), new MockServer.MockServer(ServerDataFactory.Create()));
            IChinchilla chinchilla = Chinchilla.SetInstance(mockBrowser, new WebPageFactory());

            chinchilla.WebPage.Visit("fluent-post-back/buttons.aspx");

            chinchilla.WebPage.Source.Should().NotContain("Button 1 Clicked");
            chinchilla.WebPage.Source.Should().NotContain("Button 2 Clicked");

            chinchilla.WebPage.ClickButton("Button 2");
            chinchilla.WebPage.Source.Should().NotContain("Button 1 Clicked");
            chinchilla.WebPage.Source.Should().Contain("Button 2 Clicked");
        }

        [Test]
        public void ShouldClickButtonByPath()
        {
            var mockBrowser = new MockBrowser(new CsQueryFormParser(), new MockServer.MockServer(ServerDataFactory.Create()));
            IChinchilla chinchilla = Chinchilla.SetInstance(mockBrowser, new WebPageFactory());

            chinchilla.WebPage.Visit("fluent-post-back/buttons.aspx");

            chinchilla.WebPage.Source.Should().NotContain("Button 1 Clicked");
            chinchilla.WebPage.Source.Should().NotContain("Button 2 Clicked");

            chinchilla.WebPage.ClickButton(LocatorType.Path, "//form/div/div[2]/input");
            chinchilla.WebPage.Source.Should().NotContain("Button 1 Clicked");
            chinchilla.WebPage.Source.Should().Contain("Button 2 Clicked");
        }

        [Test]
        public void ShouldRespectViewState()
        {
            var mockBrowser = new MockBrowser(new CsQueryFormParser(), new MockServer.MockServer(ServerDataFactory.Create()));
            IChinchilla chinchilla = Chinchilla.SetInstance(mockBrowser, new WebPageFactory());

            chinchilla.WebPage.Visit("fluent-post-back/buttons.aspx");

            chinchilla.WebPage.Source.Should().NotContain("Button 1 Clicked");
            chinchilla.WebPage.Source.Should().NotContain("Button 2 Clicked");

            chinchilla.WebPage.ClickButton(LocatorType.Css, ".control-2 input");
            chinchilla.WebPage.Source.Should().NotContain("Button 1 Clicked");
            chinchilla.WebPage.Source.Should().Contain("Button 2 Clicked");

            chinchilla.WebPage.ClickButton(LocatorType.Css, ".control-1 input");
            chinchilla.WebPage.Source.Should().Contain("Button 1 Clicked");
            chinchilla.WebPage.Source.Should().Contain("Button 2 Clicked");
        }
    }
}
