﻿namespace Base2art.WebRunner.CsQuery
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using System.Net.Http;
    using Base2art.MockServer;
    using Base2art.MockServer.Util.Converters;
    using Base2art.MockUI;

    public static class FormHelper
    {
        public static IDomObjectWrapper VerifyOneMatch(this IDomObjectWrapper[] matchingItems, string locator, string pageUrl)
        {
            if (matchingItems.Length == 0)
            {
                throw new NoMatchFoundException("Element with locator '" + locator + "' not found on page '" + pageUrl + "'");
            }

            if (matchingItems.Length > 1)
            {
                throw new AmbiguousMatchFoundException("Multiple Elements with locator '" + locator + "' not found on page '" + pageUrl + "'");
            }

            return matchingItems[0];
        }

        public static void VerifyOneMatch(this IDomObjectWrapper[] matchingItems, string locator, string pageUrl, Action<IDomObjectWrapper> action)
        {
            matchingItems.VerifyOneMatch(locator, pageUrl);
            action(matchingItems[0]);
        }

        public static void FillIn(this CsQueryWebPage pageApi, IDomObjectWrapper domObject, string with)
        {
            var form = domObject.Form;
            pageApi.BackingValues[form.PathToElement][domObject.Name] = with;
        }

        public static void Select(this CsQueryWebPage webPage, IDomObjectWrapper x, string with)
        {
            var form = x.Form;
            var formPath = form.PathToElement;
            var formData = webPage.CurrentPage.FormData;
            const StringComparison Comp = StringComparison.InvariantCultureIgnoreCase;
            var targtForm = formData.First(y => string.Equals(y.PathToElement, formPath, Comp));
            var foundFieldPath = x.PathToElement;
            var optionConatiner = targtForm.FirstOrDefault(y => string.Equals(y.PathToElement, foundFieldPath, Comp));

            var options = optionConatiner == null ? new IFormFieldOption[0] : optionConatiner.Options;

            var option = options.FirstOrDefault(y => y.Value == with);
            if (option == null)
            {
                option = options.FirstOrDefault(y => y.ValueText == with);
            }

            if (option == null)
            {
                throw new NoMatchFoundException("Select does not contain value or text: '" + with + "'");
            }

            webPage.BackingValues[formPath][x.Name] = option.Value;
        }

        public static void Check(this CsQueryWebPage pageApi, IDomObjectWrapper domObject)
        {
            var form = domObject.Form;
            pageApi.BackingValues[form.PathToElement][domObject.Name] = "on";
        }

        public static void Uncheck(this CsQueryWebPage pageApi, IDomObjectWrapper domObject)
        {
            var form = domObject.Form;
            pageApi.BackingValues[form.PathToElement][domObject.Name] = string.Empty;
        }

        public static void Choose(this CsQueryWebPage pageApi, IDomObjectWrapper domObject)
        {
            var form = domObject.Form;
            pageApi.BackingValues[form.PathToElement][domObject.Name] = domObject.Value;
        }

        public static void ClickButton(this CsQueryWebPage webPage, IDomObjectWrapper button)
        {
            var form = button.Form;
            var formData = webPage.BackingValues[form.PathToElement];
            formData[button.Name] = button.Value;
            TriggerPostBack(webPage, button);
        }

        public static void TriggerPostBack(this CsQueryWebPage webPage, IDomObjectWrapper button)
        {
            var form = button.Form;
            var postData = webPage.BackingValues[form.PathToElement];
            if (form.Method == HttpMethod.Get)
            {
                string queryString = postData.ToQueryString();
                webPage.Navigate(Relativize(webPage, form.Action), queryString, new Dictionary<string, string>());
            }
            else
            {
                string queryString = webPage.QueryString(form.Action);
                webPage.Navigate(Relativize(webPage, form.Action), queryString, postData);
            }
        }

        public static void ClickLink(this CsQueryWebPage webPage, IDomObjectWrapper link)
        {
            var href = link["href"];
            if (string.IsNullOrWhiteSpace(href))
            {
                // ReSharper disable NotResolvedInText
                throw new ArgumentNullException("href", "Navigating to a page that has no href");
                // ReSharper restore NotResolvedInText
            }

            webPage.Navigate(new PageData
                                 {
                                     Path = Relativize(webPage, href),
                                     QueryString = webPage.QueryString(href)
                                 });
        }

        private static string Relativize(CsQueryWebPage webPage, string href)
        {
            var uriBuilder = new UriBuilder("http://127.0.0.1/");
            uriBuilder.Path = webPage.CurrentPage.Path;
            var newUri = new Uri(uriBuilder.Uri, href);
            var output = new UriBuilder(newUri);
            return output.Path;
        }
    }
}