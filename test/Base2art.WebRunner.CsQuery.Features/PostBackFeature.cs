﻿namespace Base2art.WebRunner.CsQuery.Features
{
    using Base2art.MockServer.Features;
    using Base2art.MockUI;
    using Base2art.MockUI.CsQuery;

    using FluentAssertions;

    using NUnit.Framework;

    [TestFixture]
    public class PostBackFeature
    {
        [TearDown]
        public void DisposeChinchilla()
        {
            var chinchilla = Chinchilla.Instance;
            if (chinchilla != null)
            {
                chinchilla.Dispose();
            }
        }

        [Test]
        public void ShouldFollowLinkWithQueryString()
        {
            var mockBrowser = new MockBrowser(new CsQueryFormParser(), new MockServer.MockServer(ServerDataFactory.Create()));
            IChinchilla chinchilla = Chinchilla.SetInstance(mockBrowser, new WebPageFactory());

            chinchilla.WebPage.Visit("post-back-page-1.aspx");
            chinchilla.WebPage.ClickLink("Page 2");
            chinchilla.WebPage.Source.Should().Contain("PostBack testing: YES!");
        }

        [Test]
        public void ShouldVisitWithQueryString()
        {
            var mockBrowser = new MockBrowser(new CsQueryFormParser(), new MockServer.MockServer(ServerDataFactory.Create()));
            IChinchilla chinchilla = Chinchilla.SetInstance(mockBrowser, new WebPageFactory());

            chinchilla.WebPage.Visit("post-back-page-2.aspx?testing=NO");
            chinchilla.WebPage.Source.Should().Contain("PostBack testing: NO!");
        }

        [Test]
        public void ShouldAutoPostback()
        {
            var mockBrowser = new MockBrowser(new CsQueryFormParser(), new MockServer.MockServer(ServerDataFactory.Create()));
            IChinchilla chinchilla = Chinchilla.SetInstance(mockBrowser, new WebPageFactory());

            chinchilla.WebPage.Visit("fluent-post-back/autopostback-dropdown.aspx");
            chinchilla.WebPage.Select(LocatorType.Css, "select", "B");
            chinchilla.WebPage.TriggerPostBack(LocatorType.Css, "select");
            chinchilla.WebPage.Source.Should().Contain("Value: B!");
        }

        [Test]
        public void ShouldPostbackToFormUrl()
        {
            var mockBrowser = new MockBrowser(new CsQueryFormParser(), new MockServer.MockServer(ServerDataFactory.Create()));
            IChinchilla chinchilla = Chinchilla.SetInstance(mockBrowser, new WebPageFactory());

            chinchilla.WebPage.Visit("post-back-page-1.aspx");
            chinchilla.WebPage.FillIn(LocatorType.Css, ".control-1 input", "Base2");
            chinchilla.WebPage.ClickButton(LocatorType.Css, ".post-back-button input");
            chinchilla.WebPage.Source.Should().Contain("PostBack first_name: Base2!");
            chinchilla.WebPage.Source.Should().Contain("PostBack button: My Text!");
            chinchilla.WebPage.Source.Should().Contain("PostBack testing: 1!");
        }

        [Test]
        public void ShouldPostbackToGetFormUrl()
        {
            var mockBrowser = new MockBrowser(new CsQueryFormParser(), new MockServer.MockServer(ServerDataFactory.Create()));
            IChinchilla chinchilla = Chinchilla.SetInstance(mockBrowser, new WebPageFactory());

            chinchilla.WebPage.Visit("post-back-get-page-1.aspx");
            chinchilla.WebPage.FillIn(LocatorType.Css, ".control-1 input", "Base2");
            chinchilla.WebPage.ClickButton(LocatorType.Css, ".post-back-button input");
            chinchilla.WebPage.Source.Should().Contain("PostBack first_name: Base2!");
            chinchilla.WebPage.Source.Should().Contain("PostBack button: My Text!");
            chinchilla.WebPage.Source.Should().NotContain("PostBack testing: 1!");
        }
    }
}
