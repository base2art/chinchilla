﻿using System;

namespace Base2art.WebRunner.Server.Data
{
    public class HealthCheckContainer
    {
        private readonly Guid id;

        private readonly HealthCheck check;
        
        public HealthCheckContainer(Guid id, HealthCheck check)
        {
            this.check = check;
            this.id = id;
        }

        public Guid Id
        {
            get { return this.id; }
        }
        
        public HealthCheck Check
        {
            get { return this.check; }
        }
        
        public DateTimeOffset LastCheck { get; set; }
    }
}
