﻿namespace Base2art.WebRunner
{
    using System;
    using System.Collections.Generic;

    public abstract class WebPageBase : IWebPage
    {
        private readonly LocatorType defaultLocatorType;

        private readonly Stack<IScopeContext> scopes = new Stack<IScopeContext>();

        protected WebPageBase(LocatorType defaultLocatorType)
        {
            this.defaultLocatorType = defaultLocatorType;
        }

        string IWebPage.Source
        {
            get
            {
                return this.SourceInternal;
            }
        }

        LocatorType IWebPageAction.DefaultLocatorType
        {
            get
            {
                return this.DefaultLocatorTypeInternal;
            }
        }

        protected LocatorType DefaultLocatorTypeInternal
        {
            get
            {
                return this.defaultLocatorType;
            }
        }

        protected abstract string SourceInternal { get; }

        public void RemoveScope(IScopeContext scopeContext)
        {
            if (this.scopes.Peek() != scopeContext)
            {
                throw new InvalidOperationException("This is a development Error: You Exited Scopes Unevenly");
            }

            this.scopes.Pop();
        }

        void IWebPage.Visit(string path)
        {
            this.DoVisit(path);
        }

        IScopeContext IWebPage.Within(string locator)
        {
            return this.DoWithin(this.DefaultLocatorTypeInternal, locator);
        }

        IScopeContext IWebPage.Within(LocatorType locatorType, string locator)
        {
            return this.DoWithin(locatorType, locator);
        }

        void IWebPageAction.TriggerPostBack(string locator)
        {
            this.DoTriggerPostBack(this.DefaultLocatorTypeInternal, locator);
        }

        void IWebPageAction.TriggerPostBack(LocatorType locatorType, string locator)
        {
            this.DoTriggerPostBack(locatorType, locator);
        }

        void IWebPageAction.FillIn(string locator, string with)
        {
            this.DoFillIn(this.DefaultLocatorTypeInternal, locator, with);
        }

        void IWebPageAction.FillIn(LocatorType locatorType, string locator, string with)
        {
            this.DoFillIn(locatorType, locator, with);
        }

        void IWebPageAction.Select(string locator, string with)
        {
            this.DoSelect(this.DefaultLocatorTypeInternal, locator, with);
        }

        void IWebPageAction.Select(LocatorType locatorType, string locator, string with)
        {
            this.DoSelect(locatorType, locator, with);
        }

        void IWebPageAction.Check(string locator)
        {
            this.DoCheck(this.DefaultLocatorTypeInternal, locator);
        }

        void IWebPageAction.Check(LocatorType locatorType, string locator)
        {
            this.DoCheck(locatorType, locator);
        }

        void IWebPageAction.Uncheck(string locator)
        {
            this.DoUncheck(this.DefaultLocatorTypeInternal, locator);
        }

        void IWebPageAction.Uncheck(LocatorType locatorType, string locator)
        {
            this.DoUncheck(locatorType, locator);
        }

        void IWebPageAction.Choose(string locator)
        {
            this.DoChoose(this.DefaultLocatorTypeInternal, locator);
        }

        void IWebPageAction.Choose(LocatorType locatorType, string locator)
        {
            this.DoChoose(locatorType, locator);
        }

        void IWebPageAction.ClickButton(string locator)
        {
            this.DoClickButton(this.DefaultLocatorTypeInternal, locator);
        }

        void IWebPageAction.ClickButton(LocatorType locatorType, string locator)
        {
            this.DoClickButton(locatorType, locator);
        }

        void IWebPageAction.ClickLink(string locator)
        {
            this.DoClickLink(this.DefaultLocatorTypeInternal, locator);
        }

        void IWebPageAction.ClickLink(LocatorType locatorType, string locator)
        {
            this.DoClickLink(locatorType, locator);
        }

        IElement IWebPageFinder.FindElement(string locator)
        {
            return this.DoFindElement(this.DefaultLocatorTypeInternal, locator);
        }

        IElement IWebPageFinder.FindElement(LocatorType locatorType, string locator)
        {
            return this.DoFindElement(locatorType, locator);
        }

        IElement IWebPageFinder.FindFirstElement(string locator)
        {
            return this.DoFindFirstElement(this.DefaultLocatorTypeInternal, locator);
        }

        IElement IWebPageFinder.FindFirstElement(LocatorType locatorType, string locator)
        {
            return this.DoFindFirstElement(locatorType, locator);
        }

        IElement[] IWebPageFinder.FindElements(string locator)
        {
            return this.DoFindElements(this.DefaultLocatorTypeInternal, locator);
        }

        IElement[] IWebPageFinder.FindElements(LocatorType locatorType, string locator)
        {
            return this.DoFindElements(locatorType, locator);
        }

        protected void DoTriggerPostBack(LocatorType locatorType, string locator)
        {
            this.GetInteracter(locatorType, this.GetCurrentScope()).TriggerPostBack(locator);
        }

        protected void DoFillIn(LocatorType locatorType, string locator, string with)
        {
            this.GetInteracter(locatorType, this.GetCurrentScope(), "input", "textarea").FillIn(locator, with);
        }

        protected void DoSelect(LocatorType locatorType, string locator, string with)
        {
            this.GetInteracter(locatorType, this.GetCurrentScope(), "select").Select(locator, with);
        }

        protected void DoCheck(LocatorType locatorType, string locator)
        {
            this.GetInteracter(locatorType, this.GetCurrentScope(), "input").Check(locator);
        }

        protected void DoUncheck(LocatorType locatorType, string locator)
        {
            this.GetInteracter(locatorType, this.GetCurrentScope(), "input").Uncheck(locator);
        }

        protected void DoChoose(LocatorType locatorType, string locator)
        {
            this.GetInteracter(locatorType, this.GetCurrentScope(), "input").Choose(locator);
        }

        protected void DoClickButton(LocatorType locatorType, string locator)
        {
            if (locatorType == LocatorType.Label)
            {
                this.CreateValueInteracter(this.GetCurrentScope()).ClickButton(locator);
                return;
            }

            this.GetInteracter(locatorType, this.GetCurrentScope()).ClickButton(locator);
        }

        protected void DoClickLink(LocatorType locatorType, string locator)
        {
            if (locatorType == LocatorType.Label)
            {
                this.CreateTextInteracter(this.GetCurrentScope(), "a").ClickLink(locator);
                return;
            }

            this.GetInteracter(locatorType, this.GetCurrentScope()).ClickLink(locator);
        }

        protected IScopeContext DoWithin(LocatorType locatorType, string locator)
        {
            var scopeContext = this.GetInteracter(locatorType, this.GetCurrentScope()).CreateScopeContext(locator);
            this.scopes.Push(scopeContext);
            return scopeContext;
        }

        protected IElement DoFindElement(LocatorType locatorType, string locator)
        {
            return this.GetInteracter(locatorType, this.GetCurrentScope(), "input").FindElement(locator);
        }

        protected IElement DoFindFirstElement(LocatorType locatorType, string locator)
        {
            return this.GetInteracter(locatorType, this.GetCurrentScope(), "input").FindFirstElement(locator);
        }

        protected IElement[] DoFindElements(LocatorType locatorType, string locator)
        {
            return this.GetInteracter(locatorType, this.GetCurrentScope(), "input").FindElements(locator);
        }

        protected abstract void DoVisit(string path);

        protected abstract IWebPageInteracter CreateLabelInteracter(
            IScopeContext scope, params string[] explicitlyMatchedNodes);

        protected abstract IWebPageInteracter CreateCssInteracter(
            IScopeContext scope, params string[] explicitlyMatchedNodes);

        protected abstract IWebPageInteracter CreatePathInteracter(
            IScopeContext scope, params string[] explicitlyMatchedNodes);

        protected abstract IWebPageInteracter CreateTextInteracter(
            IScopeContext scope, params string[] explicitlyMatchedNodes);

        protected abstract IWebPageInteracter CreateValueInteracter(
            IScopeContext scope, params string[] explicitlyMatchedNodes);

        private IWebPageInteracter GetInteracter(
            LocatorType locatorType, IScopeContext scope, params string[] explicitlyMatchedNodes)
        {
            switch (locatorType)
            {
                case LocatorType.Label:
                    return this.CreateLabelInteracter(scope, explicitlyMatchedNodes);
                case LocatorType.Css:
                    return this.CreateCssInteracter(scope, explicitlyMatchedNodes);
                case LocatorType.Path:
                    return this.CreatePathInteracter(scope, explicitlyMatchedNodes);
                default:
                    throw new ArgumentOutOfRangeException("locatorType", "Should Never Get here");
            }
        }

        private IScopeContext GetCurrentScope()
        {
            if (this.scopes.Count == 0)
            {
                return null;
            }

            return this.scopes.Peek();
        }
    }
}