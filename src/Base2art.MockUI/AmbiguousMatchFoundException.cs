﻿namespace Base2art.MockUI
{
    using System;
    using System.Runtime.Serialization;

    public class AmbiguousMatchFoundException : ApplicationException
    {
        public AmbiguousMatchFoundException()
        {
        }

        public AmbiguousMatchFoundException(string message)
            : base(message)
        {
        }

        public AmbiguousMatchFoundException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        protected AmbiguousMatchFoundException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}