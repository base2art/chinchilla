﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="text-boxes.aspx.cs"
    Inherits="SampleWebAppWithCasts.text_boxes" %>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <div class="control-1">
            <asp:Label ID="Label1" runat="server" AssociatedControlID="Control1">My First Control:</asp:Label>
            <asp:Label ID="Label2" runat="server" AssociatedControlID="Control1">My Second Control:</asp:Label>
            <asp:TextBox ID="Control1" runat="server"></asp:TextBox>
        </div>
        <div>
            <asp:TextBox ID="Control2" runat="server"></asp:TextBox>
        </div>
        <div class="control-1">
            <asp:Label ID="Label3" runat="server" AssociatedControlID="Control3">My Second Control:</asp:Label>
            <asp:TextBox ID="Control3" runat="server"></asp:TextBox>
        </div>
        <div class="post-back-button">
            <asp:Button runat="server" ID="SubmitButton" />
        </div>
    </div>
    </form>
</body>
</html>
