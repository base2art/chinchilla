﻿namespace Base2art.WebRunner
{
    public interface IWebPage : IWebPageAction, IWebPageFinder
    {
        string Source { get; }

        void Visit(string path);

        IScopeContext Within(string locator);

        IScopeContext Within(LocatorType locatorType, string locator);
    }
}