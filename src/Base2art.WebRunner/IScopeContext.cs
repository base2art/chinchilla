﻿namespace Base2art.WebRunner
{
    using System;

    public interface IScopeContext : IDisposable
    {
        void ExitScope();
    }
}