﻿namespace Base2art.WebRunner.CsQuery.Features
{
    using System;

    using Base2art.MockServer;
    using Base2art.MockServer.Features;
    using Base2art.MockUI;
    using Base2art.MockUI.CsQuery;

    using FluentAssertions;

    using NUnit.Framework;

    [TestFixture]
    public class SelectApiFeature
    {
        [TearDown]
        public void DisposeChinchilla()
        {
            var chinchilla = Chinchilla.Instance;
            if (chinchilla != null)
            {
                chinchilla.Dispose();
            }
        }

        [Test]
        public void ShouldHaveFluentApiByCssByValue()
        {
            // Setup normally done in an [SetupFixture] class
            // but I do it here for testability.
            // http://www.nunit.org/index.php?p=setupFixture&r=2.4
            var mockBrowser = new MockBrowser(new CsQueryFormParser(), new MockServer(ServerDataFactory.Create()));
            IChinchilla chinchilla = Chinchilla.SetInstance(mockBrowser, new WebPageFactory());

            chinchilla.WebPage.Visit("fluent-post-back/selects.aspx");
            chinchilla.WebPage.Select(LocatorType.Css, ".control-1 select", "2");
            chinchilla.WebPage.ClickButton(LocatorType.Css, ".post-back-button input");
            chinchilla.WebPage.Source.Should().Contain("<option selected=\"selected\" value=\"2\">B</option>");
        }

        [Test]
        public void ShouldHaveFluentApiByCssByText()
        {
            // Setup normally done in an [SetupFixture] class
            // but I do it here for testability.
            // http://www.nunit.org/index.php?p=setupFixture&r=2.4
            var mockBrowser = new MockBrowser(new CsQueryFormParser(), new MockServer(ServerDataFactory.Create()));
            IChinchilla chinchilla = Chinchilla.SetInstance(mockBrowser, new WebPageFactory());

            chinchilla.WebPage.Visit("fluent-post-back/selects.aspx");
            chinchilla.WebPage.Select(LocatorType.Css, ".control-1 select", "B");
            chinchilla.WebPage.ClickButton(LocatorType.Css, ".post-back-button input");
            chinchilla.WebPage.Source.Should().Contain("<option selected=\"selected\" value=\"2\">B</option>");
        }

        [Test]
        public void ShouldHaveFluentApiByCssNonExistantValueAndText()
        {
            // Setup normally done in an [SetupFixture] class
            // but I do it here for testability.
            // http://www.nunit.org/index.php?p=setupFixture&r=2.4
            var mockBrowser = new MockBrowser(new CsQueryFormParser(), new MockServer(ServerDataFactory.Create()));
            IChinchilla chinchilla = Chinchilla.SetInstance(mockBrowser, new WebPageFactory());

            chinchilla.WebPage.Visit("fluent-post-back/selects.aspx");
            new Action(() => chinchilla.WebPage.Select(LocatorType.Css, ".control-1 select", "HELLO")).ShouldThrow<NoMatchFoundException>();
        }

        [Test]
        public void ShouldHaveFluentApiByLabel()
        {
            // Setup normally done in an [SetupFixture] class
            // but I do it here for testability.
            // http://www.nunit.org/index.php?p=setupFixture&r=2.4
            var mockBrowser = new MockBrowser(new CsQueryFormParser(), new MockServer(ServerDataFactory.Create()));
            IChinchilla chinchilla = Chinchilla.SetInstance(mockBrowser, new WebPageFactory());

            chinchilla.WebPage.Visit("fluent-post-back/selects.aspx");
            chinchilla.WebPage.Select("My First Control:", "2");
            new Action(() => chinchilla.WebPage.Select("My Second Control:", "hello world3")).ShouldThrow<AmbiguousMatchFoundException>();
            new Action(() => chinchilla.WebPage.Select("My Non-Existant Control:", "hello world3")).ShouldThrow<NoMatchFoundException>();
            chinchilla.WebPage.ClickButton(LocatorType.Css, ".post-back-button input");
            chinchilla.WebPage.Source.Should().Contain("<option selected=\"selected\" value=\"2\">B</option>");
        }

        [Test]
        public void ShouldHaveFluentApiByPath()
        {
            // Setup normally done in an [SetupFixture] class
            // but I do it here for testability.
            // http://www.nunit.org/index.php?p=setupFixture&r=2.4
            var mockBrowser = new MockBrowser(new CsQueryFormParser(), new MockServer(ServerDataFactory.Create()));
            IChinchilla chinchilla = Chinchilla.SetInstance(mockBrowser, new WebPageFactory());

            chinchilla.WebPage.Visit("fluent-post-back/selects.aspx");
            chinchilla.WebPage.Select(LocatorType.Path, "//form/div/div[1]/select[1]", "B");
            chinchilla.WebPage.ClickButton(LocatorType.Css, ".post-back-button input");
            chinchilla.WebPage.Source.Should().Contain("<option selected=\"selected\" value=\"2\">B</option>");
        }
    }
}
