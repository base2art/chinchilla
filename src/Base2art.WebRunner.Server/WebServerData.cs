﻿namespace Base2art.WebRunner.Server
{
    using System;
    using Base2art.ComponentModel;
    using Base2art.Converters;

    [Serializable]
    public class WebServerData
    {
        private readonly ConfigurationValueProperty<short> port =
            new ConfigurationValueProperty<short>(new Int16Parser(), "Port", 7998);
        
        private string[] hosts;

        public string[] Hosts
        {
            get 
            {
                var hosts = this.hosts;
                if (hosts == null || hosts.Length == 0)
                {
                    hosts = new string[]{ "localhost" };
                }
                
                return hosts; 
            }
            
            set 
            {
                this.hosts = value; 
            }
        }

        public short Port
        {
            get { return this.port.Value; }
            set { this.port.Value = value; }
        }

        public bool UseSsl { get; set; }
    }
}