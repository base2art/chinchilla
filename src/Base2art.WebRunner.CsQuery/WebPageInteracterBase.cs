﻿namespace Base2art.WebRunner.CsQuery
{
    using System;
    using System.Linq;

    using Base2art.Collections;

    using global::CsQuery;

    public abstract class WebPageInteracterBase : IWebPageInteracter
    {
        private readonly CsQueryWebPage pageApi;

        private readonly IScopeContext scope;

        private readonly string[] explicitlyMatchedNodes;

        private readonly bool explicitMatchingEnabled;

        protected WebPageInteracterBase(CsQueryWebPage pageApi, IScopeContext scope, bool explicitMatchingEnabled, string[] explicitlyMatchedNodes)
        {
            this.pageApi = pageApi;
            this.scope = scope;
            this.explicitMatchingEnabled = explicitMatchingEnabled;
            this.explicitlyMatchedNodes = explicitlyMatchedNodes;
        }

        public CsQueryWebPage PageApi
        {
            get
            {
                return this.pageApi;
            }
        }

        protected string[] ExplicitlyMatchedNodes
        {
            get
            {
                return this.explicitlyMatchedNodes;
            }
        }

        protected IScopeContext Scope
        {
            get
            {
                return this.scope;
            }
        }

        protected IDomObject ScopedNode
        {
            get
            {
                if (this.Scope == null)
                {
                    return this.PageApi.CurrentPage.Document().Document;
                }

                var cqScope = this.Scope as CqScopeContext;

                if (cqScope == null)
                {
                    throw new InvalidOperationException("Developer Exception, Mixing Scope types");
                }

                return cqScope.ScopedNode;
            }
        }

        public IScopeContext CreateScopeContext(string locator)
        {
            var wrapper = this.FindMatchingElements(locator).VerifyOneMatch(locator, this.PageApi.CurrentPage.Path);
            var native = wrapper.NativeElement;
            if (native == null)
            {
                throw new InvalidOperationException("Developer Error Bad Native Element");
            }

            return new CqScopeContext(this.PageApi, (IDomElement)native.Get(0));
        }

        public void FillIn(string locator, string with)
        {
            this.FindMatchingElements(locator).ForAll(x => this.PageApi.FillIn(x, with));
        }

        public void Select(string locator, string with)
        {
            this.FindMatchingElements(locator).ForAll(x => this.PageApi.Select(x, with));
        }

        public void Check(string locator)
        {
            this.FindMatchingElements(locator).ForAll(x => this.PageApi.Check(x));
        }

        public void Uncheck(string locator)
        {
            this.FindMatchingElements(locator).ForAll(x => this.PageApi.Uncheck(x));
        }

        public void Choose(string locator)
        {
            this.FindMatchingElements(locator).ForAll(x => this.PageApi.Choose(x));
        }

        public void ClickButton(string locator)
        {
            this.FindMatchingElements(locator)
                .VerifyOneMatch(locator, this.PageApi.CurrentPage.Path, x => this.pageApi.ClickButton(x));
        }

        public void ClickLink(string locator)
        {
            this.FindMatchingElements(locator)
                .VerifyOneMatch(locator, this.PageApi.CurrentPage.Path, x => this.pageApi.ClickLink(x));
        }

        public void TriggerPostBack(string locator)
        {
            this.FindMatchingElements(locator)
                .VerifyOneMatch(locator, this.PageApi.CurrentPage.Path, x => this.pageApi.TriggerPostBack(x));
        }

        public IElement FindElement(string locator)
        {
            var element = this.FindMatchingElements(locator).VerifyOneMatch(locator, this.PageApi.CurrentPage.Path);
            if (element == null)
            {
                return null;
            }

            return new Element(element);
        }

        public IElement FindFirstElement(string locator)
        {
            return this.FindMatchingElements(locator).Select(x => new Element(x)).FirstOrDefault();
        }

        public IElement[] FindElements(string locator)
        {
            return this.FindMatchingElements(locator).Select(x => new Element(x)).ToArray();
        }

        protected IDomObjectWrapper[] FindMatchingElements(string locator)
        {
            return this.FindMatchingElements(locator, false);
        }

        protected IDomObjectWrapper[] FindMatchingElements(string locator, bool explicitMatchingForced)
        {
            var matchingItems = this.FindTargetElements(locator);

            if (explicitMatchingForced || this.explicitMatchingEnabled)
            {
                matchingItems.VerifyOneMatch(locator, this.PageApi.CurrentPage.Path);
            }

            return matchingItems;
        }

        protected abstract IDomObjectWrapper[] FindTargetElements(string locator);
    }
}