﻿namespace Base2art.WebRunner.CsQuery.Features
{
    using System;

    using Base2art.MockServer;
    using Base2art.MockServer.Features;
    using Base2art.MockUI;
    using Base2art.MockUI.CsQuery;

    using FluentAssertions;

    using NUnit.Framework;

    [TestFixture]
    public class CheckBoxUncheckApiFeature
    {
        [TearDown]
        public void DisposeChinchilla()
        {
            var chinchilla = Chinchilla.Instance;
            if (chinchilla != null)
            {
                chinchilla.Dispose();
            }
        }

        [Test]
        public void ShouldHaveFluentApiByCss()
        {
            // Setup normally done in an [SetupFixture] class
            // but I do it here for testability.
            // http://www.nunit.org/index.php?p=setupFixture&r=2.4
            var mockBrowser = new MockBrowser(new CsQueryFormParser(), new MockServer(ServerDataFactory.Create()));
            IChinchilla chinchilla = Chinchilla.SetInstance(mockBrowser, new WebPageFactory());

            chinchilla.WebPage.Visit("fluent-post-back/check-boxes-uncheck.aspx");
            chinchilla.WebPage.Uncheck(LocatorType.Css, ".control-1 input");
            chinchilla.WebPage.ClickButton(LocatorType.Css, ".post-back-button input");
            chinchilla.WebPage.Source.Should().Contain(" id=\"Control1\" type=\"checkbox\" name=\"Control1\" />");
            chinchilla.WebPage.Source.Should().Contain(" id=\"Control2\" type=\"checkbox\" name=\"Control2\" checked=\"checked\" />");
            chinchilla.WebPage.Source.Should().Contain(" id=\"Control3\" type=\"checkbox\" name=\"Control3\" />");
        }

        [Test]
        public void ShouldHaveFluentApiByLabel()
        {
            // Setup normally done in an [SetupFixture] class
            // but I do it here for testability.
            // http://www.nunit.org/index.php?p=setupFixture&r=2.4
            var mockBrowser = new MockBrowser(new CsQueryFormParser(), new MockServer(ServerDataFactory.Create()));
            IChinchilla chinchilla = Chinchilla.SetInstance(mockBrowser, new WebPageFactory());

            chinchilla.WebPage.Visit("fluent-post-back/check-boxes-uncheck.aspx");
            chinchilla.WebPage.Uncheck("My First Control:");
            new Action(() => chinchilla.WebPage.Uncheck("My Second Control:")).ShouldThrow<AmbiguousMatchFoundException>();
            new Action(() => chinchilla.WebPage.Uncheck("My Non-Existant Control:")).ShouldThrow<NoMatchFoundException>();
            chinchilla.WebPage.ClickButton(LocatorType.Css, ".post-back-button input");
            chinchilla.WebPage.Source.Should().Contain(" id=\"Control1\" type=\"checkbox\" name=\"Control1\" />");
            chinchilla.WebPage.Source.Should().Contain(" id=\"Control2\" type=\"checkbox\" name=\"Control2\" checked=\"checked\" />");
            chinchilla.WebPage.Source.Should().Contain(" id=\"Control3\" type=\"checkbox\" name=\"Control3\" checked=\"checked\" />");
        }

        [Test]
        public void ShouldHaveFluentApiByPath()
        {
            // Setup normally done in an [SetupFixture] class
            // but I do it here for testability.
            // http://www.nunit.org/index.php?p=setupFixture&r=2.4
            var mockBrowser = new MockBrowser(new CsQueryFormParser(), new MockServer(ServerDataFactory.Create()));
            IChinchilla chinchilla = Chinchilla.SetInstance(mockBrowser, new WebPageFactory());

            chinchilla.WebPage.Visit("fluent-post-back/check-boxes-uncheck.aspx");
            chinchilla.WebPage.Uncheck(LocatorType.Path, "//form/div/div[1]/input[1] | //form/div/div[3]/input[1]");
            chinchilla.WebPage.ClickButton(LocatorType.Css, ".post-back-button input");
            chinchilla.WebPage.Source.Should().Contain(" id=\"Control1\" type=\"checkbox\" name=\"Control1\" />");
            chinchilla.WebPage.Source.Should().Contain(" id=\"Control2\" type=\"checkbox\" name=\"Control2\" checked=\"checked\" />");
            chinchilla.WebPage.Source.Should().Contain(" id=\"Control3\" type=\"checkbox\" name=\"Control3\" />");
        }
    }
}
