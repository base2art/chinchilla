﻿namespace Base2art.WebRunner.CsQuery.Features
{
    using System;

    using Base2art.MockServer.Features;
    using Base2art.MockUI;
    using Base2art.MockUI.CsQuery;

    using FluentAssertions;

    using NUnit.Framework;

    [TestFixture]
    public class FinderApiFeature
    {
        [TearDown]
        public void DisposeChinchilla()
        {
            var chinchilla = Chinchilla.Instance;
            if (chinchilla != null)
            {
                chinchilla.Dispose();
            }
        }

        [Test]
        public void ShouldGetElementsByCss()
        {
            var mockBrowser = new MockBrowser(new CsQueryFormParser(), new MockServer.MockServer(ServerDataFactory.Create()));
            IChinchilla chinchilla = Chinchilla.SetInstance(LocatorType.Css, mockBrowser, new WebPageFactory());

            chinchilla.WebPage.Visit("finder-api.aspx");

            chinchilla.WebPage.FindFirstElement(".container").Should().NotBeNull();
            chinchilla.WebPage.FindFirstElement(LocatorType.Css, ".container").Should().NotBeNull();

            chinchilla.WebPage.FindElements(".container").Length.Should().Be(2);
            chinchilla.WebPage.FindElements(LocatorType.Css, ".container").Length.Should().Be(2);

            new Action(() => chinchilla.WebPage.FindElement(".container")).ShouldThrow<AmbiguousMatchFoundException>();
            new Action(() => chinchilla.WebPage.FindElement(LocatorType.Css, ".container")).ShouldThrow<AmbiguousMatchFoundException>();

            chinchilla.WebPage.FindElement("#div1").NodeName.Should().Be("div");
            chinchilla.WebPage.FindElement("#div1").TextValue.Should().Be("Div 1");
            chinchilla.WebPage.FindElement(LocatorType.Css, "#div1").NodeName.Should().Be("div");
            chinchilla.WebPage.FindElement("#div1").TextValue.Should().Be("Div 1");
        }

        [Test]
        public void ShouldGetElementsByPath()
        {
            var mockBrowser = new MockBrowser(new CsQueryFormParser(), new MockServer.MockServer(ServerDataFactory.Create()));
            IChinchilla chinchilla = Chinchilla.SetInstance(LocatorType.Path, mockBrowser, new WebPageFactory());

            chinchilla.WebPage.Visit("finder-api.aspx");

            const string SelectorMany = "//form/div/div";
            const string SelectorSingle = "//form/div/div[1]";
            chinchilla.WebPage.FindFirstElement(SelectorMany).Should().NotBeNull();
            chinchilla.WebPage.FindFirstElement(LocatorType.Path, SelectorMany).Should().NotBeNull();

            chinchilla.WebPage.FindElements(SelectorMany).Length.Should().Be(4);
            chinchilla.WebPage.FindElements(LocatorType.Path, SelectorMany).Length.Should().Be(4);

            new Action(() => chinchilla.WebPage.FindElement(SelectorMany)).ShouldThrow<AmbiguousMatchFoundException>();
            new Action(() => chinchilla.WebPage.FindElement(LocatorType.Path, SelectorMany)).ShouldThrow<AmbiguousMatchFoundException>();

            chinchilla.WebPage.FindElement(SelectorSingle).NodeName.Should().Be("div");
            chinchilla.WebPage.FindElement(SelectorSingle).TextValue.Should().Be("Div 1");
            chinchilla.WebPage.FindElement(LocatorType.Path, SelectorSingle).NodeName.Should().Be("div");
            chinchilla.WebPage.FindElement(SelectorSingle).TextValue.Should().Be("Div 1");
        }

        [Test]
        public void ShouldGetElementsByPathWithScope()
        {
            var mockBrowser = new MockBrowser(new CsQueryFormParser(), new MockServer.MockServer(ServerDataFactory.Create()));
            IChinchilla chinchilla = Chinchilla.SetInstance(LocatorType.Path, mockBrowser, new WebPageFactory());

            chinchilla.WebPage.Visit("finder-api.aspx");

            const string SelectorSingle = "//form/div/div[3]";
            using (chinchilla.WebPage.Within(SelectorSingle))
            {
                var element1 = chinchilla.WebPage.FindElement("./div");
                element1.TextValue.Should().Be("Div 3");
                element1["class"].Should().Be("innerDiv");
                var element2 = chinchilla.WebPage.FindElement("div");
                element2.TextValue.Should().Be("Div 3");
                element2["class"].Should().Be("innerDiv");
            }
        }
    }
}
