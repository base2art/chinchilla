﻿namespace Base2art.MockServer
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Web;
    using System.Web.Compilation;
    using System.Web.UI;

    public class PageCreationFactory : IPageCreationFactory
    {
        private readonly IDictionary<string, Page> pageCache = new Dictionary<string, Page>();

        public TPage CreatePage<TPage>(string path)
               where TPage : Page
        {
            if (this.pageCache.ContainsKey(path))
            {
                return (TPage)this.pageCache[path];
            }

            try
            {
                var compiledType = BuildManager.GetCompiledType("~/" + path);
                var x = (TPage)Activator.CreateInstance(compiledType);
                if (x.IsReusable)
                {
                    this.pageCache[path.ToLowerInvariant()] = x;
                }

                return x;
            }
            catch (HttpException e)
            {
                Debug.WriteLine(e);
                return null;
            }
        }
    }
}