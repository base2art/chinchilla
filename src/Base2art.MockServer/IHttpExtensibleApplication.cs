﻿namespace Base2art.MockServer
{
    public interface IHttpExtensibleApplication
    {
        // Useful for correcting requests before the 
        event RequestProcessing PreValidateRequestAndPath;
        
        event RequestProcessing PostValidateRequestAndPath;

        // Similar to Begin Request
        event RequestProcessing PreBeginRequest;


        // These are used for tweaking handler loading
        event RequestProcessing PreMapHandler;

        event RequestProcessing PostMapHandler;

        // Probably never used.
        event RequestProcessing PreCallHandler;

        event RequestProcessing PostCallHandler;

        // Similar to End Request
        event RequestProcessing EndRequestHandler;
    }
}
