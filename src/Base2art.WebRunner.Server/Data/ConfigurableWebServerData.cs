﻿namespace Base2art.WebRunner.Server
{
    using System;
    using System.ComponentModel;

    public class ConfigurableWebServerData
    {
        private string[] hosts;

        public string[] Hosts
        {
            get
            {
                var hosts = this.hosts;
                if (hosts == null || hosts.Length == 0)
                {
                    hosts = new string[]
                    {
                        "localhost"
                    };
                }
                return hosts;
            }
            set
            {
                this.hosts = value;
            }
        }

        public short Port { get; set; }

        public bool UseSsl { get; set; }
    }
}


