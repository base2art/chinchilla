﻿namespace Base2art.WebRunner.CsQuery.Interacters
{
    using System.Linq;

    using CsQuery;

    public class ValueWebPageInteracter : WebPageInteracterBase
    {
        public ValueWebPageInteracter(CsQueryWebPage pageApi, IScopeContext scope, string[] explicitlyMatchedNodes)
            : base(pageApi, scope, false, explicitlyMatchedNodes)
        {
        }

        protected override IDomObjectWrapper[] FindTargetElements(string locator)
        {
            return
                this.ScopedNode.Cq()
                    .Find("input")
                    .Where(x => x.HasAttribute("value") && x["value"].Contains(locator))
                    .Select(x => new DomObjectWrapper(x))
                    .ToArray();
        }
    }
}