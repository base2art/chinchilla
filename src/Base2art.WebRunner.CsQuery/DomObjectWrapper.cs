﻿namespace Base2art.WebRunner.CsQuery
{
    using System;

    using System.Net.Http;
    using Base2art.MockUI.CsQuery;

    using global::CsQuery;

    public class DomObjectWrapper : IDomObjectWrapper
    {
        private readonly IDomObject element;

        public DomObjectWrapper(IDomObject element)
        {
            this.element = element;
        }

        public string Value
        {
            get
            {
                return this.element["value"];
            }
        }

        public string Name
        {
            get
            {
                return this.element["name"];
            }
        }

        public IFormWrapper Form
        {
            get
            {
                return new FormWrapper(this.element.FindForm());
            }
        }

        public string PathToElement
        {
            get
            {
                return this.element.GeneratePath();
            }
        }

        public string NodeName
        {
            get
            {
                return this.element.NodeName.ToLowerInvariant();
            }
        }

        public string TextValue
        {
            get
            {
                return this.element.Text();
            }
        }

        public CQ NativeElement
        {
            get
            {
                return this.element.Cq();
            }
        }

        public string this[string name]
        {
            get
            {
                return this.element.HasAttribute(name) ? this.element.GetAttribute(name) : null;
            }
        }

        private class FormWrapper : IFormWrapper
        {
            private readonly IDomObject form;

            public FormWrapper(IDomObject form)
            {
                this.form = form;
            }

            public string PathToElement
            {
                get
                {
                    return this.form.GeneratePath();
                }
            }

            public string Action
            {
                get
                {
                    return !this.form.HasAttribute("action") ? string.Empty : this.form["action"];
                }
            }

            public HttpMethod Method
            {
                get
                {
                    if (this.form.HasAttribute("method"))
                    {
                        var httpMethod = HttpMethod.Post;
                        if (string.Equals(this.form["method"], httpMethod.Method, StringComparison.InvariantCultureIgnoreCase))
                        {
                            return httpMethod;
                        }
                    }

                    return HttpMethod.Get;
                }
            }
        }
    }
}
