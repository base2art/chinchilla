﻿namespace RealWorldTestProfiler
{
    using System;
    using System.Diagnostics;
    using System.Linq;
    using System.Net;
    using System.Reflection;
    using System.Web;
    using System.Web.Security;

    using Base2art.MockServer;
    using Base2art.MockServer.Util;
    using Base2art.MockUI;
    using Base2art.VitalSite.Web.Security;

    public class Program
    {
        public static void Main(string[] args)
        {
            new Program().Execute();
        }

        private void Execute()
        {
            var sw = new Stopwatch();
            sw.Start();
            using (var ui = new MockBrowser())
            {
                {
                    var sw1 = new Stopwatch();
                    sw1.Start();
                    var pageData = new PageData { Path = "admin/index.aspx", Cookies = new[] { this.UserCookie() } };
                    pageData.ContextSetup = this.CreateAuthenticatedContext();
                    ui.RequestPage(pageData);
                    ////Console.WriteLine(cq.RawText);
                    ////cq.NormalizedText.Should().NotContain("Sign in using your VitalSite Core account");
                    ////cq.NormalizedText.Should()
                    ////    .Contain("<!-- NOT YET <li><a class=\"InboxLink\" href=\"javascript:;\">Inbox (5)</a></li> -->");
                    ////cq.RawText.Length.Should().BeGreaterThan(1000);
                    Console.WriteLine(sw1.Elapsed);
                }

                {
                    var sw1 = new Stopwatch();
                    sw1.Start();
                    for (int i = 0; i < 1000 * 100; i++)
                    {
                        var pageData = new PageData { Path = "admin/index.aspx", Cookies = new[] { this.UserCookie() } };
                        pageData.ContextSetup = this.CreateAuthenticatedContext();
                        ui.RequestPage(pageData);
                        //// Console.WriteLine(cq.RawText);
                        ////cq.NormalizedText.Should().NotContain("Sign in using your VitalSite Core account");
                        ////cq.NormalizedText.Should()
                        ////    .Contain("<!-- NOT YET <li><a class=\"InboxLink\" href=\"javascript:;\">Inbox (5)</a></li> -->");
                        ////cq.RawText.Length.Should().BeGreaterThan(1000);
                    }

                    Console.WriteLine(sw1.Elapsed);
                }
            }

            Console.WriteLine(sw.Elapsed);
            Console.ReadLine();
        }

        private Cookie UserCookie()
        {
            return new Cookie(
                ".VitalSite",
                "3556E793760EA786A3465754715FB77196C5418F852E4884933B3EF"
                + "7596A65CDA615686EB2D0598B3F47B2F2AC872424119FE47D2CE2E"
                + "80FA00EAB8E22DFD3455E80627F11A7588AF58E9E8BE2C7687E867"
                + "5AA38721EA9A557C9563849B3A7D9A2A8E37570F9D4362AAD1BEBB"
                + "E2407FA8AF34D807EE63D40E2ACB9FB94A88F45F953EC817374DBC"
                + "445D0D86BA6E873E913EEA798440BB0FDED8895918BB62CE0");
        }

        ////private ContextSetup CreateUnauthenticatedContext()
        ////{
        ////    return new ContextSetup((r, x) =>
        ////                            {
        ////                                x.ApplicationInstance = new HttpApplication();
        ////                                ////x.ApplicationInstance.Should().NotBeNull("Application Instance Required for ADFS");
        ////                                FormsIdentity fi = new FormsIdentity(new FormsAuthenticationTicket("TEST", true, 120));

        ////                                HttpContextBase contextBase = new HttpContextWrapper(x);
        ////                                DateTime dt = DateTime.UtcNow.AddMinutes(-1);
        ////                                var asm = r.Load(new AssemblyName("Base2art.VitalSite.Web"));
        ////                                Type t = asm.GetType("Base2art.VitalSite.Web.Security.AuthenticatedUser");
        ////                                var ctor = t.GetConstructors(BindingFlags.NonPublic | BindingFlags.Instance).First();
        ////                                var obj = ctor.Invoke(new object[] { contextBase, fi, dt });
        ////                                IAuthenticatedUser au = (IAuthenticatedUser)obj;
        ////                                x.User = au;
        ////                            });
        ////}

        private ContextSetup CreateAuthenticatedContext()
        {
            return new ContextSetup((appDomain, context) =>
                                    {
                                        context.ApplicationInstance = new HttpApplication();
                                        ////context.ApplicationInstance.Should().NotBeNull("Application Instance Required for ADFS");
                                        FormsIdentity fi = new FormsIdentity(new FormsAuthenticationTicket("2", true, 120));
                                        HttpContextBase contextBase = new HttpContextWrapper(context);
                                        DateTime dt = DateTime.UtcNow.AddMinutes(-1);
                                        var asm = appDomain.Load(new AssemblyName("Base2art.VitalSite.Web"));
                                        Type t = asm.GetType("Base2art.VitalSite.Web.Security.AuthenticatedUser");
                                        var ctor = t.GetConstructors(BindingFlags.NonPublic | BindingFlags.Instance).First();
                                        var obj = ctor.Invoke(new object[] { contextBase, fi, dt });
                                        IAuthenticatedUser au = (IAuthenticatedUser)obj;
                                        context.User = au;
                                    });
        }
    }
}
