﻿namespace Base2art.WebRunner.CsQuery
{
    using System.Net.Http;

    public interface IFormWrapper
    {
        string PathToElement { get; }

        string Action { get; }

        HttpMethod Method { get; }
    }
}