﻿namespace Base2art.MockServer.Util
{
    using System.Web.UI;

    public interface IControlCreator<out TResult>
        where TResult : Control
    {
        TResult Create();
    }


    public interface IPageCreator<out TResult>
        where TResult : Page
    {
        TResult Create(string path);
    }
}
