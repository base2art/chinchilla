﻿namespace Base2art.MockServer
{
    using System;
    using System.ComponentModel;
    using System.IO;
    using System.Web;

    using Base2art.Converters;
    using Base2art.ComponentModel;
    using Base2art.MockServer.Util;

    [Serializable]
    public class ServerData : ICloneable, IServerData
    {
        private string defaultPage;

        private IDelegate<AppDomain, HttpContext> contextSetup;
        
        [NonSerialized]
        private readonly ConfigurationValueProperty<bool> useDirectLoading =
            new ConfigurationValueProperty<bool>(new BooleanParser(), "UseDirectLoading", false);

        [NonSerialized]
        private readonly ConfigurationObjectProperty<string> physicalApplicationDirectory =
            new ConfigurationObjectProperty<string>(new StringParser(), "PhysicalApplicationDirectory", new AppDirCreator());

        [NonSerialized]
        private readonly ConfigurationObjectProperty<string> virtualDirectory =
            new ConfigurationObjectProperty<string>(new StringParser(), "VirtualDirectory", "/");

        public string VirtualDirectory
        {
            get { return this.virtualDirectory.Value; }
            set { this.virtualDirectory.Value = value; }
        }

        public bool UseDirectLoading
        {
            get { return this.useDirectLoading.Value; }
            set { this.useDirectLoading.Value = value; }
        }

        public string PhysicalApplicationDirectory
        {
            get { return this.physicalApplicationDirectory.Value; }
            set { this.physicalApplicationDirectory.Value = value; }
        }

        public string DefaultPage
        {
            get
            {
                return this.defaultPage ?? "default.aspx";
            }

            set
            {
                this.defaultPage = value;
            }
        }

        public IDelegate<AppDomain, HttpContext> ContextSetup
        {
            get
            {
                return this.contextSetup ?? new NullDelegate<AppDomain, HttpContext>();
            }

            set
            {
                this.contextSetup = value;
            }
        }

        public ServerData Clone()
        {
            return (ServerData)this.MemberwiseClone();
        }

        object ICloneable.Clone()
        {
            return this.Clone();
        }
        
        [Serializable]
        private class AppDirCreator : Base2art.ICreator<string>
        {
            public string Create()
            {
                return Path.Combine(Path.GetTempPath(), "TempAspNet-MockServer");
            }
        }
    }

}