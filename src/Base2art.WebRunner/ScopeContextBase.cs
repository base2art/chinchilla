﻿namespace Base2art.WebRunner
{
    public abstract class ScopeContextBase : System.ComponentModel.Component, IScopeContext
    {
        private readonly WebPageBase pageApi;

        protected ScopeContextBase(WebPageBase pageApi)
        {
            this.pageApi = pageApi;
        }

        public void ExitScope()
        {
            this.Dispose();
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            if (disposing)
            {
                this.pageApi.RemoveScope(this);
            }
        }
    }
}