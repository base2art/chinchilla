﻿// -----------------------------------------------------------------------
// <copyright file="TypeExtensions.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace Base2art.MockServer.Util.Reflection
{
    using System;
    using System.IO;
    using Validation;
    
    public static class TypeExtensions
    {
        public static DirectoryInfo GetOriginatingAssemblyDirectory(this System.Type type)
        {
            type.Validate().IsNotNull();
            string x = new Uri(type.Assembly.CodeBase).AbsolutePath;
            return new FileInfo(x).Directory;
        }
    }
}
