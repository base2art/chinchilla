﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="buttons.aspx.cs" Inherits="SampleWebAppWithCasts.fluent_post_back.buttons" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    THIS IS THE PAGE FOR BUTTONS!
    <form id="form1" runat="server">
    <div>
        <div class="control-1">
            <asp:Button OnClick="HandleButton1Click" runat="server" Text="Button 1"/>
            <asp:Label runat="server" ID="ButtonLabelResponse1"></asp:Label>
        </div>
        <div class="control-2">
            <asp:Button OnClick="HandleButton2Click" runat="server" Text="Button 2"/>
            <asp:Label runat="server" ID="ButtonLabelResponse2"></asp:Label>
        </div>
    </div>
    </form>
</body>
</html>
