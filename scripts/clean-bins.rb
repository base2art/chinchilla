require 'fileutils'


def delete_if(path)
  if Dir.exist? path
    puts "Removing: #{path}"
    FileUtils.rm_rf path
  else
    puts "Path Not Found: #{path}"
  end
end

def delete_bin(prefix, proj_name)

  delete_if "#{prefix}#{proj_name}/bin"
  delete_if "#{prefix}#{proj_name}/obj"
end

def delete_by_group(prefix)
  Dir.entries(prefix).select{|x| not x.start_with? "."}.each{|x| delete_bin prefix, x}
end

delete_by_group "../src/"
delete_by_group "../test/"
delete_by_group "../data/"
delete_by_group "../test/Base2art.MockServer.Features/Fixtures/"
