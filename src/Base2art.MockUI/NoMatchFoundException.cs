﻿namespace Base2art.MockUI
{
    using System;
    using System.Runtime.Serialization;

    public class NoMatchFoundException : ApplicationException
    {
        public NoMatchFoundException()
        {
        }

        public NoMatchFoundException(string message)
            : base(message)
        {
        }

        public NoMatchFoundException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        protected NoMatchFoundException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}
