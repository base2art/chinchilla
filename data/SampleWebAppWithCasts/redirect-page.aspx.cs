﻿namespace SampleWebAppWithCasts
{
    using System;

    public partial class redirect_page : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            this.Response.Redirect("Default.aspx");
        }
    }
}