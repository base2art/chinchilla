﻿namespace Base2art.WebRunner
{
    public interface IElement
    {
        string Value { get; }

        string Name { get; }

        string PathToElement { get; }

        string NodeName { get; }

        string TextValue { get; }

        string this[string name] { get; }
    }
}