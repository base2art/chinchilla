﻿namespace Base2art.MockUI
{
    using System.ComponentModel;

    using Base2art.MockServer;

    public class MockBrowser : Component, IMockBrowser
    {
        private readonly IFormParser parser;

        private readonly IMockServer mockServer;

        public MockBrowser(IFormParser parser) : this(parser, new MockServer())
        {
        }

        public MockBrowser(IFormParser parser, IMockServer mockServer)
        {
            this.parser = parser;
            this.mockServer = mockServer;
        }

        public IQueryablePageResult RequestPage(PageData pageData)
        {
            var requestPage = this.mockServer.RequestPage(pageData);
            return new QueryablePageResult(this.parser, requestPage);
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            if (disposing)
            {
                this.mockServer.Dispose();
            }
        }
    }
}
