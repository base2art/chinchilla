﻿namespace Base2art.MockUI.Features.MockUIFeature
{
    using System.Linq;
    using System.Xml.Linq;
    using System.Xml.XPath;

    using Base2art.MockUI.CsQuery;

    using global::CsQuery;

    using FluentAssertions;

    using NUnit.Framework;

    [TestFixture]
    public class DomObjectExtensionFeature
    {
        [Test]
        public void ShouldCorrectlyParsePathExpression()
        {
            var path = "/html[1]/body[1]/p[12]/span[013]".ParsePath();
            path.Length.Should().Be(4);
            path[0].Key.Should().Be("html");
            path[0].Value.Should().Be(1);
            path[1].Key.Should().Be("body");
            path[1].Value.Should().Be(1);
            path[2].Key.Should().Be("p");
            path[2].Value.Should().Be(12);
            path[3].Key.Should().Be("span");
            path[3].Value.Should().Be(13);
        }

        [Test]
        public void ShouldCorrectlyGetPathForDomObject()
        {
            const string Html = @"
<html>
  <head></head>
  <body>
    <form>
      <input type='text' id='tb1' />
      <input type='text' id='tb2' />
    </form>
    <form>
      <input type='text' id='tb3' />
      <input type='text' id='tb4' />
    </form>
  </body>
</html>";

            var cq = new CQ(Html);
            cq.Find("#tb1").Get(0).GeneratePath().Should().Be("/html[1]/body[1]/form[1]/input[1]");
            cq.Find("#tb2").Get(0).GeneratePath().Should().Be("/html[1]/body[1]/form[1]/input[2]");
            cq.Find("#tb3").Get(0).GeneratePath().Should().Be("/html[1]/body[1]/form[2]/input[1]");
            cq.Find("#tb4").Get(0).GeneratePath().Should().Be("/html[1]/body[1]/form[2]/input[2]");
            XDocument.Parse(Html).XPathSelectElement("/html[1]/body[1]/form[2]/input[2]")
                              .Attribute("id")
                              .Value.Should()
                              .Be("tb4");
        }

        [Test]
        public void ShouldCorrectlyGetPathForXElement()
        {
            const string Html = @"
<html>
  <head></head>
  <body>
    <form>
      <input type='text' id='tb1' />
      <input type='text' id='tb2' />
    </form>
    <form>
      <input type='text' id='tb3' />
      <input type='text' id='tb4' />
    </form>
  </body>
</html>";

            var cq = new CQ(Html);
            var inputs = cq.Document.ToXDocument().XPathSelectElements("//input");
            inputs.Skip(0).First().GeneratePath().Should().Be("/html[1]/body[1]/form[1]/input[1]");
            inputs.Skip(1).First().GeneratePath().Should().Be("/html[1]/body[1]/form[1]/input[2]");
            inputs.Skip(2).First().GeneratePath().Should().Be("/html[1]/body[1]/form[2]/input[1]");
            inputs.Skip(3).First().GeneratePath().Should().Be("/html[1]/body[1]/form[2]/input[2]");
            XDocument.Parse(Html).XPathSelectElement("/html[1]/body[1]/form[2]/input[2]")
                              .Attribute("id")
                              .Value.Should()
                              .Be("tb4");
        }

        [Test]
        public void ShouldGetTextOfElement()
        {
            const string Html = @"
<html>
  <head></head>
  <body>
    <form>
      <div>
        <label>
          Some Label
          <strong>*</strong>
        </label>
      </div>
    </form>
  </body>
</html>";

            var cq = new CQ(Html);
            cq.Find("label").Get(0).Text().Should().BeEquivalentTo("Some Label *");
        }
    }
}
