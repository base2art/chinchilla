﻿namespace Base2art.MockServer.Modules
{
    using System.Collections.Generic;

    public class ModuleConfiguration
    {
        public static IEnumerable<IHttpExtensibilityModule> FindModules()
        {
            return new IHttpExtensibilityModule[]
                                      {
                                          new HandlerFixModule(),
                                          new StaticFileModule(),
                                          new DefaultDocumentModule()
                                      };
        }
    }
}