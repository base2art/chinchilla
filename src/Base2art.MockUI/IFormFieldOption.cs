﻿namespace Base2art.MockUI
{
    public interface IFormFieldOption
    {
        //string Name { get; }

        string Value { get; }

        string ValueText { get; }

        string PathToElement { get; }
    }
}