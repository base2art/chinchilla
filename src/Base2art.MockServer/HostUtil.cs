﻿namespace Base2art.MockServer
{
    using System.Reflection;
    using System.Web;

    internal static class HostUtil
    {
        public static void SetupBuildSteps(Assembly webAssembly, HttpApplication app, HttpExtensibleApplicationDelegator module)
        {
            using (StepManagerManipulator manipulator = new StepManagerManipulator(webAssembly, app))
            {
                manipulator.ReplaceEndRequestDelegate((x, y) => module.OnEndRequestHandler(HttpContext.Current));

                manipulator.InsertDeletegateAfter("CallHandlerExecutionStep", (x, y) => module.OnPostCallHandler(HttpContext.Current));
                manipulator.InsertDeletegateBefore("CallHandlerExecutionStep", (x, y) => module.OnPreCallHandler(HttpContext.Current));

                manipulator.InsertDeletegateAfter("MapHandlerExecutionStep", (x, y) => module.OnPostMapHandler(HttpContext.Current));
                manipulator.InsertDeletegateBefore("MapHandlerExecutionStep", (x, y) => module.OnPreMapHandler(HttpContext.Current));

                var afterStepOfBeginRequest = manipulator.HasStep("UrlMappingsExecutionStep") ? "UrlMappingsExecutionStep" : "ValidatePathExecutionStep";
                manipulator.InsertDeletegateAfter(afterStepOfBeginRequest, (x, y) => module.OnPreBeginRequest(HttpContext.Current));

                manipulator.InsertDeletegateAfter("ValidatePathExecutionStep", (x, y) => module.OnPostValidateRequestAndPath(HttpContext.Current));
                manipulator.InsertDeletegateBefore("ValidateRequestExecutionStep", (x, y) => module.OnPreValidateRequestAndPath(HttpContext.Current));
            }
        }
    }
}
