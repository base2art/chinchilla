﻿namespace Base2art.MockServer.Features
{
    using Base2art.MockServer;
    using Base2art.MockServer.Util;

    using FluentAssertions;

    using NUnit.Framework;

    using WebControlLibrary.UI.Controls;

    [TestFixture]
    public class ContextFeature
    {
        [Test]
        public void ShouldReadContext()
        {
            using (var server = new MockServer(ServerDataFactory.Create()))
            {
                var rezult = server.RequestControl(
                        new ControlData<ContextReader>
                        {
                            Control = new ControlCreator<ContextReader>(
                                () =>
                                {
                                    var contextReader = new ContextReader { Key = "TEST_KEY" };
                                    contextReader.Init +=
                                        (x, y) =>
                                        contextReader.Page.Request.RequestContext.HttpContext.Items["TEST_KEY"]
                                            .ShouldBeEquivalentTo("TEST_VALUE");
                                    return contextReader;
                                }),
                            ContextSetup = new ContextSetup((r, x) => x.Items["TEST_KEY"] = "TEST_VALUE"),
                            PlaceInsideForm = false
                        });

                rezult.RawText.Should().Be("TEST_VALUE");
            }
        }
    }
}
