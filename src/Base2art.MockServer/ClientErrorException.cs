﻿namespace Base2art.MockServer
{
    using System;
    using System.Runtime.Serialization;

    [Serializable]
    public class ClientErrorException : ResponseException
    {
        public ClientErrorException(int statusCode, string simpleMessage, IPageResult pageResult)
            : base(statusCode, simpleMessage, pageResult)
        {
        }

        public ClientErrorException()
        {
        }

        public ClientErrorException(string message)
            : base(message)
        {
        }

        public ClientErrorException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        protected ClientErrorException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}