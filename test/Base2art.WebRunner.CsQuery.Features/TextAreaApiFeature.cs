﻿namespace Base2art.WebRunner.CsQuery.Features
{
    using System;

    using Base2art.MockServer;
    using Base2art.MockServer.Features;
    using Base2art.MockUI;
    using Base2art.MockUI.CsQuery;

    using FluentAssertions;

    using NUnit.Framework;

    [TestFixture]
    public class TextAreaApiFeature
    {
        [TearDown]
        public void DisposeChinchilla()
        {
            var chinchilla = Chinchilla.Instance;
            if (chinchilla != null)
            {
                chinchilla.Dispose();
            }
        }

        [Test]
        public void ShouldHaveFluentApiByCss()
        {
            // Setup normally done in an [SetupFixture] class
            // but I do it here for testability.
            // http://www.nunit.org/index.php?p=setupFixture&r=2.4
            var mockBrowser = new MockBrowser(new CsQueryFormParser(), new MockServer(ServerDataFactory.Create()));
            IChinchilla chinchilla = Chinchilla.SetInstance(mockBrowser, new WebPageFactory());

            chinchilla.WebPage.Visit("fluent-post-back/text-areas.aspx");
            chinchilla.WebPage.FillIn(LocatorType.Css, ".control-1 textarea", "hello world");
            chinchilla.WebPage.ClickButton(LocatorType.Css, ".post-back-button input");
            chinchilla.WebPage.Source.Should().Contain("<textarea name=\"Control1\" rows=\"2\" cols=\"20\" id=\"Control1\">\r\nhello world</textarea>");
            chinchilla.WebPage.Source.Should().Contain("<textarea name=\"Control2\" rows=\"2\" cols=\"20\" id=\"Control2\">\r\n</textarea>");
            chinchilla.WebPage.Source.Should().Contain("<textarea name=\"Control3\" rows=\"2\" cols=\"20\" id=\"Control3\">\r\nhello world</textarea>");
        }

        [Test]
        public void ShouldHaveFluentApiByLabel()
        {
            // Setup normally done in an [SetupFixture] class
            // but I do it here for testability.
            // http://www.nunit.org/index.php?p=setupFixture&r=2.4
            var mockBrowser = new MockBrowser(new CsQueryFormParser(), new MockServer(ServerDataFactory.Create()));
            IChinchilla chinchilla = Chinchilla.SetInstance(mockBrowser, new WebPageFactory());

            chinchilla.WebPage.Visit("fluent-post-back/text-areas.aspx");
            chinchilla.WebPage.FillIn("My First Control:", "hello world2");
            new Action(() => chinchilla.WebPage.FillIn("My Second Control:", "hello world3")).ShouldThrow<AmbiguousMatchFoundException>();
            new Action(() => chinchilla.WebPage.FillIn("My Non-Existant Control:", "hello world3")).ShouldThrow<NoMatchFoundException>();
            chinchilla.WebPage.ClickButton(LocatorType.Css, ".post-back-button input");
            chinchilla.WebPage.Source.Should().Contain("<textarea name=\"Control1\" rows=\"2\" cols=\"20\" id=\"Control1\">\r\nhello world2</textarea>");
            chinchilla.WebPage.Source.Should().Contain("<textarea name=\"Control2\" rows=\"2\" cols=\"20\" id=\"Control2\">\r\n</textarea>");
            chinchilla.WebPage.Source.Should().Contain("<textarea name=\"Control3\" rows=\"2\" cols=\"20\" id=\"Control3\">\r\n</textarea>");
        }

        [Test]
        public void ShouldHaveFluentApiByPath()
        {
            // Setup normally done in an [SetupFixture] class
            // but I do it here for testability.
            // http://www.nunit.org/index.php?p=setupFixture&r=2.4
            var mockBrowser = new MockBrowser(new CsQueryFormParser(), new MockServer(ServerDataFactory.Create()));
            IChinchilla chinchilla = Chinchilla.SetInstance(mockBrowser, new WebPageFactory());

            chinchilla.WebPage.Visit("fluent-post-back/text-areas.aspx");
            chinchilla.WebPage.FillIn(LocatorType.Path, "//form/div/div[1]/textarea[1] | //form/div/div[3]/textarea[1]", "hello world2");
            chinchilla.WebPage.ClickButton(LocatorType.Css, ".post-back-button input");
            chinchilla.WebPage.Source.Should().Contain("<textarea name=\"Control1\" rows=\"2\" cols=\"20\" id=\"Control1\">\r\nhello world2</textarea>");
            chinchilla.WebPage.Source.Should().Contain("<textarea name=\"Control2\" rows=\"2\" cols=\"20\" id=\"Control2\">\r\n</textarea>");
            chinchilla.WebPage.Source.Should().Contain("<textarea name=\"Control3\" rows=\"2\" cols=\"20\" id=\"Control3\">\r\nhello world2</textarea>");
        }
    }
}
