﻿namespace Base2art.MockServer.Modules
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Security.Policy;
    using System.Web;

    public class DefaultDocumentModule : IHttpExtensibilityModule
    {
        public void Init(IHttpExtensibleApplication application)
        {
            application.PreCallHandler += this.HandleDefaultDocument;
        }

        private void HandleDefaultDocument(System.Web.HttpContext httpContext)
        {
            var handler = httpContext.CurrentHandler;
            if (handler == null || handler is DefaultHttpHandler)
            {
                ContextExtensionData extensionData = httpContext.GetExtensionData();
                if (string.Equals(httpContext.Request.Url.AbsolutePath, VirtualPathUtility.ToAbsolute("~/"), StringComparison.OrdinalIgnoreCase))
                {
                    httpContext.Response.Redirect(extensionData.ServerData.DefaultPage);
                    httpContext.Response.End();
                }
            }
        }
    }
}
