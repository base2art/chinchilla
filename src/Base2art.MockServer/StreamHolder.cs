﻿namespace Base2art.MockServer
{
    using System;
    using System.ComponentModel;
    using System.IO;

    public class StreamHolder : Component
    {
        private readonly MemoryStream memoryStream = new MemoryStream();

        private StreamWriter writer;

        public TextWriter CreateWriter()
        {
            if (this.writer != null)
            {
                throw new InvalidOperationException("Developer Error: You are only allowed to create the writer one time.");
            }

            this.writer = new StreamWriter(this.memoryStream, System.Text.Encoding.Default);
            return this.writer;
        }

        public string ReadText()
        {
            if (this.writer == null)
            {
                return null;
            }

            this.writer.Flush();

            this.memoryStream.Seek(0L, SeekOrigin.Begin);

            using (var reader = new StreamReader(this.memoryStream, System.Text.Encoding.Default, true))
            {
                var text = reader.ReadToEnd();
                this.memoryStream.Dispose();
                return text;
            }
        }
    }
}