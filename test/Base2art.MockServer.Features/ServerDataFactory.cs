﻿namespace Base2art.MockServer.Features
{
    using Base2art.IO;
    using Base2art.MockServer.Util.Reflection;

    public static class ServerDataFactory
    {
        private static string AppLocation
        {
            get
            {
                return typeof(ServerDataFactory)
                    .GetOriginatingAssemblyDirectory()
                    .Parent()
                    .Parent()
                    .Parent()
                    .Parent()
                    .Dir("data")
                    .Dir("SampleWebAppWithCasts")
                    .FullName;
            }
        }

        private static string MvcAppLocation
        {
            get
            {
                return typeof(ServerDataFactory)
                    .GetOriginatingAssemblyDirectory()
                    .Parent()
                    .Parent()
                    .Parent()
                    .Parent()
                    .Dir("data")
//                    .Dir("SampleMvcWebAppWithCasts_v4_5_2")
                    .Dir("SampleMvcWebAppWithCasts")
                    .FullName;
            }
        }

        public static ServerData Create()
        {
            return new ServerData { PhysicalApplicationDirectory = AppLocation };
        }

        public static ServerData CreateMvc()
        {
            return new ServerData 
            { 
                PhysicalApplicationDirectory = MvcAppLocation 
            };
        }
    }
}
