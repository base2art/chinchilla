﻿namespace Base2art.MockServer
{
    using System;
    using System.Web;
    using System.Web.UI;

    using Base2art.MockServer.Util;

    public interface IHost : IDisposable
    {
        IPageResult RequestControl<T>(
            ICreator<HttpBrowserCapabilities> creator,
            IServerData parameters,
            ControlData<T> controlData)
            where T : Control;

        IPageResult RunPage(
            ICreator<HttpBrowserCapabilities> creator,
            IServerData parameters,
            PageData pageData);

        void Init(string defaultPage);

        AppDomain CurrentAppDomain();
    }
}