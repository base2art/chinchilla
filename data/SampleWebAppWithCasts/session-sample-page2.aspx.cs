﻿namespace SampleWebAppWithCasts
{
    using System;

    public partial class session_sample_Page2 : System.Web.UI.Page
    {

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            this.SessionData.Text = this.Session["Hello"] as string;
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            this.Session["Hello"] = "World";
        }
    }
}