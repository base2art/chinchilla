﻿namespace Base2art.MockServer
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Web.Hosting;
    using Base2art.Collections;
    using Base2art.MockServer.Util.Converters;

    public class CustomSimpleWorkerRequest : SimpleWorkerRequest
    {
        private readonly string host;

        private readonly TextWriter output;

        private readonly IReadOnlyMap<string, string> inputHeaders;

        private readonly IMap<string, HeaderValue> outputHeaders;

        private readonly bool isSecure;

        private readonly int port;

        private readonly string method;

        private readonly byte[] bodyContent;

        private readonly NameValueCollection postContent;

        public CustomSimpleWorkerRequest(
            bool isSecure,
            string method,
            string host,
            int port,
            string page,
            string query,
            IReadOnlyMap<string, string> inputHeaders,
            byte[] bodyContent,
            NameValueCollection postContent,
            TextWriter output,
            IMap<string, HeaderValue> outputHeaders)
            : base(page, query, output)
        {
            this.postContent = postContent;
            this.bodyContent = bodyContent;
            this.method = method;
            this.port = port;
            this.isSecure = isSecure;
            this.inputHeaders = inputHeaders;
            this.outputHeaders = outputHeaders;
            this.output = output;
            this.host = host;
        }
        
        public override string GetHttpVerbName()
        {
            var content = string.IsNullOrWhiteSpace(this.method)
                ? base.GetHttpVerbName()
                : this.method;
            
            if ( (this.bodyContent != null && this.bodyContent.Length > 0) || (this.postContent != null && this.postContent.Count > 0) )
            {
                if (string.Equals(this.method, "GET", StringComparison.OrdinalIgnoreCase))
                {
                    return "POST";
                }
            }
            
            return content;
        }

        public IMap<string, HeaderValue> Headers
        {
            get { return this.outputHeaders; }
        }
        
        public override string[][] GetUnknownRequestHeaders()
        {
            List<string[]> items = new List<string[]>();
            
            var @set = new HashSet<string>(
                Enum.GetValues(typeof(KnownHeaderTypes))
                .Cast<KnownHeaderTypes>()
                .Select(index=>KnownHeaderTypesLookup.Find(index, true, false)));
            
            foreach (var key in this.inputHeaders.Keys) 
            {
                if (!@set.Contains(key))
                {
                    var headerArray = new string[2];
                    headerArray[0] = key;
                    headerArray[1] = this.inputHeaders[key];
                    items.Add(headerArray);
                }
            }
            
            return items.ToArray();
        }
        
        public override bool IsSecure()
        {
            return this.isSecure;
        }
        
        public override int GetLocalPort()
        {
            return this.port;
        }
        
        public override string GetLocalAddress()
        {
            if (this.host != null)
            {
                return this.host;
            }

            return base.GetLocalAddress();
        }
        
        public override void SendKnownResponseHeader(int index, string value)
        {
            var type = (KnownHeaderTypes)index;
            var key = KnownHeaderTypesLookup.Find(type, false, true);
            if (this.Headers != null && !string.IsNullOrWhiteSpace(key))
            {
                this.Headers[key] = new HeaderValue { Value = value, IsKnownValue = true };
            }
        }
        
        public override void SendUnknownResponseHeader(string name, string value)
        {
            if (this.Headers != null)
            {
                this.Headers[name] = new HeaderValue { Value = value, IsKnownValue = false };
            }
        }
        
        public override bool IsEntireEntityBodyIsPreloaded()
        {
            return true;
        }
        
        public override byte[] GetPreloadedEntityBody()
        {
            if (this.postContent != null && this.postContent.Count > 0)
            {
                return Encoding.Default.GetBytes(this.postContent.Convert().ToQueryString());
            }
            
            return this.bodyContent;
        }
        
        public override void SendResponseFromFile(string filename, long offset, long length)
        {
            if (filename == null)
            {
                throw new ArgumentNullException("filename");
            }
            
            if (offset < 0L)
            {
                throw new ArgumentException("offset Must be 0 or more", "offset");
            }
            
            if (length < -1L)
            {
                throw new ArgumentException("length Must be -1 or more", "length");
            }
            
            //            filename = this.GetNormalizedFilename(filename);
            using (FileStream fileStream = new FileStream(filename, FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                long length2 = fileStream.Length;
                if (length == -1L)
                {
                    length = length2 - offset;
                }
                if (length2 < offset)
                {
                    throw new ArgumentException("Invalid_range", "offset");
                }
                if (length2 - offset < length)
                {
                    throw new ArgumentException("Invalid_range", "length");
                }
                
                this.WriteStreamAsText(fileStream, offset, length);
                return;
            }
        }
        
        public override string GetKnownRequestHeader(int index)
        {
            var type = (KnownHeaderTypes)index;
            
            if (this.postContent != null && this.postContent.Count > 0 && type == KnownHeaderTypes.HeaderContentType)
            {
                return "application/x-www-form-urlencoded";
            }
            
            var key = KnownHeaderTypesLookup.Find(type, true, false);
            
            if (string.IsNullOrWhiteSpace(key))
            {
                return null;
            }
            
            if (this.inputHeaders.Contains(key))
            {
                return this.inputHeaders[key];
            }
            
            return null;
        }
        
        public override string GetUnknownRequestHeader(string name)
        {
            if (this.inputHeaders.Contains(name))
            {
                return this.inputHeaders[name];
            }
            
            foreach (var key in this.inputHeaders.Keys)
            {
                if (string.Equals(key, name, StringComparison.OrdinalIgnoreCase))
                {
                    return this.inputHeaders[key];
                }
            }
            
            return null;
        }
        
        public override string GetPathInfo()
        {
            return string.Empty;
        }
        
        public override string GetFilePath()
        {
            return this.GetUriPath();
        }
        
        private void WriteStreamAsText(Stream f, long offset, long size)
        {
            if (size < 0L)
            {
                size = f.Length - offset;
            }
            if (size > 0L)
            {
                if (offset > 0L)
                {
                    f.Seek(offset, SeekOrigin.Begin);
                }
                
                byte[] array = new byte[(int)size];
                int count = f.Read(array, 0, (int)size);
                this.output.Write(Encoding.Default.GetChars(array, 0, count));
            }
        }
    }
}