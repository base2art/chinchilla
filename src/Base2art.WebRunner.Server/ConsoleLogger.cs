﻿namespace Base2art.WebRunner.Server
{
    using System;

    public class ConsoleLogger : ILogger
    {
        public void WriteLine(string log)
        {
            Console.Out.WriteLine(log);
        }
    }
}