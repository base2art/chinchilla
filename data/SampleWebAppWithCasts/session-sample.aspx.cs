﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SampleWebAppWithCasts
{
    public partial class session_sample : System.Web.UI.Page
    {

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            this.SessionData.Text = this.Session["Hello"] as string;
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            this.Session["Hello"] = "World";
        }
    }
}