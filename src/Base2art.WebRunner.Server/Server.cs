﻿namespace Base2art.WebRunner.Server
{
    using System;
    using System.ComponentModel;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Runtime.Remoting;
    using System.Text;
    using System.Threading;

    using Base2art.Collections;
    using Base2art.MockServer;

    public class Server : Component
    {
        private readonly WebServerData webServerData;

        private readonly ServerData serverData;

        private readonly ILogger logger;

        private readonly HttpListener httpListener = new HttpListener();

        public Server(WebServerData webServerData, ServerData serverData, ILogger logger)
        {
            this.webServerData = webServerData;
            this.serverData = serverData;
            this.logger = logger;
        }

        public static bool IsSupported
        {
            get
            {
                return HttpListener.IsSupported;
            }
        }
        
        public virtual Uri PrimaryUrl
        {
            get
            {
                var uriBuilder = new UriBuilder();
                uriBuilder.Host = this.webServerData.Hosts.First();
                uriBuilder.Port = webServerData.Port;
                uriBuilder.Scheme = webServerData.UseSsl ? Uri.UriSchemeHttps : Uri.UriSchemeHttp;
                uriBuilder.Path = string.Concat(serverData.VirtualDirectory, "/", serverData.DefaultPage);
                
                while (uriBuilder.Path.Contains("//"))
                {
                    uriBuilder.Path = uriBuilder.Path.Replace("//", "/");
                }
                
                return uriBuilder.Uri;
            }
        }

        public void Start()
        {
            ThreadPool.QueueUserWorkItem(o =>
                                         {
                                             foreach (var host in this.webServerData.Hosts)
                                             {
                                                 var schema = webServerData.UseSsl ? Uri.UriSchemeHttps :Uri.UriSchemeHttp;
                                                 var uriBuilder = new UriBuilder(schema, host, this.webServerData.Port);
                                                 httpListener.Prefixes.Add(uriBuilder.Uri.ToString());
                                             }
                                             
                                             httpListener.Start();
                                             
                                             var server = this.ProvisionServer("Webserver running...");
                                             
                                             ThreadPool.QueueUserWorkItem(
                                                 c =>
                                                 {
                                                     Thread.Sleep(TimeSpan.FromSeconds(0.02));
                                                     try
                                                     {
                                                         using (var client = new WebClient())
                                                         {
                                                             client.DownloadStringAsync(this.PrimaryUrl);
                                                         }
                                                     }
                                                     catch (Exception e)
                                                     {
                                                         this.WriteException(e);
                                                     }
                                                 });
                                             
                                             try
                                             {
                                                 while (this.httpListener.IsListening)
                                                 {
                                                     ThreadPool.QueueUserWorkItem(
                                                         c =>
                                                         {
                                                             try
                                                             {
                                                                 this.ProcessRequest(server, c);
                                                             }
                                                             catch (Exception e)
                                                             {
                                                                 
                                                                 this.WriteException(e);
                                                                 server = this.ProvisionServer("Webserver restarted...");
                                                                 this.ProcessRequest(server, c);
                                                             }
                                                         },
                                                         httpListener.GetContext());
                                                 }
                                             }
                                             catch (Exception e)
                                             {
                                                 this.WriteException(e);
                                             }
                                         });
        }

        public void Stop()
        {
            this.Dispose();
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            if (disposing)
            {
                this.httpListener.Stop();
                this.httpListener.Close();
            }
        }

        private IMockServer ProvisionServer(string logText)
        {
            var server = new MockServer(new JavascriptEnabledCapabilitiesFactory(), this.serverData);
            server.EagerInit();
            logger.WriteLine(logText);
            return server;
        }
        
        private void ProcessRequest(IMockServer server, object c)
        {
            var ctx = c as HttpListenerContext;
            
            if (ctx == null)
            {
                return;
            }

            bool close = true;
            try
            {
                var url = ctx.Request.Url;
                var path = url.AbsolutePath;
                this.logger.WriteLine("Request Received: " + ctx.Request.HttpMethod + " " + path);
                PageData pageData = new PageData
                {
                    VanityHost = url.Host,
                    Path = path,
                    QueryString = url.Query,
                    Cookies = ctx.Request.Cookies.OfType<Cookie>().ToArray(),
                    Method = ctx.Request.HttpMethod,
                    BodyContent = ctx.Request.InputStream.ReadFully(),
                    UrlScheme = url.Scheme,
                    Port = url.Port,
                    Headers = this.Convert(ctx.Request.Headers)
                        
                };

                string resultContent = string.Empty;
                IPageResult pageResult;
                try
                {
                    pageResult = server.RequestPage(pageData);
                }
                catch (RedirectException re)
                {
                    ctx.Response.StatusCode = re.StatusCode;
                    ctx.Response.RedirectLocation = re.RedirectLocation;
                    pageResult = re.PageResult;
                }
                catch (ResponseException cee)
                {
                    ctx.Response.StatusCode = cee.StatusCode;
                    pageResult = cee.PageResult;
                }

                if (pageResult != null)
                {
                    this.logger.WriteLine("Request Finished (" + pageResult.StatusCode + " / " + pageResult.ContentType + "): " + path);
                    
                    if (pageResult.Headers != null)
                    {
                        foreach (var header in pageResult.Headers.Keys)
                        {
                            SetHeaderValue(ctx, header, pageResult);
                        }
                    }
                    
                    resultContent = pageResult.RawText;
                    pageResult.Cookies.Values.ForAll(x => ctx.Response.Cookies.Add(x));
                    ctx.Response.ContentType = pageResult.ContentType;
                }

                ctx.Response.Headers.Add(HttpResponseHeader.Server, "Base2art-Chincilla/1.0");
                
                var contentEncoding = Encoding.Default;
                byte[] buf = contentEncoding.GetBytes(resultContent);
                ctx.Response.ContentLength64 = buf.Length;
                ctx.Response.ContentEncoding = contentEncoding;
                ctx.Response.OutputStream.Write(buf, 0, buf.Length);
            }
            catch (Exception e)
            {
                WriteException(e);
                if (e is RemotingException)
                {
                    close = false;
                    throw;
                }
            }
            finally
            {
                if (close)
                {
                    // almost always close the stream
                    ctx.Response.OutputStream.Close();
                }
            }
        }
        
        void WriteException(Exception e)
        {
            if (e == null)
            {
                return;
            }
            
            try
            {
                this.logger.WriteLine(e.GetType().ToString());
                this.logger.WriteLine(e.Message);
                this.logger.WriteLine(e.StackTrace);
                this.WriteException(e.InnerException);
            }
            catch (Exception)
            {
                #if DEBUG
                
                if (!File.Exists("c:\\Temp\\current.log"))
                {
                    File.WriteAllText("c:\\Temp\\current.log", "");
                }
                
                File.AppendAllLines("c:\\Temp\\current.log", new []{
                                        
                                        e.GetType().ToString(),
                                        e.Message,
                                        e.StackTrace
                                    });
                
                #endif
                Console.WriteLine(e.GetType());
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
                this.WriteException(e.InnerException);
            }
        }

        private IReadOnlyMap<string, string> Convert(System.Collections.Specialized.NameValueCollection headers)
        {
            var map = new Map<string, string>();
            
            foreach (var header in headers.AllKeys)
            {
                map[header] = headers[header];
            }
            
            return map;
        }

        void SetHeaderValue(HttpListenerContext ctx, string headerName, IPageResult pageResult)
        {
            var headerValue = pageResult.Headers[headerName];
            if (headerValue.IsKnownValue)
            {
                if (string.Equals(headerName, "www-authenticate", StringComparison.InvariantCultureIgnoreCase))
                {
                    var authTypeParts = headerValue.Value.Split(new[] {
                                                                    ' '
                                                                }, 2, StringSplitOptions.RemoveEmptyEntries);
                    AuthenticationSchemes foundType;
                    if (Enum.TryParse(authTypeParts[0], out foundType))
                    {
                        if (httpListener.AuthenticationSchemes.HasFlag(foundType))
                        {
                            return;
                        }
                        
                        httpListener.AuthenticationSchemes |= foundType;
                        var realm = authTypeParts[1];
                        this.logger.WriteLine(string.Format("Found Realm: '{0}'", realm));
                        
                        if (realm.StartsWith("Realm=\"", StringComparison.InvariantCultureIgnoreCase))
                        {
                            realm = realm.Substring("Realm=\"".Length);
                            if (realm.EndsWith("\"", StringComparison.InvariantCultureIgnoreCase))
                            {
                                realm = realm.Substring(0, realm.Length - 1);
                            }
                            this.logger.WriteLine(string.Format("Setting Realm: '{0}'", realm));
                            httpListener.Realm = realm;
                        }
                    }
                    
                    return;
                }
                
                if (string.Equals(headerName, "Content-Length", StringComparison.InvariantCultureIgnoreCase))
                {
                    return;
                }
                
                try
                {
                    ctx.Response.Headers[headerName] = headerValue.Value;
                }
                catch (Exception e)
                {
                    this.WriteException(e);
                    this.logger.WriteLine(string.Format("Error Setting Header: {0}", headerName));
                }
            }
            else
            {
                ctx.Response.Headers[headerName] = headerValue.Value;
            }
        }
    }
}
