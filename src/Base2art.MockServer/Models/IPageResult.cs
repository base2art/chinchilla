﻿namespace Base2art.MockServer
{
    using System.Net;

    using Base2art.Collections;

    public interface IPageResult
    {
        string Path { get; }

        string NormalizedText { get; }
        
        string RawText { get; }

        IReadOnlyMultiMap<string, Cookie> Cookies { get; }

        int StatusCode { get; }

        string ContentType { get; }

        IReadOnlyMap<string, HeaderValue> Headers { get; }
    }
}