﻿namespace Base2art.WebRunner
{
    public interface IWebPageInteracter
    {
        void FillIn(string locator, string with);

        void Select(string locator, string with);

        void Check(string locator);

        void Uncheck(string locator);

        void Choose(string locator);

        void ClickButton(string locator);

        void ClickLink(string locator);

        void TriggerPostBack(string locator);

        IScopeContext CreateScopeContext(string locator);

        IElement FindElement(string locator);

        IElement FindFirstElement(string locator);

        IElement[] FindElements(string locator);
    }
}