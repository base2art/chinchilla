﻿
using System;
namespace Base2art.WebRunner.Server.UI
{
    public class AttributeChangedEventArgs<T> 
    {
        private readonly T value;

        public AttributeChangedEventArgs(T value)
        {
            this.value = value;
        }

        public T Value
        {
            get
            {
                return value;
            }
        }
    }
}


