﻿namespace Base2art.MockUI
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;

    public class FormData : IFormData
    {
        private readonly string pathToElement;

        private readonly KeyedCollection<string, FormField> backingSet = new FormFieldCollection();

        public FormData(string pathToElement)
        {
            this.pathToElement = pathToElement;
        }

        public string PathToElement
        {
            get
            {
                return this.pathToElement;
            }
        }

        public IEnumerator<IFormField> GetEnumerator()
        {
            foreach (var field in this.backingSet)
            {
                yield return field;
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }

        public void Add(string path, InputType inputType, string name, string value, bool assignCurrentValue)
        {
            if (this.backingSet.Contains(name))
            {
                var formField = this.backingSet[name];
                var newOptions = new List<IFormFieldOption>(formField.Options);
                newOptions.Add(new FormFieldOption { PathToElement = path, Value = value });
                formField.Options = newOptions.ToArray();
            }
            else
            {
                this.Add(name, path, inputType, value, assignCurrentValue, new[] { new FormFieldOption { PathToElement = path, Value = value } });
            }
        }

        public void Add(string name, string pathToInputElement, InputType inputType, string value, bool assignCurrentValue, FormFieldOption[] options)
        {
            if (this.backingSet.Contains(name))
            {
                throw new InvalidOperationException("Duplicate Item");
            }

            var field = new FormField();
            field.Name = name;
            field.PathToElement = pathToInputElement;
            field.InputType = inputType;
            if (assignCurrentValue)
            {
                field.CurrentValue = value;
            }

            field.Options = options ?? new FormFieldOption[0];
            this.backingSet.Add(field);
        }
    }
}