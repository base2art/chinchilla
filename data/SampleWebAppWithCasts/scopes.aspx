﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="scopes.aspx.cs" Inherits="SampleWebAppWithCasts.scopes" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div id="block1">
        <asp:Label runat="server" AssociatedControlID="input1">My Input</asp:Label>
        <asp:TextBox runat="server" ID="input1" />
    </div>
    <div id="block2">
        <asp:Label runat="server" AssociatedControlID="input2">My Input</asp:Label>
        <asp:TextBox runat="server" ID="input2" />
    </div>
    <asp:Button runat="server" Text="Submit"/>
    </form>
</body>
</html>
