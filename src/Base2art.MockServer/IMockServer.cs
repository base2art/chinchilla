﻿namespace Base2art.MockServer
{
    using System;
    using System.Web.UI;

    public interface IMockServer : IDisposable
    {
        void EagerInit();

        IPageResult RequestPage(PageData pageData);

        IPageResult RequestControl<T>(ControlData<T> controlData)
            where T : Control;
    }
}