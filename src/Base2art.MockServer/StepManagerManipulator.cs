﻿namespace Base2art.MockServer
{
    using System;
    using System.Collections;
    using System.ComponentModel;
    using System.Linq;
    using System.Reflection;
    using System.Web;

    using Base2art.Collections;

    using ReflectionMagic;

    public sealed class StepManagerManipulator : Component
    {
        private const string SyncTypeName = "System.Web.HttpApplication+SyncEventExecutionStep";

        private readonly Assembly webAssembly;

        private readonly HttpApplication app;

        private readonly dynamic stepManager;

        private readonly dynamic[] steps;

        private readonly ArrayList arr;

        private readonly dynamic stepHandlerType;

        private object endStep;

        public StepManagerManipulator(Assembly webAssembly, HttpApplication app)
        {
            this.webAssembly = webAssembly;
            this.app = app;
            this.stepHandlerType = this.webAssembly.GetDynamicType("System.Web.HttpApplication+IExecutionStep");

            this.stepManager = app.AsDynamic()._stepManager;
            this.steps = (dynamic[])this.stepManager._execSteps;
            this.arr = new ArrayList(this.steps);
        }

        public object InsertDeletegateBefore(string name, EventHandler eventHandler)
        {
            return this.InsertAt(this.FindStep(name), eventHandler);
        }

        public object InsertDeletegateAfter(string name, EventHandler eventHandler)
        {
            return this.InsertAt(this.FindStep(name) + 1, eventHandler);
        }

        public object ReplaceEndRequestDelegate(EventHandler eventHandler)
        {
            int endStepIndex = (int)this.stepManager._endRequestStepIndex;
            var newEndIndex = this.arr.Cast<object>().IndexOf(x => x == this.steps[endStepIndex]);
            return this.endStep = this.InsertAt(newEndIndex, eventHandler);
        }

        public bool HasStep(string name)
        {
            return this.arr.OfType<object>().Any(x => x.GetType().Name.EndsWith(name));
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            if (disposing)
            {
                this.stepManager._endRequestStepIndex = this.arr.Cast<object>().IndexOf(x => x == this.endStep);
                var array = Array.CreateInstance(this.stepHandlerType.RealObject, this.arr.Count);
                for (int j = 0; j < this.arr.Count; j++)
                {
                    var o = this.arr[j];
                    array[j] = o;
                }

                this.app.AsDynamic()._stepManager._execSteps = array;
            }
        }

        private int FindStep(string name)
        {
            return this.arr.OfType<object>().IndexOf(x => x.GetType().Name.EndsWith(name));
        }

        private object InsertAt(int i, EventHandler handler)
        {
            object endRequest = this.webAssembly.CreateDynamicInstance(SyncTypeName, this.app, handler).RealObject;
            this.arr.Insert(i, endRequest);
            return endRequest;
        }
    }
}