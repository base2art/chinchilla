﻿namespace WebControlLibrary.UI.Controls
{
    using System;

    public abstract class AbstractWebControlBase : WebControlBase
    {
        public string GetSomeAbstractValue()
        {
            var page = this.Page as AbstractPageBase;
            if (page == null)
            {
                throw new InvalidOperationException("Control must be on PageBase");
            }

            return page.GetSomeAbstractValue();
        }
    }
}