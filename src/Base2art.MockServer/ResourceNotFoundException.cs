﻿namespace Base2art.MockServer
{
    using System;
    using System.Runtime.Serialization;

    [Serializable]
    public class ResourceNotFoundException : ResponseException
    {
        public ResourceNotFoundException(int statusCode, string simpleMessage, IPageResult pageResult)
            : base(statusCode, simpleMessage, pageResult)
        {
        }
        
        public ResourceNotFoundException(int statusCode, string simpleMessage, IPageResult pageResult, Exception innerException)
            : base(statusCode, simpleMessage, pageResult, innerException)
        {
        }

        public ResourceNotFoundException()
        {
        }

        public ResourceNotFoundException(string message)
            : this(404, message, null)
        {
        }

        public ResourceNotFoundException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        protected ResourceNotFoundException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}