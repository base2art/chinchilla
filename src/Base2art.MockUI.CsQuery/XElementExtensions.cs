﻿namespace Base2art.MockUI.CsQuery
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Linq;

    public static class XElementExtensions
    {
        public static KeyValuePair<string, int>[] ParsePath(this string simplePathExpression)
        {
            List<KeyValuePair<string, int>> parts = new List<KeyValuePair<string, int>>();

            foreach (var part in simplePathExpression.Split(new[] { '/' }, StringSplitOptions.RemoveEmptyEntries))
            {
                var name = new string(part.TakeWhile(x => x != '[').ToArray());
                var intValue = new string(part.SkipWhile(x => x != '[').ToArray());
                intValue = intValue.TrimStart('[').TrimEnd(']');
                parts.Add(new KeyValuePair<string, int>(name, int.Parse(intValue)));
            }

            return parts.ToArray();
        }

        ////public static XElement FindForm(this XElement domObject)
        ////{
        ////    if (domObject == null)
        ////    {
        ////        return null;
        ////    }

        ////    if (domObject.Name.LocalName.ToLowerInvariant() == "form")
        ////    {
        ////        return domObject;
        ////    }

        ////    return FindForm(domObject.Parent);
        ////}

        public static string GeneratePath(this XElement domObject)
        {
            var items = new List<string>();
            GeneratePathInternal(domObject, items);
            items.Reverse();
            return string.Concat("/", string.Join("/", items)).ToLowerInvariant();
        }

        private static void GeneratePathInternal(XElement domObject, List<string> items)
        {
            if (domObject == null)
            {
                return;
            }

            var index = 0;
            var parent = domObject.Parent;
            if (parent == null)
            {
                items.Add(domObject.Name.LocalName + "[1]");
                return;
            }

            var siblings = parent.Elements().ToArray();

            int counter = 0;
            for (var i = 0; i < siblings.Length; i++)
            {
                if (string.Equals(siblings[i].Name.LocalName, domObject.Name.LocalName, StringComparison.InvariantCultureIgnoreCase))
                {
                    counter += 1;
                }

                if (siblings[i] == domObject)
                {
                    index = counter;
                    break;
                }
            }

            items.Add(domObject.Name.LocalName + "[" + index + "]");
            GeneratePathInternal(parent, items);
        }
    }
}
