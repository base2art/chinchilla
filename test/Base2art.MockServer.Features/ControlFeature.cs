﻿namespace Base2art.MockServer.Features
{
    using System.Web.UI;
    using System.Web.UI.WebControls;

    using Base2art.MockServer.Features.Fixtures.Controls;
    using Base2art.MockServer.Util;

    using FluentAssertions;

    using NUnit.Framework;

    [TestFixture]
    public class ControlFeature
    {
        [Test]
        public void ShouldLoadControl()
        {
            using (var server = new MockServer())
            {
                var rezult = server.RequestControl(new ControlData<TextBox>
                {
                    Path = "Default.aspx",
                    QueryString = "qsValue=23",
                    Control = new ControlCreator<TextBox>(() => new TextBox { Text = "23" }),
                    PlaceInsideForm = true
                });

                rezult.RawText.Should().Contain("23");
            }
        }

        [Test]
        public void ShouldGetQueryString()
        {
            using (var server = new MockServer())
            {
                var rezult = server.RequestControl(new ControlData<QueryStringControl>
                {
                    Path = "Default.aspx",
                    QueryString = "qsValue=42",
                    Control = new ControlCreator<QueryStringControl>(() => new QueryStringControl { Key = "qsValue" }),
                    PlaceInsideForm = false
                });

                rezult.RawText.Should().Be("42");
            }
        }

        [Test]
        public void ShouldLoadControlWithDefault()
        {
            using (var server = new MockServer())
            {
                var rezult = server.RequestControl(new ControlData<TextBox>
                {
                    Path = "Default.aspx",
                    PageType = PageType.DefaultPageInstance,
                    QueryString = "qsValue=23",
                    Control = new ControlCreator<TextBox>(() => new TextBox { Text = "23" }),
                    PlaceInsideForm = true
                });

                rezult.RawText.Should().Contain("23");
            }
        }


        [Test]
        public void ShouldLoadControlWithDefaultWithNoPath()
        {
            using (var server = new MockServer())
            {
                var rezult = server.RequestControl(new ControlData<TextBox>
                {
                    Path = "/",
                    PageType = PageType.DefaultPageInstance,
                    QueryString = "qsValue=23",
                    Control = new ControlCreator<TextBox>(() => new TextBox { Text = "23" }),
                    PlaceInsideForm = true
                });

                rezult.RawText.Should().Contain("23");
            }
        }

        [Test]
        public void ShouldLoadControlWithDefaultWithPathPrefixes()
        {
            using (var server = new MockServer())
            {
                var rezult = server.RequestControl(new ControlData<TextBox>
                {
                    Path = "\\Default.aspx",
                    PageType = PageType.DefaultPageInstance,
                    QueryString = "qsValue=23",
                    Control = new ControlCreator<TextBox>(() => new TextBox { Text = "23" }),
                    PlaceInsideForm = true
                });

                rezult.RawText.Should().Contain("23");
            }

            using (var server = new MockServer())
            {
                var rezult = server.RequestControl(new ControlData<TextBox>
                {
                    Path = "/Default.aspx",
                    PageType = PageType.DefaultPageInstance,
                    QueryString = "qsValue=23",
                    Control = new ControlCreator<TextBox>(() => new TextBox { Text = "23" }),
                    PlaceInsideForm = true
                });

                rezult.RawText.Should().Contain("23");
            }
        }

        public void ShouldLoadControlWithDefaultNoPath()
        {
            using (var server = new MockServer())
            {
                var rezult = server.RequestControl(new ControlData<TextBox>
                {
                    Path = string.Empty,
                    PageType = PageType.DefaultPageInstance,
                    QueryString = "qsValue=23",
                    Control = new ControlCreator<TextBox>(() => new TextBox { Text = "23" }),
                    PlaceInsideForm = true
                });

                rezult.RawText.Should().Contain("23");
            }
        }

        [Test]
        public void ShouldLoadControlWithDelegate()
        {
            using (var server = new MockServer())
            {
                var rezult = server.RequestControl(new ControlData<TextBox>
                {
                    PageType = PageType.Delegate,
                    QueryString = "qsValue=23",
                    Control = new ControlCreator<TextBox>(() => new TextBox { Text = "23" }),
                    PlaceInsideForm = true,
                    Page = new PageCreator<Page>(() => new Page())
                });

                rezult.RawText.Should().Contain("23");
            }
        }

        [Test]
        public void ShouldLoadControlWithDelegateClaimingDiskPath()
        {
            using (var server = new MockServer())
            {
                var rezult = server.RequestControl(new ControlData<TextBox>
                {
                    PageType = PageType.DiskPath,
                    QueryString = "qsValue=23",
                    Control = new ControlCreator<TextBox>(() => new TextBox { Text = "23" }),
                    PlaceInsideForm = true,
                    Page = new PageCreator<Page>(() => new Page())
                });

                rezult.RawText.Should().Contain("23");
            }
        }
    }
}
