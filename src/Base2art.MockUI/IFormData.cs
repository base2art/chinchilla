﻿namespace Base2art.MockUI
{
    using System.Collections.Generic;

    public interface IFormData : IEnumerable<IFormField>
    {
        string PathToElement { get; }

        ////IReadOnlyMap<string, string> Inputs { get; }

////        IReadOnlyMultiMap<string, string> PostData { get; }
    }
}