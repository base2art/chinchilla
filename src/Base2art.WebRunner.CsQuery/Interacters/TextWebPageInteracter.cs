﻿namespace Base2art.WebRunner.CsQuery.Interacters
{
    using System.Linq;

    using CsQuery;

    public class TextWebPageInteracter : WebPageInteracterBase
    {
        public TextWebPageInteracter(CsQueryWebPage pageApi, IScopeContext scope, string[] explicitlyMatchedNodes)
            : base(pageApi, scope, false, explicitlyMatchedNodes)
        {
        }

        protected override IDomObjectWrapper[] FindTargetElements(string locator)
        {
            return this.ScopedNode.Cq().Find(string.Join(", ", this.ExplicitlyMatchedNodes))
                .Select(x => new DomObjectWrapper(x))
                .Where(x => x.TextValue.Contains(locator))
                .ToArray();
        }
    }
}