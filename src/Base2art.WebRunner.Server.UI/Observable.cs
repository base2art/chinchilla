﻿
namespace Base2art.WebRunner.Server.UI
{
    using System;
    public class Observable<T>
    {
        private T value;

        public event EventHandler<AttributeChangingEventArgs<T>> Changing;
        
        public event EventHandler<AttributeChangedEventArgs<T>> Changed;
        
        public T Value
        {
            get
            {
                return this.value;
            }
            
            set
            {
                this.OnChanging(new AttributeChangingEventArgs<T>(this.value, value));
                this.value = value;
                this.OnChanged(new AttributeChangedEventArgs<T>(this.value));
            }
        }

        protected virtual void OnChanged(AttributeChangedEventArgs<T> e)
        {
            var handler = Changed;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        protected virtual void OnChanging(AttributeChangingEventArgs<T> e)
        {
            var handler = Changing;
            if (handler != null)
            {
                handler(this, e);
            }
        }
    }
}
