﻿namespace Base2art.MockServer.Features
{
    using System;
    using System.Text.RegularExpressions;
    using Base2art.IO;
    using Base2art.MockServer.Util.Reflection;

    using FluentAssertions;

    using NUnit.Framework;

    [TestFixture]
    public class RequestFeature
    {
        private string SimpleSiteBaseDir
        {
            get
            {
                return typeof(RequestFeature)
                    .GetOriginatingAssemblyDirectory()
                    .Parent()
                    .Parent()
                    .Parent()
                    .Parent()
                    .Dir("test")
                    .Dir("Base2art.MockServer.Features")
                    .Dir("Fixtures")
                    .Dir("SimpleSite")
                    .FullName;
            }
        }

        [Test]
        public void ShouldGetPageWithBases()
        {
            var serverData = ServerDataFactory.Create();
            serverData.UseDirectLoading.Should().BeFalse();
            using (var server = new MockServer(serverData))
            {
                var rezult = server.RequestPage(new PageData
                                                {
                                                    Path = "About.aspx"
                                                });

                rezult.RawText.Should().Contain("--s--42--e--");
            }
        }

        [Test]
        public void ShouldGetPage()
        {
            var serverData = new ServerData { PhysicalApplicationDirectory = this.SimpleSiteBaseDir };
            using (var server = new MockServer(serverData))
            {
                var rezult = server.RequestPage(new PageData
                                                {
                                                    Path = "Default.aspx",
                                                    QueryString = "qsValue=43"
                                                });

                rezult.RawText.Should().Contain("START-&gt;43&lt;-END");
            }
        }

        [Test]
        public void ShouldGetPageSkipNormalization()
        {
            var serverData = new ServerData { PhysicalApplicationDirectory = this.SimpleSiteBaseDir };
            using (var server = new MockServer(serverData))
            {
                var rezult = server.RequestPage(new PageData
                                                {
                                                    Path = "Default.aspx",
                                                    QueryString = "qsValue=%20%2043",
                                                });

                rezult.RawText.Should().Contain("START-&gt;  43&lt;-END");
                rezult.NormalizedText.Should().Contain("START-&gt; 43&lt;-END");
            }
        }

        [Test]
        public void ShouldThrowExpectionOnRedirect()
        {
            var serverData = new ServerData { PhysicalApplicationDirectory = this.SimpleSiteBaseDir, UseDirectLoading = false };
            using (var server = new MockServer(serverData))
            {
                new Action(() => server.RequestPage(new PageData { Path = "RedirectPage.aspx" })).ShouldThrow<RedirectException>();
            }
        }

        [Test]
        public void ShouldThrowExpectionOnResourceNotFound()
        {
            var serverData = new ServerData { PhysicalApplicationDirectory = this.SimpleSiteBaseDir };
            using (var server = new MockServer(serverData))
            {
                new Action(() => server.RequestPage(new PageData { Path = "NULL_PAGE.aspx" })).ShouldThrow<ResourceNotFoundException>();
            }
        }

        [Test]
        public void ShouldFindFilesOnDisk()
        {
            var serverData = new ServerData { PhysicalApplicationDirectory = this.SimpleSiteBaseDir };
            using (var server = new MockServer(serverData))
            {
                var result = server.RequestPage(new PageData { Path = "file-on-disk.css" });
                result.RawText.Should().Contain("background-color: hotpink;");
            }
        }

        [Test]
        public void ShouldGetAxds()
        {
            var serverData = new ServerData { PhysicalApplicationDirectory = this.SimpleSiteBaseDir };
            using (var server = new MockServer(serverData))
            {
                
                var result1 = server.RequestPage(new PageData { Path = "Default.aspx" });
                var text = result1.NormalizedText;
                var regex = new Regex("d=[A-Za-z0-9_-]*\\&amp;t=[0-9]*\\\"");
                
                var matches = regex.Matches(text);
                Console.WriteLine(matches);
                var item = matches[0].Groups[0].Value;
                var result2 =
                    server.RequestPage(
                        new PageData
                        {
                            Path = "WebResource.axd",
                            QueryString = "?" + item.Replace("&amp;", "&")
                        });
                
                result2.RawText.Should().StartWith("var Page_ValidationVer = \"125\";");
            }
        }

        [Test]
        public void ShouldNotGetWebConfig()
        {
            using (var server = new MockServer(ServerDataFactory.Create()))
            {
                new Action(() => server.RequestPage(new PageData { Path = "Web.Config" })).ShouldThrow<ClientErrorException>();
            }
        }

        [Test]
        public void ShouldGetPageWithUniqueDomain()
        {
            using (var server = new MockServer(ServerDataFactory.Create()))
            {
                var rezult = server.RequestPage(new PageData
                                                {
                                                    VanityHost = "www.test.com",
                                                    Path = "About.aspx"
                                                });

                rezult.RawText.Should().Be("TEST DOMAIN FOUND");
            }
        }

        [Test]
        public void ShouldBeAbleToSupressRedirects()
        {
            using (var server = new MockServer(ServerDataFactory.Create()))
            {
                new Action(() => server.RequestPage(new PageData { Path = "NULL_PAGE.aspx" }))
                    .ShouldThrow<ResourceNotFoundException>().And.PageResult.RawText.Should().Contain("ERROR 404 PAGE");
            }
        }

        [Test]
        public void ShouldBeAbleToSeeServerExecutes()
        {
            using (var server = new MockServer(ServerDataFactory.Create()))
            {
                var rezult = server.RequestPage(new PageData { Path = "server-execute.aspx" });

                rezult.RawText.Should().Contain("ERROR 404 PAGE");
            }
        }
    }
}
