﻿namespace Base2art.WebRunner.CsQuery.Features
{
    using Base2art.MockServer;
    using Base2art.MockServer.Features;
    using Base2art.MockUI;
    using Base2art.MockUI.CsQuery;

    using FluentAssertions;

    using NUnit.Framework;

    [TestFixture]
    public class ScopingApiFeature
    {
        [TearDown]
        public void DisposeChinchilla()
        {
            var chinchilla = Chinchilla.Instance;
            if (chinchilla != null)
            {
                chinchilla.Dispose();
            }
        }

        [Test]
        public void ShouldScopeWhenFillingOutForms()
        {
            MockServer mockServer = new MockServer(new JavascriptEnabledCapabilitiesFactory(), ServerDataFactory.Create());
            var mockBrowser = new MockBrowser(new CsQueryFormParser(), mockServer);
            IChinchilla chinchilla = Chinchilla.SetInstance(mockBrowser, new WebPageFactory());

            chinchilla.WebPage.Visit("scopes.aspx");
            using (chinchilla.WebPage.Within(LocatorType.Css, "#block1"))
            {
                chinchilla.WebPage.FillIn("My Input", "Text");
            }

            chinchilla.WebPage.ClickButton("Submit");
            chinchilla.WebPage.Source.Should().Contain("<input name=\"input1\" type=\"text\" value=\"Text\" id=\"input1\" />");
            chinchilla.WebPage.Source.Should().Contain("<input name=\"input2\" type=\"text\" id=\"input2\" />");
        }
    }
}
