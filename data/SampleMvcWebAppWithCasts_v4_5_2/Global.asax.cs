﻿
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace SampleMvcWebAppWithCasts_v4_5_2
{
    public class MvcApplication : HttpApplication
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.Ignore("{resource}.axd/{*pathInfo}");
            
            routes.MapRoute(
                "Default",
                "{controller}/{action}/{id}",
                new {
                    controller = "Home",
                    action = "Index",
                    id = UrlParameter.Optional
                });
        }
        
        protected void Application_Start()
        {
            RegisterRoutes(RouteTable.Routes);
        }
    }
}
