﻿
namespace SampleWebAppWithCasts
{
    public partial class rewrite_dest : System.Web.UI.Page
    {
        protected override void OnPreInit(System.EventArgs e)
        {
            this.Context.RewritePath(this.Request.RawUrl);
            base.OnPreInit(e);
        }
    }
}