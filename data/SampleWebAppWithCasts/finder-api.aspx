﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="finder-api.aspx.cs" Inherits="SampleWebAppWithCasts.finder_api" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <div id="div1" class="container">
            Div 1
        </div>
        <div id="div2" class="container">
            Div 2
        </div>
    
        <div id="div3" class="container2">
            <div class='innerDiv'>
                Div 3
            </div>
        </div>
        <div id="div4" class="container2">
            <div class='innerDiv'>
                Div 4
            </div>
        </div>
    </div>
    </form>
</body>
</html>
