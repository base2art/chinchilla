﻿namespace Base2art.MockServer
{
    using System;
    using System.Collections.Specialized;
    using System.Net;

    using Base2art.Collections;

    [Serializable]
    public class PageResult : IPageResult
    {
        private string normalizedText;

        public string Path { get; set; }

        public string NormalizedText
        {
            get
            {
                if (this.normalizedText == null)
                {
                    this.normalizedText = this.RawText != null ? this.RawText.NormalizeText() : string.Empty;
                }

                return this.normalizedText;
            }
        }

        public string RawText { get; set; }

        public int StatusCode { get; set; }

        public string ContentType { get; set; }

        public IReadOnlyMultiMap<string, Cookie> Cookies { get; set; }

        public IReadOnlyMap<string, HeaderValue> Headers { get; set; }
    }
}