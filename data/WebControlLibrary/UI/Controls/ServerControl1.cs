﻿
namespace WebControlLibrary.UI.Controls
{
    using System;
    using System.ComponentModel;
    using System.Web.UI;

    [DefaultProperty("Text")]
    [ToolboxData("<{0}:ServerControl1 runat=server></{0}:ServerControl1>")]
    public class ServerControl1 : WebControlBase
    {
        [Bindable(true)]
        [Category("Appearance")]
        [DefaultValue("")]
        [Localizable(true)]
        public string Text
        {
            get
            {
                String s = (String)this.ViewState["Text"];
                return ((s == null) ? "[" + this.ID + "]" : s);
            }

            set
            {
                this.ViewState["Text"] = value;
            }
        }

        protected override void RenderContents(HtmlTextWriter output)
        {
            output.Write("--s--");
            output.Write(this.GetMajickText());
            output.Write("--e--");

            output.Write(this.Text);
        }
    }
}
