﻿namespace Base2art.WebRunner.Server
{
    using System;
    using System.ComponentModel;

    public class ConfigurableServerData
    {
        private string defaultPage;


        public string VirtualDirectory { get; set; }

        public bool UseDirectLoading { get; set; }

        public string PhysicalApplicationDirectory { get; set; }

        public string DefaultPage
        {
            get
            {
                return this.defaultPage ?? "default.aspx";
            }
            set
            {
                this.defaultPage = value;
            }
        }
    }
}


