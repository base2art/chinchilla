﻿namespace Base2art.MockServer
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Configuration;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Reflection;
    using System.Runtime.Remoting.Messaging;
    using System.ServiceModel;
    using System.Threading;
    using System.Web;
    using System.Web.Compilation;
    using System.Web.Hosting;
    using System.Web.ModelBinding;
    using System.Web.UI;

    using Base2art.Collections;
    using Base2art.MockServer.Modules;
    using Base2art.MockServer.Util;
    using Base2art.MockServer.Util.Converters;
    using Base2art.MockServer.Util.Reflection;
    using Base2art.Validation;

    using ReflectionMagic;
    
    using Page = System.Web.UI.Page;

    public class AspNetHost : MarshalByRefObject, IHost
    {
        private readonly HashSet<int> applications = new HashSet<int>();

        private dynamic applicationFactoryType;

        private Assembly assembly;

        ~AspNetHost()
        {
            // Finalizer calls Dispose(false)
            this.Dispose(false);
        }

        private static dynamic CurrentRuntime
        {
            get { return typeof(HttpRuntime).AsDynamicType()._theRuntime; }
        }

        public IPageResult RequestControl<T>(
            ICreator<HttpBrowserCapabilities> creator,
            IServerData parameters,
            ControlData<T> controlData)
            where T : Control
        {
            using (var sh = new StreamHolder())
            {
                var headers = new Map<string, HeaderValue>();
                HttpContext context = this.Setup(creator, parameters, controlData, sh.CreateWriter(), headers);

                var page = this.GetPageByType(parameters, controlData);

                if (page == null)
                {
                    throw new InvalidOperationException(
                        "Loading from Disk Is not supported for controls at this time...");
                }

                if (controlData.PlaceInsideForm)
                {
                    page.Init += (sender, args) =>
                    {
                        var form = page.Form;
                        if (form == null)
                        {
                            form = new System.Web.UI.HtmlControls.HtmlForm();
                            page.Controls.Add(form);
                        }

                        form.Controls.Add(controlData.Control.Create());
                    };
                }
                else
                {
                    page.Controls.Add(controlData.Control.Create());
                }

                var result = this.ProcessRequest(parameters, controlData, page, context, headers);
                //                context.Response.OutputStream.Flush();
                result.RawText = sh.ReadText();
                return result;
            }
        }

        public IPageResult RunPage(
            ICreator<HttpBrowserCapabilities> creator,
            IServerData parameters,
            PageData pageData)
        {
            using (var sh = new StreamHolder())
            {
                var headers = new Map<string, HeaderValue>();
                HttpContext context = this.Setup(creator, parameters, pageData, sh.CreateWriter(), headers);

                try
                {
                    var result = this.ProcessRequest(parameters, pageData, null, context, headers);
                    //                    context.Response.OutputStream.Flush();
                    result.RawText = sh.ReadText();
                    return result;
                }
                catch (ClientErrorException cee)
                {
                    var newPageResult = this.CreateExceptionalPageResult(sh, cee);
                    throw new ClientErrorException(cee.StatusCode, cee.SimpleMessage, newPageResult);
                }
                catch (RedirectException re)
                {
                    var newPageResult = this.CreateExceptionalPageResult(sh, re);
                    throw new RedirectException(re.StatusCode, re.RedirectLocation, re.SimpleMessage, newPageResult);
                }
                catch (ResourceNotFoundException rnfe)
                {
                    var newPageResult = this.CreateExceptionalPageResult(sh, rnfe);
                    throw new ResourceNotFoundException(rnfe.StatusCode, rnfe.SimpleMessage, newPageResult, rnfe);
                }
                catch (ServerErrorException see)
                {
                    var newPageResult = this.CreateExceptionalPageResult(sh, see);
                    throw new ServerErrorException(see.StatusCode, see.SimpleMessage, newPageResult, see.InnerException);
                }
                catch (Exception see)
                {
                    Console.WriteLine(see.Message);
                    Console.WriteLine(see.GetType());
                    Console.WriteLine(see.StackTrace);
                    throw;
                }
            }
        }

        public void Init(string defaultPage)
        {
            var appType = typeof(HttpApplication);
            this.assembly = appType.Assembly;
            this.applicationFactoryType = this.assembly.GetDynamicType("System.Web.HttpApplicationFactory");

            var swr = new SimpleWorkerRequest(defaultPage, string.Empty, new StringWriter());
            HttpRuntime.ProcessRequest(swr);
        }

        public AppDomain CurrentAppDomain()
        {
            return AppDomain.CurrentDomain;
        }

        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
        }

        protected virtual HttpContext Setup(
            ICreator<HttpBrowserCapabilities> creator,
            IServerData parameters,
            RequestDataBase requestData,
            TextWriter writer,
            IMap<string, HeaderValue> headers)
        {
            requestData.Validate().IsNotNull();

            var capabilities = creator.Create();
            var workerRequest = new CustomSimpleWorkerRequest(
                string.Equals(requestData.UrlScheme, Uri.UriSchemeHttps),
                requestData.Method,
                requestData.VanityHost,
                requestData.Port,
                requestData.Path,
                requestData.QueryString,
                requestData.Headers,
                requestData.BodyContent,
                requestData.Post,
                writer,
                headers);
            
            var context = new HttpContext(workerRequest);
            
            HttpContext.Current = context;

            context.Response.ContentEncoding = System.Text.Encoding.Default;
            context.Response.AsDynamic().InitResponseWriter();

            requestData.Cookies.ForAll(x => context.Request.Cookies.Add(x.Convert()));

            CurrentRuntime.Init();

            context.Request.Browser = capabilities;

            return context;
        }
        
        // Page page,
        private PageResult ProcessRequest(
            IServerData parameters,
            RequestDataBase requestData,
            Page page,
            HttpContext context,
            IReadOnlyMap<string, HeaderValue> headers)
        {
            requestData.ContextSetup.Invoke(AppDomain.CurrentDomain, context);
            parameters.ContextSetup.Invoke(AppDomain.CurrentDomain, context);

            ContextExtensionData contextExtensionData = new ContextExtensionData
            {
                HasException = false,
                Page = page,
                RequestData = requestData,
                ServerData = parameters,
            };
            
            context.SetExtensionData(contextExtensionData);

            var factoryType = this.applicationFactoryType;

            var type = this.assembly.GetDynamicType("System.Web.Compilation.PreStartInitStage");
            typeof(BuildManager).AsDynamicType().PreStartInitStage = type.AfterPreStartInit;
            
            
            IHttpHandler handler = factoryType.GetApplicationInstance(context);

            handler.Validate().IsNotNull();
            var app = (HttpApplication)handler;

            try
            {
                this.SetupApplication(app);

                IHttpAsyncHandler httpAsyncHandler = app;

                context.AsDynamic().AsyncAppHandler = httpAsyncHandler;

                AsyncCallback callback = CurrentRuntime._handlerCompletionCallback;
                IAsyncResult result = httpAsyncHandler.BeginProcessRequest(context, callback, context);
                if (result == null)
                {
                    throw new InvalidOperationException("Not Async");
                }
                else
                {
                    var handle = result.AsyncWaitHandle;
                    if (handle != null)
                    {
                        WaitHandle.WaitAll(new[]{ handle }, TimeSpan.FromSeconds(35), false);
                    }
                    else
                    {
                        while (!result.IsCompleted)
                        {
                            Thread.Sleep(TimeSpan.FromSeconds(0.001));
                        }
                    }
                }

                var httpResponse = context.Response;

                var responseCookies = new MultiMap<string, Cookie>();

                var cookies = httpResponse.Cookies;

                foreach (var header in cookies.AllKeys)
                {
                    Cookie[] cookie = { cookies.Get(header).Convert() };
                    foreach (var value in cookie)
                    {
                        responseCookies.Add(header, value);
                    }
                }

                //                new Action(httpResponse.Flush).SupressExceptions();

                var statusCode = httpResponse.StatusCode;
                PageResult pageResult = new PageResult
                {
                    Path = requestData.Path,
                    Cookies = responseCookies,
                    StatusCode = statusCode,
                    ContentType = httpResponse.ContentType,
                };
                
                
                if (httpResponse.HeadersWritten)
                {
                    pageResult.Headers = headers ?? new Map<string, HeaderValue>();
                }

                var exception = context.Error as HttpException;
                if (exception != null)
                {
                    var exceptionStatusCode = exception.GetHttpCode();
                    this.ThrowExceptionOnStatusCodeNot200(exceptionStatusCode, httpResponse, exception, pageResult);
                }
                
                var configException = context.Error as ConfigurationErrorsException;
                if (configException != null)
                {
                    this.ThrowExceptionOnStatusCodeNot200(500, httpResponse, configException, pageResult);
                }
                
                var generalException = context.Error as Exception;
                if (generalException != null)
                {
                    this.ThrowExceptionOnStatusCodeNot200(500, httpResponse, generalException, pageResult);
                }


                this.ThrowExceptionOnStatusCodeNot200(statusCode, httpResponse, null, pageResult);
                return pageResult;
            }
            finally
            {
                context.ClearExtensionData();

                app.CompleteRequest();

                HttpContext.Current = null;
            }
        }

        private void SetupApplication(HttpApplication app)
        {
            var hashCode = app.GetHashCode();

            if (!this.applications.Contains(hashCode))
            {
                this.applications.Add(hashCode);
                var delegator = new HttpExtensibleApplicationDelegator();

                ModuleConfiguration.FindModules().ForAll(module => module.Init(delegator));

                HostUtil.SetupBuildSteps(this.assembly, app, delegator);
            }
            
            var hm = (object)(typeof(ServiceHostingEnvironment).AsDynamicType().hostingManager);
            
            if (hm != null)
            {
                hm.AsDynamic().isRecycling = false;
            }
        }

        private void ThrowExceptionOnStatusCodeNot200(int statusCode, HttpResponse httpResponse, Exception exception, IPageResult pageResult)
        {
            if (300 <= statusCode && statusCode < 400)
            {
                var simpleMessage = string.Format("This page is redirecting to '{0}", httpResponse.RedirectLocation);
                throw new RedirectException(statusCode, httpResponse.RedirectLocation, simpleMessage, pageResult);
            }

            string fallback = new Func<string>(() => ((HttpStatusCode)statusCode).ToString("G"))
                .SupressExceptions();

            string exceptionText = exception == null ? null : exception.Message;

            if (404 == statusCode)
            {
                throw new ResourceNotFoundException(statusCode, exceptionText ?? fallback ?? string.Empty, pageResult);
            }

            if (400 <= statusCode && statusCode < 500)
            {
                throw new ClientErrorException(statusCode, exceptionText ?? fallback ?? string.Empty, pageResult);
            }

            if (500 <= statusCode && statusCode < 600)
            {
                Exception lastError = exception as HttpUnhandledException;
                if (lastError != null && exception.InnerException != null)
                {
                    exception = lastError.InnerException;
                    exceptionText = exception == null ? null : exception.Message;
                }

                throw new ServerErrorException(statusCode, exceptionText ?? fallback ?? string.Empty, pageResult, exception);
            }
        }

        private Page GetPageByType<T>(IServerData parameters, ControlData<T> controlData)
            where T : Control
        {
            if (controlData.PageType == PageType.DiskPath)
            {
                var factory = ModuleConfiguration.FindModules().OfType<IPageCreationFactory>().FirstOrDefault();
                if (factory == null)
                {
                    throw new InvalidOperationException("Developer Error: The Module Should Implement the factory.");
                }

                return factory.CreatePage<Page>(controlData.Path);
            }

            if (controlData.PageType == PageType.Delegate)
            {
                return controlData.Page.Create(controlData.Path);
            }

            if (controlData.PageType == PageType.DefaultPageInstance)
            {
                string path = controlData.Path ?? "/";
                path = new string(path.SkipWhile(x => x == '/' || x == '\\').ToArray());
                if (string.IsNullOrWhiteSpace(path))
                {
                    return new Page();
                }

                Directory.CreateDirectory(parameters.PhysicalApplicationDirectory);
                var outFile = Path.Combine(parameters.PhysicalApplicationDirectory, path);
                if (!File.Exists(outFile))
                {
                    File.WriteAllText(outFile, string.Empty);
                }

                return new Page();
            }

            throw new InvalidOperationException("Missing a switch");
        }

        private PageResult CreateExceptionalPageResult(StreamHolder sh, ResponseException re)
        {
            var readText = sh.ReadText();
            var pageResult = re.PageResult;
            var newPageResult = new PageResult
            {
                Cookies = pageResult.Cookies,
                Path = pageResult.Path,
                RawText = readText,
                StatusCode = pageResult.StatusCode,
                ContentType = pageResult.ContentType,
                Headers = pageResult.Headers
            };
            
            return newPageResult;
        }
    }
}

//                if (contextExtensionData.HasException)
//                {
//                    var lastError = context.Server.GetLastError();
//                    if (lastError.InnerException != null && lastError is HttpUnhandledException)
//                    {
//                        lastError = lastError.InnerException;
//                    }
//
//                    throw new ApplicationException("Page Execution Error", lastError);
//                }

/*
if (string.IsNullOrWhiteSpace(controlData.Path))
{
    return new Page();
}

if ("/" == controlData.Path)
{
    return new Page();
}

if ("\\" == controlData.Path)
{
    return new Page();
}

//throw new InvalidOperationException("If you set the Control Path," +
//    " then you must also set the type to disk path.");

//string path = controlData.Path ?? "/";
//path = new string(path.SkipWhile(x => x == '/' || x == '\\').ToArray());

//return this.pageCreationFactory.CreatePage<Page>(path);
 */
////            var runtime = typeof(HttpRuntime);
////            runtime.GetField("_useIntegratedPipeline", BindingFlags.NonPublic | BindingFlags.Static)
////                   .SetValue(null, true);

////            runtime.GetMethod("ProcessRequestNoDemand", BindingFlags.NonPublic | BindingFlags.Static)
////                   .Invoke(null, new object[] { swr });
////var appStateType = typeof(HttpApplicationState);
////const BindingFlags BindingFlags = BindingFlags.NonPublic | BindingFlags.CreateInstance | BindingFlags.Instance;
////var appStateCtor = appStateType.GetConstructor(BindingFlags, null, new Type[0], new ParameterModifier[0]);
////            this.applicationState =
////                (HttpApplicationState)
////                appStateCtor.Invoke(new object[0]);
////
////            this.applicationFactoryType = typeof(HttpApplication).Assembly.GetType("System.Web.HttpApplicationFactory");
////            // Hack to-re-init the app;
////            this.appFactory = this.applicationFactoryType.GetField("_theApplicationFactory", StandardBinding.StaticBindingFlags | BindingFlags.GetField).GetValue(null);
////            Stack stack = (Stack)this.applicationFactoryType.GetField("_freeList", StandardBinding.StandardBindingFlags | BindingFlags.GetField).GetValue(this.appFactory);
////            while (stack.Count > 0)
////            {
////                stack.Pop();
////            }
////
////            this.applicationFactoryType.GetField("_numFreeAppInstances", StandardBinding.StandardBindingFlags | BindingFlags.SetField).SetValue(this.appFactory, 0);

////
////            context.ApplicationInstance = app;
////            app.Context = context;
////            context.ApplicationInstance.CallProcedure(null, "InitInternal", context, this.applicationState, new MethodInfo[0]);

////            var bindingFlags = BindingFlags.CreateInstance | BindingFlags.NonPublic;
////            var type = typeof(HttpSessionState);
////            var container = new HttpSessionStateContainer("", new SessionStateItemCollection(), new HttpStaticObjectsCollection(), 10000, true, HttpCookieMode.UseCookies, SessionStateMode.InProc, false);
////            var session = Activator.CreateInstance(type, bindingFlags, null, new object[] { container }, CultureInfo.InvariantCulture);

////        private HttpApplicationState applicationState;
////        private Type applicationFactoryType;
////        private object appFactory;

////            var handlerStack = new EventedStack();
////            handlerStack.Changed += (sender, args) =>
////                {
////                    Console.WriteLine(handlerStack.Peek());
////                };
////            context.SetField("_handlerStack", handlerStack);