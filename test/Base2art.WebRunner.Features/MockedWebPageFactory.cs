﻿namespace Base2art.WebRunner.Features
{
    using Base2art.MockUI;

    public class MockedWebPageFactory : IWebPageFactory
    {
        private readonly IWebPage webPage;

        public MockedWebPageFactory(IWebPage webPage)
        {
            this.webPage = webPage;
        }

        public IWebPage Create(LocatorType defaultLocatorType, IMockBrowser mockBrowser)
        {
            return this.webPage;
        }
    }
}