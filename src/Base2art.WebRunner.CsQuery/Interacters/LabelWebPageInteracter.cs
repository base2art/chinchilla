﻿namespace Base2art.WebRunner.CsQuery.Interacters
{
    using System.Collections.Generic;
    using System.Linq;

    using Base2art.Collections;
    using Base2art.MockUI.CsQuery;

    using global::CsQuery;

    public class LabelWebPageInteracter : WebPageInteracterBase
    {
        public LabelWebPageInteracter(CsQueryWebPage pageApi, IScopeContext scope, string[] explicitlyMatchedNodes)
            : base(pageApi, scope, true, explicitlyMatchedNodes)
        {
        }

        protected override IDomObjectWrapper[] FindTargetElements(string label)
        {
            var matchingLabels = this.ScopedNode.Cq().Find("label").Where(
                x =>
                {
                    var text = x.Text();
                    return text.Contains(label);
                }).ToArray();

            List<IDomObjectWrapper> rezults = new List<IDomObjectWrapper>();

            matchingLabels.ForAll(matchingLabel =>
            {
                var matchingId = matchingLabel["for"];
                var items = this.ScopedNode.Cq().Find("#" + matchingId);
                IEnumerable<IDomObject> domObjects = items;
                if (this.ExplicitlyMatchedNodes.Length > 0)
                {
                    domObjects = domObjects.Where(x => x.NodeName.ToLowerInvariant().In(this.ExplicitlyMatchedNodes));
                }

                rezults.AddRange(domObjects.Select(x => new DomObjectWrapper(x)));
            });

            return rezults.ToArray();
        }
    }
}

/*
var matchingLabel = x;
if (!matchingLabel.HasAttribute("for"))
{
    throw new AmbiguousMatchFoundException("There is no for attr on this label");
}

var matchingId = matchingLabel["for"];
if (string.IsNullOrWhiteSpace(matchingId))
{
    throw new AmbiguousMatchFoundException("There is no for attr on this label");
}

var items = this.PageApi.CurrentPage.Document().Find("#" + matchingId);
if (items.Length == 0)
{
    throw new AmbiguousMatchFoundException("There is no Element with this Id: '" + matchingId + "'");
}

if (items.Length != 1)
{
    throw new AmbiguousMatchFoundException("There is multiple Elements with this Id: '" + matchingId + "'");
}

 rezults.Add(new DomObjectWrapper(items[0]));
 */