﻿namespace Base2art.WebRunner.CsQuery.Features
{
    using System;
    using System.Diagnostics;

    using Base2art.MockServer;
    using Base2art.MockServer.Features;
    using Base2art.MockUI;
    using Base2art.MockUI.CsQuery;

    using NUnit.Framework;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class ApiFeature
    {
        [TearDown]
        public void DisposeChinchilla()
        {
            var chinchilla = Chinchilla.Instance;
            if (chinchilla != null)
            {
                chinchilla.Dispose();
            }
        }

        [Test]
        public void ShouldHaveFluentApiInstance()
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();

            // Setup normally done in an [SetupFixture] class
            // but I do it here for testability.
            // http://www.nunit.org/index.php?p=setupFixture&r=2.4
            MockServer mockServer = new MockServer(new JavascriptEnabledCapabilitiesFactory(), ServerDataFactory.Create());
            var mockBrowser = new MockBrowser(new CsQueryFormParser(), mockServer);
            IChinchilla chinchilla = Chinchilla.SetInstance(mockBrowser, new WebPageFactory());

            chinchilla.WebPage.Visit("fluent-post-back/text-boxes.aspx");


            for (int i = 0; i < 1000; i++)
            {
                chinchilla.WebPage.FillIn(LocatorType.Css, ".control-1 input", "hello world");
                chinchilla.WebPage.ClickButton(LocatorType.Css, ".post-back-button input");
                ////chinchilla.WebPage.Source.Should().Contain("name=\"Control1\" type=\"text\" value=\"hello world\" id=\"Control1\" />");
                ////chinchilla.WebPage.Source.Should().Contain("name=\"Control2\" type=\"text\" id=\"Control2\" />");
                ////chinchilla.WebPage.Source.Should().Contain("name=\"Control3\" type=\"text\" value=\"hello world\" id=\"Control3\" />");   
            }

            Console.WriteLine(sw.Elapsed);
        }

        [Test]
        public void ShouldHaveFluentApiStatic()
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();

            // Setup normally done in an [SetupFixture] class
            // but I do it here for testability.
            // http://www.nunit.org/index.php?p=setupFixture&r=2.4
            MockServer mockServer = new MockServer(new JavascriptEnabledCapabilitiesFactory(), ServerDataFactory.Create());
            var mockBrowser = new MockBrowser(new CsQueryFormParser(), mockServer);
            Chinchilla.SetInstance(mockBrowser, new WebPageFactory());

            WebPage.Visit("fluent-post-back/text-boxes.aspx");


            for (int i = 0; i < 1000; i++)
            {
                WebPage.FillIn(LocatorType.Css, ".control-1 input", "hello world");
                WebPage.ClickButton(LocatorType.Css, ".post-back-button input");
                ////chinchilla.WebPage.Source.Should().Contain("name=\"Control1\" type=\"text\" value=\"hello world\" id=\"Control1\" />");
                ////chinchilla.WebPage.Source.Should().Contain("name=\"Control2\" type=\"text\" id=\"Control2\" />");
                ////chinchilla.WebPage.Source.Should().Contain("name=\"Control3\" type=\"text\" value=\"hello world\" id=\"Control3\" />");   
            }

            Console.WriteLine(sw.Elapsed);
        }
    }
}
