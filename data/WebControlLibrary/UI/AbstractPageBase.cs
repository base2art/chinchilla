﻿namespace WebControlLibrary.UI
{
    public abstract class AbstractPageBase : PageBase
    {
        public string GetSomeAbstractValue()
        {
            return this.DoGetSomeAbstractValue();
        }

        protected virtual string DoGetSomeAbstractValue()
        {
            return "666";
        }
    }
}