﻿namespace Base2art.MockUI.CsQuery
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Xml.Linq;

    using Base2art.MockServer;
    using Base2art.Validation;

    using global::CsQuery;

    public static class DomObjectExtensions
    {
        public static XDocument ToXDocument(this IDomDocument document)
        {
            var rootElements = document.ChildElements;
            rootElements.Count().Validate().Is(1);

            var doc = new XDocument();
            var domElement = rootElements.First();
            XElement rootElement = new XElement(domElement.NodeName.ToLowerInvariant());
            doc.Add(rootElement);
            CopyNode(rootElement, domElement);

            return doc;
        }

        public static XElement ToXElement(this IDomElement domElement)
        {
            var doc = new XDocument();
            XElement rootElement = new XElement(domElement.NodeName.ToLowerInvariant());
            doc.Add(rootElement);
            CopyNode(rootElement, domElement);

            return doc.Root;
        }

        public static string Text(this IDomObject domObject)
        {
            StringBuilder sb = new StringBuilder();
            TextInternal(domObject, sb);
            return sb.ToString().NormalizeText().Trim();
        }

        public static IDomObject FindForm(this IDomObject domObject)
        {
            if (domObject == null)
            {
                return null;
            }

            if (domObject.NodeName.ToLowerInvariant() == "form")
            {
                return domObject;
            }

            return FindForm(domObject.ParentNode);
        }

        public static string GeneratePath(this IDomObject domObject)
        {
            var items = new List<string>();
            GeneratePathInternal(domObject, items);
            items.Reverse();
            return string.Concat("/", string.Join("/", items)).ToLowerInvariant();
        }

        private static void TextInternal(IDomObject domObject, StringBuilder sb)
        {
            if (domObject.NodeType == NodeType.TEXT_NODE)
            {
                sb.Append(domObject.NodeValue.NormalizeText());
            }
            else if (domObject.NodeType == NodeType.ELEMENT_NODE)
            {
                foreach (var childNode in domObject.ChildNodes)
                {
                    TextInternal(childNode, sb);
                }
            }
            else if (domObject.NodeType == NodeType.CDATA_SECTION_NODE)
            {
                sb.Append(domObject.Value.NormalizeText());
            }
            else if (domObject.NodeType == NodeType.COMMENT_NODE)
            {
                // PaSS
            }
            else
            {
                throw new ArgumentOutOfRangeException("Unknown Type: '" + domObject.NodeType + "'");
            }
        }

        private static void GeneratePathInternal(IDomObject domObject, List<string> items)
        {
            if (domObject == null)
            {
                return;
            }

            var index = 0;
            var parent = domObject.ParentNode;
            if (parent == null)
            {
                return;
            }

            var siblings = parent.ChildElements.ToArray();
            int counter = 0;
            for (var i = 0; i < siblings.Length; i++)
            {
                if (string.Equals(siblings[i].NodeName, domObject.NodeName, StringComparison.InvariantCultureIgnoreCase))
                {
                    counter += 1;
                }

                if (siblings[i] == domObject)
                {
                    index = counter;
                    break;
                }
            }

            items.Add(domObject.NodeName + "[" + index + "]");
            GeneratePathInternal(parent, items);
        }

        private static void CopyNode(XElement destination, IDomElement oldElement)
        {
            foreach (var attribute in oldElement.Attributes)
            {
                destination.Add(new XAttribute(attribute.Key, attribute.Value));
            }

            foreach (var node in oldElement.ChildNodes)
            {
                if (node.NodeType == NodeType.TEXT_NODE)
                {
                    destination.Add(new XText(node.NodeValue ?? string.Empty));
                }
                else if (node.NodeType == NodeType.ELEMENT_NODE)
                {
                    XElement xChild = new XElement(node.NodeName.ToLowerInvariant());
                    destination.Add(xChild);
                    CopyNode(xChild, (IDomElement)node);
                }
                else
                {
                    throw new InvalidOperationException("Off the end of the dom");
                }
            }
        }
    }
}
