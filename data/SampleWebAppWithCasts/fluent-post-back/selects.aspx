﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="selects.aspx.cs"
    Inherits="SampleWebAppWithCasts.selects" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
     THIS IS THE PAGE FOR SELECTS!
    <form id="form1" runat="server">
    <div>
        <div class="control-1">
            <asp:Label runat="server" AssociatedControlID="Control1">My First Control:</asp:Label>
            <asp:Label runat="server" AssociatedControlID="Control1">My Second Control:</asp:Label>
            <asp:DropDownList ID="Control1" runat="server">
                <asp:ListItem Value="1" Text="A" />
                <asp:ListItem Value="2" Text="B" />
                <asp:ListItem Value="3" Text="C" />
            </asp:DropDownList>
        </div>
        <div>
            <asp:DropDownList ID="Control2" runat="server">
                <asp:ListItem Value="4" Text="D" />
                <asp:ListItem Value="5" Text="E" />
                <asp:ListItem Value="6" Text="F" />
            </asp:DropDownList>
        </div>
        <div class="control-3">
            <asp:Label runat="server" AssociatedControlID="Control3">My Second Control:</asp:Label>
            <asp:DropDownList ID="Control3" runat="server">
                <asp:ListItem Value="7" Text="G" />
                <asp:ListItem Value="8" Text="H" />
                <asp:ListItem Value="9" Text="I" />
            </asp:DropDownList>
        </div>
        <div class="post-back-button">
            <asp:Button runat="server" ID="SubmitButton" OnClick="HandleClick"/>
        </div>
    </div>
    </form>
</body>
</html>
