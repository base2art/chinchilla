﻿namespace Base2art.MockServer
{
    using System;
    using System.Collections.Specialized;
    using System.Linq;
    using System.Net;
    using System.Web;

    using Base2art.Collections;
    using Base2art.MockServer.Util;

    [Serializable]
    public class RequestDataBase
    {
        private string queryString;

        private NameValueCollection post;

        private string path;

        private string method;
        
        private IDelegate<AppDomain, HttpContext> contextSetup;

        private Cookie[] cookies;

        private string host;
        
        private string urlScheme;
        
        private int? port;
        
        private IReadOnlyMap<string, string> headers;

        ////public bool SupressExceptions { get; set; }

        public Cookie[] Cookies
        {
            get
            {
                return this.cookies ?? new Cookie[0];
            }

            set
            {
                this.cookies = value;
            }
        }

        public string Path
        {
            get
            {
                return new string((this.path ?? "/").SkipWhile(x => x == '/' || x == '\\').ToArray());
            }

            set
            {
                this.path = value;
            }
        }
        
        public string UrlScheme
        {
            get
            {
                return string.IsNullOrWhiteSpace(this.urlScheme) ? Uri.UriSchemeHttp : this.urlScheme;
            }
            
            set
            {
                this.urlScheme = value;
            }
        }
        
        public int Port
        {
            get
            {
                if (this.port.HasValue)
                {
                    return this.port.Value;
                }
                
                if (string.Equals(this.UrlScheme, Uri.UriSchemeHttp))
                {
                    return 80;
                }
                
                if (string.Equals(this.UrlScheme, Uri.UriSchemeHttp))
                {
                    return 443;
                }
                
                return 0;
            }
            set
            {
                port = value;
            }
        }

        public string VanityHost
        {
            get
            {
                return string.IsNullOrWhiteSpace(this.host) ? null : this.host;
            }

            set
            {
                this.host = value;
            }
        }

        public string QueryString
        {
            get
            {
                return new string((this.queryString ?? string.Empty).SkipWhile(x => x == '?').ToArray());
            }
            
            set
            {
                this.queryString = value;
            }
        }

        public NameValueCollection Post
        {
            get
            {
                return this.post ?? new NameValueCollection();
            }

            set
            {
                this.post = value;
            }
        }
        
        public byte[] BodyContent { get; set; }

        public string Method
        {
            get
            {
                return string.IsNullOrWhiteSpace(this.method) ? "GET" : this.method;
            }

            set
            {
                this.method = value;
            }
        }
        
        public IReadOnlyMap<string, string> Headers
        {
            get
            {
                return headers ?? new Map<string, string>();
            }
            set
            {
                headers = value;
            }
        }

        public IDelegate<AppDomain, HttpContext> ContextSetup
        {
            get
            {
                return this.contextSetup ?? new NullDelegate<AppDomain, HttpContext>();
            }

            set
            {
                this.contextSetup = value;
            }
        }
    }
}