﻿namespace Base2art.WebRunner
{
    public enum LocatorType
    {
        Label = 0,
        Css = 1,
        Path = 2,
    }
}