﻿namespace Base2art.WebRunner.CsQuery.Features
{
    using Base2art.MockServer;
    using Base2art.MockServer.Features;
    using Base2art.MockUI;
    using Base2art.MockUI.CsQuery;

    using FluentAssertions;

    using NUnit.Framework;

    [TestFixture]
    public class RadioButtonApiFeature
    {
        [TearDown]
        public void DisposeChinchilla()
        {
            var chinchilla = Chinchilla.Instance;
            if (chinchilla != null)
            {
                chinchilla.Dispose();
            }
        }

        [Test]
        public void ShouldHaveFluentApiNoChangeWithoutSelecting()
        {
            // Setup normally done in an [SetupFixture] class
            // but I do it here for testability.
            // http://www.nunit.org/index.php?p=setupFixture&r=2.4
            var mockBrowser = new MockBrowser(new CsQueryFormParser(), new MockServer(ServerDataFactory.Create()));
            IChinchilla chinchilla = Chinchilla.SetInstance(mockBrowser, new WebPageFactory());

            chinchilla.WebPage.Visit("fluent-post-back/radio-buttons.aspx");
            chinchilla.WebPage.ClickButton(LocatorType.Css, ".post-back-button input");
            chinchilla.WebPage.Source.Should().Contain("<input id=\"Control1_0\" type=\"radio\" name=\"Control1\" value=\"1\" />");
            chinchilla.WebPage.Source.Should().Contain("<input id=\"Control1_1\" type=\"radio\" name=\"Control1\" value=\"2\" />");
            chinchilla.WebPage.Source.Should().Contain("<input id=\"Control1_2\" type=\"radio\" name=\"Control1\" value=\"3\" />");
        }

        [Test]
        public void ShouldHaveFluentApiByCss()
        {
            // Setup normally done in an [SetupFixture] class
            // but I do it here for testability.
            // http://www.nunit.org/index.php?p=setupFixture&r=2.4
            var mockBrowser = new MockBrowser(new CsQueryFormParser(), new MockServer(ServerDataFactory.Create()));
            IChinchilla chinchilla = Chinchilla.SetInstance(mockBrowser, new WebPageFactory());

            chinchilla.WebPage.Visit("fluent-post-back/radio-buttons.aspx");
            chinchilla.WebPage.Choose(LocatorType.Css, "#Control1_2");
            chinchilla.WebPage.ClickButton(LocatorType.Css, ".post-back-button input");
            chinchilla.WebPage.Source.Should().Contain("<input id=\"Control1_0\" type=\"radio\" name=\"Control1\" value=\"1\" />");
            chinchilla.WebPage.Source.Should().Contain("<input id=\"Control1_1\" type=\"radio\" name=\"Control1\" value=\"2\" />");
            chinchilla.WebPage.Source.Should().Contain("<input id=\"Control1_2\" type=\"radio\" name=\"Control1\" value=\"3\" checked=\"checked\" />");
        }

        [Test]
        public void ShouldHaveFluentApiByLabel()
        {
            // Setup normally done in an [SetupFixture] class
            // but I do it here for testability.
            // http://www.nunit.org/index.php?p=setupFixture&r=2.4
            var mockBrowser = new MockBrowser(new CsQueryFormParser(), new MockServer(ServerDataFactory.Create()));
            IChinchilla chinchilla = Chinchilla.SetInstance(mockBrowser, new WebPageFactory());

            chinchilla.WebPage.Visit("fluent-post-back/radio-buttons.aspx");
            chinchilla.WebPage.Choose("F");
            chinchilla.WebPage.ClickButton(LocatorType.Css, ".post-back-button input");
            chinchilla.WebPage.Source.Should().Contain("<input id=\"Control2_0\" type=\"radio\" name=\"Control2\" value=\"4\" />");
            chinchilla.WebPage.Source.Should().Contain("<input id=\"Control2_1\" type=\"radio\" name=\"Control2\" value=\"5\" />");
            chinchilla.WebPage.Source.Should().Contain("<input id=\"Control2_2\" type=\"radio\" name=\"Control2\" value=\"6\" checked=\"checked\" />");
        }

        [Test]
        public void ShouldHaveFluentApiByPath()
        {
            // Setup normally done in an [SetupFixture] class
            // but I do it here for testability.
            // http://www.nunit.org/index.php?p=setupFixture&r=2.4
            var mockBrowser = new MockBrowser(new CsQueryFormParser(), new MockServer(ServerDataFactory.Create()));
            IChinchilla chinchilla = Chinchilla.SetInstance(mockBrowser, new WebPageFactory());

            chinchilla.WebPage.Visit("fluent-post-back/radio-buttons.aspx");
            chinchilla.WebPage.Choose(LocatorType.Path, "//form/div/div[3]//tr[3]//input");
            chinchilla.WebPage.ClickButton(LocatorType.Css, ".post-back-button input");
            chinchilla.WebPage.Source.Should().Contain("<input id=\"Control3_0\" type=\"radio\" name=\"Control3\" value=\"7\" />");
            chinchilla.WebPage.Source.Should().Contain("<input id=\"Control3_1\" type=\"radio\" name=\"Control3\" value=\"8\" />");
            chinchilla.WebPage.Source.Should().Contain("<input id=\"Control3_2\" type=\"radio\" name=\"Control3\" value=\"9\" checked=\"checked\" />");
        }
    }
}
