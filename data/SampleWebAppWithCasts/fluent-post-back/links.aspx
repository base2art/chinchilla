﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="links.aspx.cs" Inherits="SampleWebAppWithCasts.fluent_post_back.links" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    THIS IS THE PAGE FOR LINKS!
    <form id="form1" runat="server">
    <div>
        <div class="control-1">
            <asp:HyperLink runat="server" Text="Link 1" NavigateUrl="buttons.aspx"/>
        </div>
        <div class="control-2">
            <asp:HyperLink runat="server" Text="Link 2" NavigateUrl="selects.aspx" />
        </div>
    </div>
    </form>
</body>
</html>
