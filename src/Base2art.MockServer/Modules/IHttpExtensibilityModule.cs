﻿namespace Base2art.MockServer.Modules
{
    public interface IHttpExtensibilityModule
    {
        void Init(IHttpExtensibleApplication application);
    }
}