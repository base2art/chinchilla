﻿
namespace Base2art.WebRunner.Server.UI
{
    partial class MainForm
    {
        /// <summary>
        /// Designer variable used to keep track of non-visual components.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private System.Windows.Forms.Button runButton;
        private System.Windows.Forms.Button openBrowserButton;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox outputDisplay;
        private Base2art.WebRunner.Server.UI.SingleModelEditor singleModelEditor1;
        private System.Windows.Forms.Timer timer1;
        
        /// <summary>
        /// Disposes resources used by the form.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing) {
                if (components != null) {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }
        
        /// <summary>
        /// This method is required for Windows Forms designer support.
        /// Do not change the method contents inside the source code editor. The Forms designer might
        /// not be able to load this method if it was changed manually.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.runButton = new System.Windows.Forms.Button();
            this.openBrowserButton = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.singleModelEditor1 = new Base2art.WebRunner.Server.UI.SingleModelEditor();
            this.outputDisplay = new System.Windows.Forms.TextBox();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // runButton
            // 
            this.runButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.runButton.Location = new System.Drawing.Point(12, 396);
            this.runButton.Name = "runButton";
            this.runButton.Size = new System.Drawing.Size(75, 23);
            this.runButton.TabIndex = 3;
            this.runButton.Text = "Run";
            this.runButton.UseVisualStyleBackColor = true;
            this.runButton.Click += new System.EventHandler(this.runButtonClick);
            // 
            // openBrowserButton
            // 
            this.openBrowserButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.openBrowserButton.Enabled = false;
            this.openBrowserButton.Location = new System.Drawing.Point(93, 396);
            this.openBrowserButton.Name = "openBrowserButton";
            this.openBrowserButton.Size = new System.Drawing.Size(98, 23);
            this.openBrowserButton.TabIndex = 4;
            this.openBrowserButton.Text = "Open Browser";
            this.openBrowserButton.UseVisualStyleBackColor = true;
            this.openBrowserButton.Click += new System.EventHandler(this.OpenBrowserButtonClick);
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.Controls.Add(this.singleModelEditor1);
            this.panel1.Controls.Add(this.outputDisplay);
            this.panel1.Location = new System.Drawing.Point(13, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(383, 378);
            this.panel1.TabIndex = 6;
            // 
            // singleModelEditor1
            // 
            this.singleModelEditor1.Location = new System.Drawing.Point(27, 30);
            this.singleModelEditor1.Name = "singleModelEditor1";
            this.singleModelEditor1.Size = new System.Drawing.Size(151, 119);
            this.singleModelEditor1.TabIndex = 9;
            // 
            // outputDisplay
            // 
            this.outputDisplay.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.outputDisplay.Location = new System.Drawing.Point(161, 74);
            this.outputDisplay.Multiline = true;
            this.outputDisplay.Name = "outputDisplay";
            this.outputDisplay.ReadOnly = true;
            this.outputDisplay.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.outputDisplay.Size = new System.Drawing.Size(191, 149);
            this.outputDisplay.TabIndex = 8;
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.Timer1Tick);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(408, 431);
            this.Controls.Add(this.openBrowserButton);
            this.Controls.Add(this.runButton);
            this.Controls.Add(this.panel1);
            this.Name = "MainForm";
            this.Text = "Asp.Net Server";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }
    }
}
