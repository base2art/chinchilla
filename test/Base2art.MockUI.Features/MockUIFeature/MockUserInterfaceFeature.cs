﻿namespace Base2art.MockUI.Features.MockUIFeature
{
    using System.Collections.Specialized;
    using System.Linq;

    using Base2art.MockServer;
    using Base2art.MockServer.Features;
    using Base2art.MockUI.CsQuery;
    using Base2art.WebRunner.CsQuery;

    using FluentAssertions;

    using NUnit.Framework;

    [TestFixture]
    public class MockUserInterfaceFeature
    {
        [Test]
        public void ShouldSelect()
        {
            using (var ui = new MockBrowser(new CsQueryFormParser(), new MockServer(ServerDataFactory.Create())))
            {
                var cq = ui.RequestPage(new PageData { Path = "Default.aspx" });
                cq.Document().Find(".world").Get(0).InnerText.Trim().Should().Be("Welcome to ASP.NET!");

                cq.Document().Find("World").Get(0).Should().BeNull();
            }
        }

        [Test]
        public void ShouldGetPageThatDoesPostBack()
        {
            using (var ui = new MockBrowser(new CsQueryFormParser(), new MockServer(ServerDataFactory.Create())))
            {
                var cq = ui.RequestPage(new PageData { Path = "post-back-test.aspx" });

                var postData = new NameValueCollection();

                foreach (var input in cq.FormData.First())
                {
                    var option = input.Options.Last();
                    postData.Add(input.Name, option.Value);
                }

                string textAreaInput = "American Horror Story\r\n Murder House";
                postData["TextBox1"] = "hello world";
                postData["TextBox2"] = textAreaInput;
                postData["TextBox3"] = "pa55w0rd";

                var cq1 = ui.RequestPage(new PageData { Path = "post-back-test.aspx", Post = postData, });

                var text = cq1.RawText;

                // textboxes
                text.Should().Contain("name=\"TextBox1\" type=\"text\" value=\"hello world\" id=\"TextBox1\" />");
                text.Should()
                    .Contain(
                        "<textarea name=\"TextBox2\" rows=\"2\" cols=\"20\" id=\"TextBox2\">\r\n" + textAreaInput
                        + "</textarea>");
                text.Should().Contain("Password: pa55w0rd");

                // DDL
                text.Should().Contain("<option value=\"A\">A</option>");
                text.Should().Contain("<option value=\"B\">B</option>");
                text.Should().Contain("<option selected=\"selected\" value=\"C\">C</option>");

                // RBL
                text.Should().Contain("name=\"RadioButtonList1\" value=\"x\" />");
                text.Should().Contain("name=\"RadioButtonList1\" value=\"y\" />");
                text.Should().Contain("name=\"RadioButtonList1\" value=\"z\" checked=\"checked\" />");

                // CBL
                text.Should().Contain("name=\"ctl02$0\" checked=\"checked\" value=\"r\" />");
                text.Should().Contain("name=\"ctl02$1\" checked=\"checked\" value=\"s\" />");
                text.Should().Contain("name=\"ctl02$2\" checked=\"checked\" value=\"t\" />");

                // ListBox
                text.Should().Contain("<option value=\"d\">d</option>");
                text.Should().Contain("<option value=\"e\">e</option>");
                text.Should().Contain("<option selected=\"selected\" value=\"f\">f</option>");
            }
        }
    }
}
