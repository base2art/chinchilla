﻿namespace Base2art.WebRunner.CsQuery.Interacters
{
    using System.Linq;

    using CsQuery;

    public class CssWebPageInteracter : WebPageInteracterBase
    {
        public CssWebPageInteracter(CsQueryWebPage pageApi, IScopeContext scope, string[] explicitlyMatchedNodes)
            : base(pageApi, scope, false, explicitlyMatchedNodes)
        {
        }

        protected override IDomObjectWrapper[] FindTargetElements(string locator)
        {
            return this.ScopedNode.Cq().Find(locator).Select(x => new DomObjectWrapper(x)).ToArray();
        }
    }
}