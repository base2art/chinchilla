﻿namespace Base2art.MockUI
{
    public interface IFormParser
    {
        IFormData[] GetFormData(string rawText);
    }
}