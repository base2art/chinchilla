﻿/*
 * Created by SharpDevelop.
 * User: IEUser
 * Date: 3/30/2016
 * Time: 2:10 AM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
namespace Base2art.WebRunner.Server.UI
{
    partial class UserControl1
    {
        /// <summary>
        /// Designer variable used to keep track of non-visual components.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button folderChooserButton;
        private System.Windows.Forms.TextBox VirDirInput;
        private System.Windows.Forms.CheckBox directLoadingChooser;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox AppDirInput;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox sslChooser;
        private System.Windows.Forms.NumericUpDown portChooser;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox hostChooser;
        
        /// <summary>
        /// Disposes resources used by the control.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing) {
                if (components != null) {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }
        
        /// <summary>
        /// This method is required for Windows Forms designer support.
        /// Do not change the method contents inside the source code editor. The Forms designer might
        /// not be able to load this method if it was changed manually.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.folderChooserButton = new System.Windows.Forms.Button();
            this.VirDirInput = new System.Windows.Forms.TextBox();
            this.directLoadingChooser = new System.Windows.Forms.CheckBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.AppDirInput = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.sslChooser = new System.Windows.Forms.CheckBox();
            this.portChooser = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.hostChooser = new System.Windows.Forms.TextBox();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.portChooser)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.folderChooserButton);
            this.groupBox2.Controls.Add(this.VirDirInput);
            this.groupBox2.Controls.Add(this.directLoadingChooser);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.AppDirInput);
            this.groupBox2.Location = new System.Drawing.Point(106, 157);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(532, 72);
            this.groupBox2.TabIndex = 3;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Application Settings";
            // 
            // folderChooserButton
            // 
            this.folderChooserButton.Location = new System.Drawing.Point(440, 17);
            this.folderChooserButton.Name = "folderChooserButton";
            this.folderChooserButton.Size = new System.Drawing.Size(75, 23);
            this.folderChooserButton.TabIndex = 11;
            this.folderChooserButton.Text = "Choose...";
            this.folderChooserButton.UseVisualStyleBackColor = true;
            // 
            // VirDirInput
            // 
            this.VirDirInput.Location = new System.Drawing.Point(129, 43);
            this.VirDirInput.Name = "VirDirInput";
            this.VirDirInput.Size = new System.Drawing.Size(188, 20);
            this.VirDirInput.TabIndex = 10;
            this.VirDirInput.Text = "/";
            // 
            // directLoadingChooser
            // 
            this.directLoadingChooser.Location = new System.Drawing.Point(335, 44);
            this.directLoadingChooser.Name = "directLoadingChooser";
            this.directLoadingChooser.Size = new System.Drawing.Size(99, 19);
            this.directLoadingChooser.TabIndex = 9;
            this.directLoadingChooser.Text = "Direct Loading";
            this.directLoadingChooser.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(7, 45);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(111, 15);
            this.label3.TabIndex = 7;
            this.label3.Text = "Virtual Directory:";
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(6, 22);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(112, 17);
            this.label4.TabIndex = 6;
            this.label4.Text = "Application Directory:";
            // 
            // AppDirInput
            // 
            this.AppDirInput.Location = new System.Drawing.Point(129, 19);
            this.AppDirInput.Name = "AppDirInput";
            this.AppDirInput.Size = new System.Drawing.Size(305, 20);
            this.AppDirInput.TabIndex = 5;
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.sslChooser);
            this.groupBox1.Controls.Add(this.portChooser);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.hostChooser);
            this.groupBox1.Location = new System.Drawing.Point(106, 95);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(515, 56);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Server Settings";
            // 
            // sslChooser
            // 
            this.sslChooser.Location = new System.Drawing.Point(298, 21);
            this.sslChooser.Name = "sslChooser";
            this.sslChooser.Size = new System.Drawing.Size(75, 19);
            this.sslChooser.TabIndex = 4;
            this.sslChooser.Text = "Use SSL";
            this.sslChooser.UseVisualStyleBackColor = true;
            // 
            // portChooser
            // 
            this.portChooser.Location = new System.Drawing.Point(229, 20);
            this.portChooser.Maximum = new decimal(new int[] {
            65000,
            0,
            0,
            0});
            this.portChooser.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.portChooser.Name = "portChooser";
            this.portChooser.Size = new System.Drawing.Size(51, 20);
            this.portChooser.TabIndex = 3;
            this.portChooser.Value = new decimal(new int[] {
            7997,
            0,
            0,
            0});
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(191, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(32, 15);
            this.label2.TabIndex = 2;
            this.label2.Text = "Port:";
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(6, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(33, 17);
            this.label1.TabIndex = 1;
            this.label1.Text = "Host:";
            // 
            // hostChooser
            // 
            this.hostChooser.Location = new System.Drawing.Point(45, 19);
            this.hostChooser.Name = "hostChooser";
            this.hostChooser.Size = new System.Drawing.Size(140, 20);
            this.hostChooser.TabIndex = 0;
            this.hostChooser.Text = "localhost";
            // 
            // UserControl1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "UserControl1";
            this.Size = new System.Drawing.Size(744, 324);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.portChooser)).EndInit();
            this.ResumeLayout(false);

        }
    }
}
