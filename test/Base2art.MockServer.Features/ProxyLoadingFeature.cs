﻿namespace Base2art.MockServer.Features
{
    using System;
    using System.Globalization;
    using System.IO;

    using FluentAssertions;

    using NUnit.Framework;

    [TestFixture]
    public class ProxyLoadingFeature
    {
        private readonly JavascriptEnabledCapabilitiesFactory creator = new JavascriptEnabledCapabilitiesFactory();
        private readonly ServerData serverData = ServerDataFactory.Create();

        [SetUp]
        public void DeleteBinaries()
        {
            var path = Path.Combine(this.serverData.PhysicalApplicationDirectory, "bin");
            DeleteByPattern(path, "Base2art.*");
            DeleteByPattern(path, "FluentAssertions.*");
            DeleteByPattern(path, "nunit.framework.*");
            DeleteByPattern(path, "ReflectionMagic.*");
        }

        [Test]
        public void ShouldGetPageUsingDynamicLoading()
        {
            var serverParams = this.serverData.Clone();
            serverParams.UseDirectLoading = false;
            using (var server = new MockServer(this.creator, serverParams))
            {
                var rezult = server.RequestPage(new PageData
                {
                    Path = "Default.aspx",
                    QueryString = "qsValue=42"
                });

                var str = DateTime.UtcNow.Date.ToString(CultureInfo.InvariantCulture);
                rezult.RawText.Should().Contain("--STARTER--" + str + "--ENDER--");
            }
        }

        [Test]
        public void ShouldGetPageUsingDirectLoading()
        {
            var serverParams = this.serverData.Clone();
            serverParams.UseDirectLoading = true;
            using (var server = new MockServer(this.creator, this.serverData))
            {
                var rezult = server.RequestPage(new PageData
                {
                    Path = "Default.aspx",
                    QueryString = "qsValue=42"
                });

                var str = DateTime.UtcNow.Date.ToString(CultureInfo.InvariantCulture);
                rezult.RawText.Should().Contain("--STARTER--" + str + "--ENDER--");
            }
        }

        private static void DeleteByPattern(string path, string searchPattern)
        {
            foreach (var file in Directory.GetFiles(path, searchPattern))
            {
                File.Delete(file);
            }
        }
    }
}
