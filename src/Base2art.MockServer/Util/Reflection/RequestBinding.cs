﻿namespace Base2art.MockServer.Util.Reflection
{
    using System;
    using System.Collections.Specialized;
    using System.Reflection;
    using System.Web;

    using Base2art.Validation;

    public static class RequestBinding
    {
        private static readonly Type RequestType = typeof(HttpRequest);

        /// <summary>
        /// Set the Properties on the HttpCapabilities.
        /// </summary>
        /// <param name="request">The Request object.</param>
        /// <param name="valueProp">The name of the value property.</param>
        /// <param name="value">The value to set.</param>
        /// <returns>A value indicating that the property was set.</returns>
        public static bool SetProperty(this HttpRequest request, string valueProp, NameValueCollection value)
        {
            if (value != null && value.Count > 0)
            {
                var form = CreateForm(valueProp, RequestType);
                var nameValueCollection = CreateNameValueCollection();
                nameValueCollection.Add(value);
                form.SetValue(request, nameValueCollection);
                return true;
            }

            return false;
        }

        /// <summary>
        /// Set the Properties on an object.
        /// </summary>
        /// <param name="request">The Request object.</param>
        /// <param name="valueProp">The name of the value property.</param>
        /// <param name="value">The value to set.</param>
        public static void SetProperty(this HttpRequest request, string valueProp, object value)
        {
            var containerField = RequestType.GetField(valueProp, StandardBinding.StandardBindingFlags);
            containerField.Validate().IsNotNull();
            containerField.SetValue(request, value);
        }


        private static NameValueCollection CreateNameValueCollection()
        {
            var rapper = RequestType.Assembly.GetType("System.Web.HttpValueCollection");
            var nameValueCollection = Activator.CreateInstance(rapper, true) as NameValueCollection;
            nameValueCollection.Validate().IsNotNull();
            return nameValueCollection;
        }

        private static FieldInfo CreateForm(string valueProp, Type requestType)
        {
            var form = requestType.GetField(valueProp, StandardBinding.StandardBindingFlags);
            form.Validate().IsNotNull();
            return form;
        }
    }
}
