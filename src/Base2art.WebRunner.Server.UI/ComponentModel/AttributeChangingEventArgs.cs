﻿
namespace Base2art.WebRunner.Server.UI
{
    public class AttributeChangingEventArgs<T>
    {
        private readonly T oldValue;

        private readonly T newValue;
        
        public AttributeChangingEventArgs(T oldValue, T newValue)
        {
            this.newValue = newValue;
            this.oldValue = oldValue;
        }

        public T OldValue
        {
            get
            {
                return oldValue;
            }
        }

        public T NewValue
        {
            get
            {
                return newValue;
            }
        }
    }
}


