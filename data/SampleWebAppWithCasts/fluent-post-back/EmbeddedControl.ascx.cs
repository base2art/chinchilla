﻿namespace SampleWebAppWithCasts.fluent_post_back
{
    using System;
    using System.Collections.Generic;

    public partial class EmbeddedControl : System.Web.UI.UserControl
    {
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            this.MainRepeater.DataSource = new List<string>() { "sd1", "sd2" };
            this.MainRepeater.DataBind();
            this.FormView1.DataSource = new List<string>() { "sd1", "sd2" };
            this.FormView1.DataBind();
        }

        protected void HandleChange(object sender, EventArgs e)
        {
            this.PostbackLabel.Text = "Value: " + this.PostbackControl.Text + "!";
        }

        protected void HandleSomething(object sender, EventArgs e)
        {
        }
    }
}