﻿namespace Base2art.MockServer
{
    using System;
    using System.Runtime.Serialization;
    using System.Security.Permissions;

    [Serializable]
    public class ResponseException : ApplicationException
    {
        private readonly int statusCode;

        private readonly IPageResult pageResult;

        private readonly string simpleMessage;

        public ResponseException(int statusCode, string simpleMessage, IPageResult pageResult)
            : this(string.Format("{0} => {1}", statusCode, simpleMessage))
        {
            this.statusCode = statusCode;
            this.pageResult = pageResult;
            this.simpleMessage = simpleMessage;
        }

        public ResponseException(int statusCode, string simpleMessage, IPageResult pageResult, Exception exception)
            : this(string.Format("{0} => {1}", statusCode, simpleMessage), exception)
        {
            this.statusCode = statusCode;
            this.pageResult = pageResult;
            this.simpleMessage = simpleMessage;
        }

        public ResponseException()
        {
        }

        public ResponseException(string message)
            : base(message)
        {
        }

        public ResponseException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        [SecurityPermission(SecurityAction.Demand, SerializationFormatter = true)]
        protected ResponseException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            this.statusCode = info.GetInt32("ResponseException.StatusCode");
            this.simpleMessage = info.GetString("ResponseException.SimpleMessage");
            this.pageResult = (IPageResult)info.GetValue("ResponseException.PageResult", typeof(IPageResult));
        }

        public IPageResult PageResult
        {
            get
            {
                return this.pageResult;
            }
        }

        public int StatusCode
        {
            get
            {
                return this.statusCode;
            }
        }

        public string SimpleMessage
        {
            get
            {
                return this.simpleMessage;
            }
        }

        [SecurityPermissionAttribute(SecurityAction.Demand, SerializationFormatter = true)]
        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);

            info.AddValue("ResponseException.StatusCode", this.StatusCode);
            info.AddValue("ResponseException.SimpleMessage", this.SimpleMessage);
            info.AddValue("ResponseException.PageResult", this.PageResult);
        }
    }
}