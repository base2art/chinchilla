﻿/*
 * Created by SharpDevelop.
 * User: IEUser
 * Date: 3/30/2016
 * Time: 2:35 AM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
namespace Base2art.WebRunner.Server.UI
{
    partial class SingleModelEditor
    {
        /// <summary>
        /// Designer variable used to keep track of non-visual components.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private System.Windows.Forms.PropertyGrid propertyGrid;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.ListBox siteChooser;
        private System.Windows.Forms.Button removeSiteButton;
        private System.Windows.Forms.Button addSiteButton;
        private System.Windows.Forms.BindingSource bindingSource1;
        
        /// <summary>
        /// Disposes resources used by the control.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing) {
                if (components != null) {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }
        
        /// <summary>
        /// This method is required for Windows Forms designer support.
        /// Do not change the method contents inside the source code editor. The Forms designer might
        /// not be able to load this method if it was changed manually.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.propertyGrid = new System.Windows.Forms.PropertyGrid();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.siteChooser = new System.Windows.Forms.ListBox();
            this.bindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.removeSiteButton = new System.Windows.Forms.Button();
            this.addSiteButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).BeginInit();
            this.SuspendLayout();
            // 
            // propertyGrid
            // 
            this.propertyGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.propertyGrid.Location = new System.Drawing.Point(0, 0);
            this.propertyGrid.Name = "propertyGrid";
            this.propertyGrid.Size = new System.Drawing.Size(507, 309);
            this.propertyGrid.TabIndex = 8;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.siteChooser);
            this.splitContainer1.Panel1.Controls.Add(this.removeSiteButton);
            this.splitContainer1.Panel1.Controls.Add(this.addSiteButton);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.propertyGrid);
            this.splitContainer1.Size = new System.Drawing.Size(766, 309);
            this.splitContainer1.SplitterDistance = 255;
            this.splitContainer1.TabIndex = 9;
            // 
            // siteChooser
            // 
            this.siteChooser.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.siteChooser.DataSource = this.bindingSource1;
            this.siteChooser.DisplayMember = "Name";
            this.siteChooser.FormattingEnabled = true;
            this.siteChooser.Location = new System.Drawing.Point(3, 32);
            this.siteChooser.Name = "siteChooser";
            this.siteChooser.Size = new System.Drawing.Size(249, 264);
            this.siteChooser.TabIndex = 10;
            this.siteChooser.SelectedIndexChanged += new System.EventHandler(this.SiteChooserSelectedIndexChanged);
            // 
            // bindingSource1
            // 
            this.bindingSource1.DataSource = typeof(Base2art.WebRunner.Server.ConfigurableSiteData);
            // 
            // removeSiteButton
            // 
            this.removeSiteButton.AccessibleDescription = "Add Site";
            this.removeSiteButton.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.removeSiteButton.Location = new System.Drawing.Point(127, 3);
            this.removeSiteButton.Name = "removeSiteButton";
            this.removeSiteButton.Size = new System.Drawing.Size(27, 23);
            this.removeSiteButton.TabIndex = 9;
            this.removeSiteButton.Text = "-";
            this.removeSiteButton.UseVisualStyleBackColor = true;
            this.removeSiteButton.Click += new System.EventHandler(this.RemoveSiteButtonClick);
            // 
            // addSiteButton
            // 
            this.addSiteButton.AccessibleDescription = "Add Site";
            this.addSiteButton.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.addSiteButton.Location = new System.Drawing.Point(94, 3);
            this.addSiteButton.Name = "addSiteButton";
            this.addSiteButton.Size = new System.Drawing.Size(27, 23);
            this.addSiteButton.TabIndex = 8;
            this.addSiteButton.Text = "+";
            this.addSiteButton.UseVisualStyleBackColor = true;
            this.addSiteButton.Click += new System.EventHandler(this.AddSiteButtonClick);
            // 
            // SingleModelEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.splitContainer1);
            this.Name = "SingleModelEditor";
            this.Size = new System.Drawing.Size(766, 309);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).EndInit();
            this.ResumeLayout(false);

        }
    }
}
