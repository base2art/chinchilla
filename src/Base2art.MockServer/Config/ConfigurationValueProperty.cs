﻿namespace Base2art.ComponentModel
{
    using System;
    using System.Configuration;
    using System.Runtime.Serialization;
    using System.Security.Permissions;

    using Base2art.Converters;
    using Base2art.Validation;

    [Serializable]
    public class ConfigurationValueProperty<T> : ISerializable
        where T : struct
    {
        [NonSerialized]
        private readonly IValueParser<T> parser;

        [NonSerialized]
        private readonly string settingName;

        [NonSerialized]
        private readonly ICreator<T> defaultValueProvider;

        private T value;

        private bool hasValue;

        public ConfigurationValueProperty(IValueParser<T> parser, string settingName)
            : this(parser, settingName, default(T))
        {
        }

        public ConfigurationValueProperty(IValueParser<T> parser, string settingName, T defaultValue)
            : this(parser, settingName, new NullCreator<T>(defaultValue))
        {
        }

        public ConfigurationValueProperty(IValueParser<T> parser, string settingName, ICreator<T> defaultValueProvider)
        {
            this.parser = parser;
            this.settingName = settingName;
            this.defaultValueProvider = defaultValueProvider ?? new NullCreator<T>();
        }

        protected ConfigurationValueProperty(SerializationInfo info, StreamingContext context)
        {
            info.Validate().IsNotNull();
            this.Value = (T)info.GetValue("ConfigurationValueProperty.Value", typeof(T));
        }

        public T Value
        {
            get
            {
                if (this.hasValue)
                {
                    return this.value;
                }

                return parser.ParseOrDefault(
                    ConfigurationManager.AppSettings[this.settingName],
                    this.defaultValueProvider);
            }

            set
            {
                this.hasValue = true;
                this.value = value;
            }
        }

        [SecurityPermission(SecurityAction.Demand, SerializationFormatter = true)]
        protected virtual void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("ConfigurationValueProperty.Value", this.Value);
        }

        [SecurityPermission(SecurityAction.LinkDemand, Flags = SecurityPermissionFlag.SerializationFormatter)]
        void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.Validate().IsNotNull();
            this.GetObjectData(info, context);
        }
    }
}