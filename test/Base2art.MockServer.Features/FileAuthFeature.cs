﻿namespace Base2art.MockServer.Features
{
    using System;

    using FluentAssertions;

    using NUnit.Framework;

    [TestFixture]
    public class FileAuthFeature
    {
        [Test]
        public void ShouldGetPageWithThatIsAllowed()
        {
            using (var server = new MockServer(ServerDataFactory.Create()))
            {
                var rezult = server.RequestPage(new PageData { Path = "file-authorization/permitted.aspx" });
                rezult.RawText.Should().Contain("PERMITTED PAGE");
            }
        }

        [Test]
        public void ShouldGetPageWithThatIsAllowedBecauseOfFormsAuth()
        {
            using (var server = new MockServer(ServerDataFactory.Create()))
            {
                var rezult = server.RequestPage(new PageData { Path = "file-authorization/login.aspx" });
                rezult.RawText.Should().Contain("LOGIN PAGE");
            }
        }

        [Test]
        public void ShouldGetExceptionGetPageWithThatIsNotAllowed()
        {
            using (var server = new MockServer(ServerDataFactory.Create()))
            {
                new Action(() => server.RequestPage(new PageData { Path = "file-authorization/restricted.aspx" }))
                    .ShouldThrow<RedirectException>()
                    .And.RedirectLocation.Should().StartWith("/file-authorization/login.aspx?ReturnUrl=");
            }
        }
    }
}
