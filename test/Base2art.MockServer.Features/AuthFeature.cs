﻿namespace Base2art.MockServer.Features
{
    using System.Security.Principal;
    using System.Web.UI.WebControls;

    using Base2art.MockServer;
    using Base2art.MockServer.Util;

    using FluentAssertions;

    using NUnit.Framework;

    [TestFixture]
    public class AuthFeature
    {
        [Test]
        public void ShouldReadCurrentUser()
        {
            var serverData = ServerDataFactory.Create();
            serverData.UseDirectLoading.Should().BeFalse();
            using (var server = new MockServer(serverData))
            {
                var rezult = server.RequestControl(
                        new ControlData<Literal>
                            {
                                Control = new ControlCreator<Literal>(
                                    () =>
                                    {
                                        var literal = new Literal();
                                        literal.Init += (x, y) =>
                                            {
                                                var name = literal.Page.User.Identity.Name;
                                                name.Should().Be("lhakkor");
                                                literal.Text = name;
                                            };
                                        return literal;
                                    }),
                                ContextSetup = new ContextSetup((r, x) => x.User = new GenericPrincipal(new GenericIdentity("lhakkor"), new string[0])),
                                PlaceInsideForm = false
                            });
                rezult.RawText.Should().Be("lhakkor");
            }
        }
    }
}
