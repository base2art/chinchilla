﻿namespace Base2art.MockServer
{
    using System;
    using System.Runtime.Serialization;
    using System.Security.Permissions;

    [Serializable]
    public class RedirectException : ResponseException
    {
        private readonly string redirectLocation;

        public RedirectException(int statusCode, string redirectLocation, string simpleMessage, IPageResult pageResult)
            : base(statusCode, simpleMessage, pageResult)
        {
            this.redirectLocation = redirectLocation;
        }

        public RedirectException()
        {
        }

        public RedirectException(string message)
            : base(message)
        {
        }

        public RedirectException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        [SecurityPermission(SecurityAction.Demand, SerializationFormatter = true)]
        protected RedirectException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            this.redirectLocation = info.GetString("RedirectException.RedirectLocation");
        }

        public string RedirectLocation
        {
            get
            {
                return this.redirectLocation;
            }
        }

        [SecurityPermission(SecurityAction.Demand, SerializationFormatter = true)]
        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);

            info.AddValue("RedirectException.RedirectLocation", this.RedirectLocation);
        }
    }
}