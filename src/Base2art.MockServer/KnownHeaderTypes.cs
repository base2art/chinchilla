﻿namespace Base2art.MockServer
{
    public enum KnownHeaderTypes
    {
        /// <summary>The index that represents the HTTP Cache-Control HTTP header.</summary>
        HeaderCacheControl = 0,
        /// <summary>Specifies the index number for the Connection HTTP header.</summary>
        HeaderConnection = 1,
        /// <summary>Specifies the index number for the Date HTTP header.</summary>
        HeaderDate = 2,
        /// <summary>Specifies the index number for the Keep-Alive HTTP header.</summary>
        HeaderKeepAlive = 3,
        /// <summary>Specifies the index number for the Pragma HTTP header.</summary>
        HeaderPragma = 4,
        /// <summary>Specifies the index number for the Trailer HTTP header.</summary>
        HeaderTrailer = 5,
        /// <summary>Specifies the index number for the Transfer-Encoding HTTP header.</summary>
        HeaderTransferEncoding = 6,
        /// <summary>Specifies the index number for the Upgrade HTTP header.</summary>
        HeaderUpgrade = 7,
        /// <summary>Specifies the index number for the Via HTTP header.</summary>
        HeaderVia = 8,
        /// <summary>Specifies the index number for the Warning HTTP header.</summary>
        HeaderWarning = 9,
        /// <summary>Specifies the index number for the Allow HTTP header.</summary>
        HeaderAllow = 10,
        /// <summary>Specifies the index number for the Content-Length HTTP header.</summary>
        HeaderContentLength = 11,
        /// <summary>Specifies the index number for the Content-Type HTTP header.</summary>
        HeaderContentType = 12,
        /// <summary>Specifies the index number for the Content-Encoding HTTP header.</summary>
        HeaderContentEncoding = 13,
        /// <summary>Specifies the index number for the Content-Language HTTP header.</summary>
        HeaderContentLanguage = 14,
        /// <summary>Specifies the index number for the Content-Location HTTP header.</summary>
        HeaderContentLocation = 15,
        /// <summary>Specifies the index number for the Content-MD5 HTTP header.</summary>
        HeaderContentMd5 = 16,
        /// <summary>Specifies the index number for the Content-Range HTTP header.</summary>
        HeaderContentRange = 17,
        /// <summary>Specifies the index number for the Expires HTTP header.</summary>
        HeaderExpires = 18,
        /// <summary>Specifies the index number for the Last-Modified HTTP header.</summary>
        HeaderLastModified = 19,
        /// <summary>Specifies the index number for the Accept HTTP header.</summary>
        HeaderAccept = 20,
        /// <summary>Specifies the index number for the Accept-Charset HTTP header.</summary>
        HeaderAcceptCharset = 21,
        /// <summary>Specifies the index number for the Accept-Encoding HTTP header.</summary>
        HeaderAcceptEncoding = 22,
        /// <summary>Specifies the index number for the Accept-Language HTTP header.</summary>
        HeaderAcceptLanguage = 23,
        /// <summary>Specifies the index number for the Authorization HTTP header.</summary>
        HeaderAuthorization = 24,
        /// <summary>Specifies the index number for the Cookie HTTP header.</summary>
        HeaderCookie = 25,
        /// <summary>Specifies the index number for the Except HTTP header.</summary>
        HeaderExpect = 26,
        /// <summary>Specifies the index number for the From HTTP header.</summary>
        HeaderFrom = 27,
        /// <summary>Specifies the index number for the Host HTTP header.</summary>
        HeaderHost = 28,
        /// <summary>Specifies the index number for the If-Match HTTP header.</summary>
        HeaderIfMatch = 29,
        /// <summary>Specifies the index number for the If-Modified-Since HTTP header.</summary>
        HeaderIfModifiedSince = 30,
        /// <summary>Specifies the index number for the If-None-Match HTTP header.</summary>
        HeaderIfNoneMatch = 31,
        /// <summary>Specifies the index number for the If-Range HTTP header.</summary>
        HeaderIfRange = 32,
        /// <summary>Specifies the index number for the If-Unmodified-Since HTTP header.</summary>
        HeaderIfUnmodifiedSince = 33,
        /// <summary>Specifies the index number for the Max-Forwards HTTP header.</summary>
        HeaderMaxForwards = 34,
        /// <summary>Specifies the index number for the Proxy-Authorization HTTP header.</summary>
        HeaderProxyAuthorization = 35,
        /// <summary>Specifies the index number for the Referer HTTP header.</summary>
        HeaderReferer = 36,
        /// <summary>Specifies the index number for the Range HTTP header.</summary>
        HeaderRange = 37,
        /// <summary>Specifies the index number for the TE HTTP header.</summary>
        HeaderTe = 38,
        /// <summary>Specifies the index number for the User-Agent HTTP header.</summary>
        HeaderUserAgent = 39,
        /// <summary>Specifies the index number for the Maximum HTTP request header.</summary>
        RequestHeaderMaximum = 40,
        /// <summary>Specifies the index number for the Accept-Ranges HTTP header.</summary>
        HeaderAcceptRanges = 20,
        /// <summary>Specifies the index number for the Age HTTP header.</summary>
        HeaderAge = 21,
        /// <summary>Specifies the index number for the ETag HTTP header.</summary>
        HeaderEtag = 22,
        /// <summary>Specifies the index number for the Location HTTP header.</summary>
        HeaderLocation = 23,
        /// <summary>Specifies the index number for the Proxy-Authenticate HTTP header.</summary>
        HeaderProxyAuthenticate = 24,
        /// <summary>Specifies the index number for the Retry-After HTTP header.</summary>
        HeaderRetryAfter = 25,
        /// <summary>Specifies the index number for the Server HTTP header.</summary>
        HeaderServer = 26,
        /// <summary>Specifies the index number for the Set-Cookie HTTP header.</summary>
        HeaderSetCookie = 27,
        /// <summary>Specifies the index number for the Vary HTTP header.</summary>
        HeaderVary = 28,
        /// <summary>Specifies the index number for the WWW-Authenticate HTTP header.</summary>
        HeaderWwwAuthenticate = 29,
        /// <summary>Specifies the index number for the Maximum HTTP response header.</summary>
        ResponseHeaderMaximum = 30,
    }
}
