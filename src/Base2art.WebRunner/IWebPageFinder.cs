﻿namespace Base2art.WebRunner
{
    public interface IWebPageFinder
    {
        IElement FindElement(string locator);

        IElement FindElement(LocatorType locatorType, string locator);

        IElement FindFirstElement(string locator);

        IElement FindFirstElement(LocatorType locatorType, string locator);

        IElement[] FindElements(string locator);

        IElement[] FindElements(LocatorType locatorType, string locator);
    }
}