﻿namespace Base2art.MockUI
{
    public interface IFormField
    {
        string Name { get; }

        IFormFieldOption[] Options { get; }

        string CurrentValue { get; }

        InputType InputType { get; }

        string PathToElement { get; }
    }
}