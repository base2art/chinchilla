﻿namespace RealWorldTest
{
    using System;
    using System.Configuration;
    using System.Linq;
    using System.Net;
    using System.Reflection;
    using System.Web;
    using System.Web.Security;

    using Base2art.MockServer;
    using Base2art.MockServer.Util;
    using Base2art.MockUI;
    using Base2art.MockUI.CsQuery;

    using FluentAssertions;

    using Geonetric.VitalSite.Web.Security;

    using NUnit.Framework;

    [TestFixture]
    public class VitalSiteTests
    {
        [Test]
        public void ShouldRunLoginPage()
        {
            var serverData = this.CreatServerData();
            using (var ui = new MockBrowser(new CsQueryFormParser(), new MockServer(serverData)))
            {
                var pageData = new PageData { Path = "login.aspx" };
                pageData.ContextSetup = CreateUnauthenticatedContext();
                var cq = ui.RequestPage(pageData);
                cq.NormalizedText.Should().Contain("Sign in using your VitalSite Core account");
            }
        }

        [Test]
        public void ShouldBeLoggedIn()
        {
            var serverData = this.CreatServerData();
            using (var ui = new MockBrowser(new CsQueryFormParser(), new MockServer(serverData)))
            {
                var pageData = new PageData { Path = "admin/index.aspx", Cookies = new[] { this.UserCookie() } };
                pageData.ContextSetup = CreateAuthenticatedContext();
                var cq = ui.RequestPage(pageData);
                Console.WriteLine(cq.RawText);
                cq.NormalizedText.Should().NotContain("Sign in using your VitalSite Core account");
                cq.NormalizedText.Should()
                  .Contain("<!-- NOT YET <li><a class=\"InboxLink\" href=\"javascript:;\">Inbox (5)</a></li> -->");
                cq.RawText.Length.Should().BeGreaterThan(1000);
            }
        }

        [Test]
        public void ShouldBeAbleToGetCmsPage()
        {
            var serverData = this.CreatServerData();
            serverData.DefaultPage = "app/blank.aspx";

            using (var ui = new MockBrowser(new CsQueryFormParser(), new MockServer(serverData)))
            {
                var pageData = new PageData
                                   {
                                       Path = "calendar/",
                                       Cookies = new[] { this.UserCookie() }
                                   };
                pageData.ContextSetup = new ContextSetup((x, y) =>
                    {
                        CreateAuthenticatedContext().Invoke(x, y);
                        y.RewritePath("/app/blank.aspx");
                    });
                try
                {
                    var cq = ui.RequestPage(pageData);
                    Console.WriteLine(cq.RawText);
                    cq.RawText.Should().Contain("Calendar Event Search");
                }
                catch (ResourceNotFoundException rnfe)
                {
                    Console.WriteLine(rnfe.PageResult.RawText);
                    throw new Exception();
                }
            }
        }

        private ServerData CreatServerData()
        {
            return new ServerData { PhysicalApplicationDirectory = ConfigurationManager.AppSettings["defaultPhysicalApplicationDirectory"] };
        }

        private Cookie UserCookie()
        {
            return new Cookie(
                ".VitalSite",
                "3556E793760EA786A3465754715FB77196C5418F852E4884933B3EF"
                + "7596A65CDA615686EB2D0598B3F47B2F2AC872424119FE47D2CE2E"
                + "80FA00EAB8E22DFD3455E80627F11A7588AF58E9E8BE2C7687E867"
                + "5AA38721EA9A557C9563849B3A7D9A2A8E37570F9D4362AAD1BEBB"
                + "E2407FA8AF34D807EE63D40E2ACB9FB94A88F45F953EC817374DBC"
                + "445D0D86BA6E873E913EEA798440BB0FDED8895918BB62CE0");
        }

        private static ContextSetup CreateUnauthenticatedContext()
        {
            return new ContextSetup((r, x) =>
            {
                x.ApplicationInstance = new HttpApplication();
                x.ApplicationInstance.Should().NotBeNull("Application Instance Required for ADFS");
                FormsIdentity fi = new FormsIdentity(new FormsAuthenticationTicket("TEST", true, 120));

                HttpContextBase contextBase = new HttpContextWrapper(x);
                DateTime dt = DateTime.UtcNow.AddMinutes(-1);
                var asm = r.Load(new AssemblyName("Geonetric.VitalSite.Web"));
                Type t = asm.GetType("Geonetric.VitalSite.Web.Security.AuthenticatedUser");
                var ctor = t.GetConstructors(BindingFlags.NonPublic | BindingFlags.Instance).First();
                var obj = ctor.Invoke(new object[] { contextBase, fi, dt });
                IAuthenticatedUser au = (IAuthenticatedUser)obj;
                x.User = au;
            });
        }

        private static ContextSetup CreateAuthenticatedContext()
        {
            return new ContextSetup((appDomain, context) =>
            {
                context.ApplicationInstance = new HttpApplication();
                context.ApplicationInstance.Should().NotBeNull("Application Instance Required for ADFS");
                FormsIdentity fi = new FormsIdentity(new FormsAuthenticationTicket("2", true, 120));
                HttpContextBase contextBase = new HttpContextWrapper(context);
                DateTime dt = DateTime.UtcNow.AddMinutes(-1);
                var asm = appDomain.Load(new AssemblyName("Geonetric.VitalSite.Web"));
                Type t = asm.GetType("Geonetric.VitalSite.Web.Security.AuthenticatedUser");
                var ctor = t.GetConstructors(BindingFlags.NonPublic | BindingFlags.Instance).First();
                var obj = ctor.Invoke(new object[] { contextBase, fi, dt });
                IAuthenticatedUser au = (IAuthenticatedUser)obj;
                context.User = au;
            });
        }
    }
}

////                        object obj = r.CreateInstanceAndUnwrap(
////                            "Base2art.VitalSite.Web",
////                            "Base2art.VitalSite.Web.Security.AuthenticatedUser",
////                            false,
////                            BindingFlags.NonPublic | BindingFlags.Public,
////                            null,
////                            new object[] { contextBase, fi, dt },
////                            CultureInfo.InvariantCulture,
////                            new object[0]);
////                        Base2art.VitalSite.Web.Security.IAuthenticatedUser au = null;