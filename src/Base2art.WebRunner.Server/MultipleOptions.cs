﻿
namespace Base2art.WebRunner.Server
{
    using System;
    using CommandLine;
    
    public class MultipleOptions : ModeOptions
    {
        [Option('c', "config-file", HelpText = "The json configuration file", Required = true)]
        public string ConfigFile { get; set; }
    }
}


