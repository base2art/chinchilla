﻿namespace Base2art.MockUI
{
    public class FormField : IFormField
    {
        public string PathToElement { get; set; }

        public string Name { get; set; }

        public IFormFieldOption[] Options { get; set; }

        public string CurrentValue { get; set; }

        public InputType InputType { get; set; }
    }
}