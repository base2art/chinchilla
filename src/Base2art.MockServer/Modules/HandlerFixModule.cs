﻿namespace Base2art.MockServer.Modules
{
    using System.Web;
    using System.Web.UI;

    using Base2art.MockServer;

    using ReflectionMagic;

    public class HandlerFixModule : IHttpExtensibilityModule, IPageCreationFactory
    {
        private readonly PageCreationFactory pageCreationFactory = new PageCreationFactory();

        public TPage CreatePage<TPage>(string path)
            where TPage : Page
        {
            return this.pageCreationFactory.CreatePage<TPage>(path);
        }

        public void Init(IHttpExtensibleApplication application)
        {
            application.PostMapHandler += this.HandleRequest;
        }

        private void HandleRequest(HttpContext httpContext)
        {
            ContextExtensionData extensionData = httpContext.GetExtensionData();
            var pageObj = extensionData.Page;

            Page currentPage = null;

            var httpHandler = httpContext.CurrentHandler;
            var defaultHandler = httpHandler as DefaultHttpHandler;

            if (defaultHandler == null && httpHandler != null)
            {
                IHttpHandler currentHandler = pageObj ?? httpHandler;
                httpContext.AsDynamic().Handler = currentHandler;
                httpContext.AsDynamic()._currentHandler = currentHandler;
                if (pageObj != null)
                {
                    currentPage = pageObj;
                }
                else
                {
                    Page page = httpHandler as Page;
                    if (page != null)
                    {
                        currentPage = page;
                    }
                }
            }
            else
            {
                var newPage = this.pageCreationFactory.CreatePage<Page>(httpContext.Request.Path);
                if (newPage == null)
                {
                    newPage = pageObj;
                }

                if (newPage != null)
                {
                    httpContext.AsDynamic().Handler = newPage;
                    httpContext.AsDynamic()._currentHandler = newPage;
                }

                currentPage = newPage;
            }

            if (currentPage != null)
            {
                currentPage.Error += (sender, args) => { extensionData.HasException = true; };
            }
        }
    }
}
