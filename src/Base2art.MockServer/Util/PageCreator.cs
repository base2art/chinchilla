﻿namespace Base2art.MockServer.Util
{
    using System;
    using System.Web;
    using System.Web.UI;

    [Serializable]
    public class PageCreator<T> : IPageCreator<T>
        where T : Page
    {
        private readonly Func<T> func;

        public PageCreator(Func<T> func)
        {
            this.func = func;
        }

        public T Create(string path)
        {
            return this.func.Invoke();
        }
    }
}