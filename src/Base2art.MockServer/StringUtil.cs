namespace Base2art.MockServer
{
    using System.Text.RegularExpressions;

    public static class StringUtil
    {
        public static string NormalizeText(this string rawText)
        {
            return Regex.Replace(rawText, @"\s+", " ");
        }
    }
}