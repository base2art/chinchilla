﻿namespace SampleWebAppWithCasts
{
    using System.Web;

    public class CustomRewriteModule : IHttpModule
    {
        void IHttpModule.Init(HttpApplication application)
        {
            application.BeginRequest += (sender, args) =>
                {
                    var context = HttpContext.Current;

                    if (context.Request.Path == "/rewrite-test")
                    {
                        context.RewritePath("rewrite-dest.aspx", true);
                    }

                    if (context.Request.Url.Host == "www.test.com")
                    {
                        context.Response.Write("TEST DOMAIN FOUND");
                        context.Response.End();
                    }
                };
        }

        void IHttpModule.Dispose()
        {
        }
    }
}