﻿namespace Base2art.MockUI
{
    using System.Collections.ObjectModel;

    public class FormFieldCollection : KeyedCollection<string, FormField>
    {
        protected override string GetKeyForItem(FormField item)
        {
            return item.Name;
        }
    }
}