﻿namespace Base2art.WebRunner.CsQuery
{
    public class Element : IElement
    {
        private readonly IDomObjectWrapper element;

        public Element(IDomObjectWrapper element)
        {
            this.element = element;
        }

        public string Value
        {
            get
            {
                return this.element.Value;
            }
        }

        public string Name
        {
            get
            {
                return this.element.Name;
            }
        }

        public string PathToElement
        {
            get
            {
                return this.element.PathToElement;
            }
        }

        public string NodeName
        {
            get
            {
                return this.element.NodeName;
            }
        }

        public string TextValue
        {
            get
            {
                return this.element.TextValue;
            }
        }

        public string this[string name]
        {
            get
            {
                return this.element[name];
            }
        }
    }
}