﻿namespace Base2art.MockUI.Features.MockUIFeature
{
    using System;
    using System.Linq;
    using System.Xml.Linq;
    using System.Xml.XPath;

    using Base2art.MockUI.CsQuery;

    using global::CsQuery;

    using FluentAssertions;

    using NUnit.Framework;

    [TestFixture]
    public class DomObjectToXElementConversionFeature
    {
        [Test]
        public void ShouldConvertCorrectlty()
        {
            const string Html = @"
<html>
  <head></head>
  <body>
    <form>
      <input type='text' id='tb1' value=''  />
      <input type='text' id='tb2' value='sdf' />
    </form>
    <form>
      <input type='text' id='tb3' />
      <input type='text' id='tb4' />
    </form>
  </body>
</html>";

            var cq = new CQ(Html);
            var doc = cq.Document.ToXDocument();
            Console.WriteLine(doc);
            var elements = doc.XPathSelectElements("//input[@value='sdf']");
            elements.Count().Should().Be(1);
            elements.First().Attribute("id").Value.Should().Be("tb2");
        }
    }
}
