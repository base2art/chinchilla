﻿namespace Base2art.MockServer.Util
{
    using System;

    [Serializable]
    public class NullDelegate<T> : IDelegate<T>
    {
        public void Invoke(T item)
        {
        }
    }

    [Serializable]
    public class NullDelegate<T1, T2> : IDelegate<T1, T2>
    {
        public void Invoke(T1 item1, T2 item2)
        {
        }
    }
}