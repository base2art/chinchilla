﻿namespace Base2art.MockUI
{
    using System;

    public enum InputType
    {
        Text,
        TextArea,
        DropDownList,

        Hidden,
        Password,

        Button,

        Checkbox,

        Color,

        Date,

        DateTime,

        LocalDateTime,

        Email,

        File,

        Image,

        Month,

        Number,

        RadioButton,

        Range,

        Reset,

        Search,

        Submit,

        Time,

        Tel,

        Url,

        Week
    }

    public static class InputTypeHelper
    {
        public static bool AutomaticallyPostOnSubmit(this InputType inputType)
        {
            switch (inputType)
            {
                case InputType.Text:
                case InputType.TextArea:
                case InputType.DropDownList:
                case InputType.Hidden:
                case InputType.Password:
                case InputType.Checkbox:
                case InputType.Color:
                case InputType.Date:
                case InputType.DateTime:
                case InputType.LocalDateTime:
                case InputType.Email:
                case InputType.File:
                case InputType.Month:
                case InputType.Number:
                case InputType.RadioButton:
                case InputType.Range:
                case InputType.Search:
                case InputType.Time:
                case InputType.Tel:
                case InputType.Url:
                case InputType.Week:
                    return true;
                case InputType.Button:
                case InputType.Image:
                case InputType.Reset:
                case InputType.Submit:
                    return false;
                default:
                    throw new ArgumentOutOfRangeException("inputType");
            }
        }

        public static InputType ParseType(string inputType)
        {
            switch (inputType.ToLower())
            {
                case "button":
                    return InputType.Button;
                case "checkbox":
                    return InputType.Checkbox;
                case "color":
                    return InputType.Color;
                case "date":
                    return InputType.Date;
                case "datetime":
                    return InputType.DateTime;
                case "datetime-local":
                    return InputType.LocalDateTime;
                case "email":
                    return InputType.Email;
                case "file":
                    return InputType.File;
                case "hidden":
                    return InputType.Hidden;
                case "image":
                    return InputType.Image;
                case "month":
                    return InputType.Month;
                case "number":
                    return InputType.Number;
                case "password":
                    return InputType.Password;
                case "radio":
                    return InputType.RadioButton;
                case "range":
                    return InputType.Range;
                case "reset":
                    return InputType.Reset;
                case "search":
                    return InputType.Search;
                case "submit":
                    return InputType.Submit;
                case "tel":
                    return InputType.Tel;
                case "text":
                    return InputType.Text;
                case "time":
                    return InputType.Time;
                case "url":
                    return InputType.Url;
                case "week":
                    return InputType.Week;
                default:
                    throw new InvalidOperationException("Unknown Input Type: '" + inputType + "'");
            }
        }
    }
}